<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>HR System Software Solutions | Baba Software</title>
    <meta name="description" content="Baba Software provides HR Software, HR Software Solutions, HR Software Suppliers and more. HR Software is affiliated with ERP Software Companies. Hire better & faster with Baba Software an all-in-one recruitment software for business owners, hiring managers & recruitment agencies. Start a free trial today!">
    <meta name="keywords" content="HR Software, HR Software Solutions, HR Systems">
    <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="static-assets/css/Hiring.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">

    
    <!-- web-fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gabriela&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Overlock&display=swap" rel="stylesheet">

    <?php include 'header.php';?>

    <section class="app_banner_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="app_banner_contentmt mt_40">
                        <h2 class="f_p f_700 f_size_50 w_color mb_20 wow fadeInLeft h2-font" style="font-family: 'Bree Serif', serif;">All Your<BR> Employee Information <br>in One Place.</h2>
                        <br>
                        <center><a href="http://baba.software/payroll/register_subscriber" class="btn_hover mt_30 app_btn wow fadeInLeft">Get Started</a></center>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="app_img">
                        <!--<img class="app_screen one wow fadeInDown animated" src="static-assets/images/HR/01.png" alt="">-->
                        <!--<img class="app_screen two wow fadeInDown animated" src="static-assets/images/HR/03.png" alt="">-->
                        <!--<img class="app_screen three wow fadeInDown animated" src="static-assets/images/HR/02.png" alt="" >-->
                        <!--<img class="mobile" src="static-assets/images/HR/app.png" alt="">-->
                           <img class="mobile img-banner img-margin" src="static-assets/images/HR/hiring.png" alt="" style="    margin-left: -8em;">
                    </div>
                </div>
            </div>
        </div>
    </section>


        <section class="saas_featured_area sec_pad dk_bg_one">
            <div class="square_box box_one"></div>
            <div class="square_box box_two"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="saas_featured_content pr_70 mt_60">
                            <h2 style="color: #000;font-size: 20px;">Human Resource Management Software - Solutions to meet the need of HR</h2>
                            <p style="color: #000;"><b>Human resources</b> are the most valuable asset of any organization. The key behind the success of any organization is its motivated employees. HRMS is a software solution that provides various modules for Personnel Management, <b>Human Resource Development</b>, Payroll, Organization Structure and Records of Attendance.</p>
                            <img src="static-assets/images/HR/hr1.png" class="img-banner" alt="HR Software" style="    float: right;">
                        </div>
                    </div>
                    <div class="col-lg-7 saas_featured_info">
                        <div class="row mb_30">
                             <h2 style="color: #000;font-size: 20px;">Why is HRMS advantageous for an organization</h2>
                            <div class="col-lg-6 col-md-6">
                                <div class="saas_featured_item s_featured_one wow fadeInUp">
                                    <i class="ti-package f_size_30"></i>
                                    <h6 class="mt_30 mb_20">Functions of an organization</h6>
                                    <p class="mb-0">It is important to plan and manage the primary functions of an organization. HRMS does this task efficiently. </p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="saas_featured_item s_featured_two wow fadeInUp">
                                    <i class="ti-cloud-down f_size_30"></i>
                                    <h6 class="mt_30 mb_20">Selection of employees</h6>
                                    <p class="mb-0">HRMS helps an organization in the selection of suitable employees. It has replaced written tests with online tests.</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="saas_featured_item s_featured_three wow fadeInUp">
                                    <i class="ti-files f_size_30"></i>
                                    <h6 class="mt_30 mb_20">Manpower</h6>
                                    <p class="mb-0">HRMS provides assistance for handling manpower of an organization.</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="saas_featured_item s_featured_four wow fadeInUp">
                                    <i class="ti-server f_size_30"></i>
                                    <h6 class="mt_30 mb_20">Employees' skills</h6>
                                    <p class="mb-0">Evaluating the performance of employees at different levels and monitoring their number of productive hours have become easier through HRMS.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="saas_features_area_three bg_color sec_pad" style="padding: 3em;background: #e4daff;">
            <div class="container">
                <!-- <div class="sec_title text-center mb_70 wow fadeInUp">
                    <h2 class="f_p f_size_30 l_height50 f_600 t_color">Our great services</h2>
                    <p class="f_400 f_size_16">Skive off mush victoria sponge super lavatory it's all gone to pot<br> knees up fanny around vagabond</p>
                </div> -->
                <div class="row mb_30 new_service">
                    <div class="col-lg-4 col-sm-6">
                        <div class="saas_features_item text-center wow fadeInUp">
                            <div class="number">1</div>
                            <div class="separator"></div>
                            <div class="new_service_content">
                                <center><img src="static-assets/images/HR/icon1.png" alt=""></center>
                                <h4 class="f_size_20 f_p t_color f_500 fn_20" style="">All employee information in one place</h4>
                                <p class="f_400 f_size_15 mb-0">Baba consolidates all of the employee information across the internet into a secure and centralized information store that is easily available from anywhere. All your employee information can easily be collected, imported and stored in the database.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="saas_features_item text-center wow fadeInUp">
                            <div class="number">2</div>
                            <div class="separator"></div>
                            <div class="new_service_content">
                                <center><img src="static-assets/images/HR/icon2.png" alt=""></center>
                                <h4 class="f_size_20 f_p t_color f_500 fn_50" style="">Beautiful Employee Profiles</h4>
                                <p class="f_400 f_size_15 mb-0">No need to remember and grope around hundred different places before finalizing payroll.
                                                                <br>All decisions required to process payroll are consolidated in one screen.
                                                                <br><br>Pending activities and reconciliation of Payroll right where you need them.
                                                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="saas_features_item text-center wow fadeInUp">
                            <div class="number">3</div>
                            <div class="separator"></div>
                            <div class="new_service_content">
                                <center><img src="static-assets/images/HR/icon3.png" alt=""></center>
                                <h4 class="f_size_20 f_p t_color f_500 fn_col" style="">Employee Documents</h4>
                                <p class="f_400 f_size_15 mb-0">Baba HR software comes with employee document management system that would allow the HR team or employees themselves to upload and maintain Identity, Education, Work Experience and Internal performance docs in one place. You can define permissions on who has access to these documents.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="prototype_service_area_three bg_color">
        <div class="container">
            <div class="prototype_service_info">
                <div class="symbols-pulse active">
                    <div class="pulse-1"></div>
                    <div class="pulse-2"></div>
                    <div class="pulse-3"></div>
                    <div class="pulse-4"></div>
                    <div class="pulse-x"></div>
                </div>
                <h2 class="f_size_30 f_600 t_color3 l_height45 text-center mb_90 wow fadeInUp fn_wo" style="">What HR Baba does for you ?</h2>
                <p style="font-size:19px;text-align: center;">HR Baba provides you with a complete solution for managing the entire employee life cycle. As your business grows and your needs evolve, select these HR solutions to tackle your specific HR management challenges.</p>
                <br>
                <div class="row p_service_info" style="text-align:center;">
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pr_70 wow fadeInLeft">
                            <center><div class="icon icon_one"><i class="fas fa-users-cog"></i></div></center>
                            <h5 class="f_600 f_p t_color3">Retain your best people and grow them</h5>
                            <p class="f_400">Save time, reduce employee turnover and easily identify internal leadership candidates.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pl_50 pr_20 wow fadeInLeft">
                        <center><div class="icon icon_one"><i class="fas fa-business-time"></i></div></center>
                            <h5 class="f_600 f_p t_color3">Manage time and attendance</h5>
                            <p class="f_400">Improve your entire workforce's time management and cover all types of employee absences.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pl_70 wow fadeInLeft">
                        <center><div class="icon icon_one"><i class="fas fa-database"></i></div></center>
                            <h5 class="f_600 f_p t_color3">Go paperless</h5>
                            <p class="f_400">Choose one tool to manage all HR processes and integrate payroll, compensation, performance reviews, self-service employees, and more.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pr_70 wow fadeInLeft">
                        <center><div class="icon icon_one"><i class="fas fa-hiking"></i></div></center>
                            <h5 class="f_600 f_p t_color3">Hiring the best talent</h5>
                            <p class="f_400">Nothing is as expensive as hiring the wrong employee. Our HR Baba recruiting and onboarding solution, and its self-service tools, automates and streamlines recruiting, hiring, and onboarding employees.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pl_50 pr_20 wow fadeInLeft">
                        <center><div class="icon icon_one"><i class="fas fa-chart-line"></i></div></center>
                            <h5 class="f_600 f_p t_color3">Compensation</h5>
                            <p class="f_400">Manage your employee benefits with Simple, self-service time tracking, paid time off, and benefits tracking to make paying your employees on time and accurately an easy and delightful experience.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pl_70 wow fadeInLeft">
                        <center><div class="icon icon_one"><i class="fas fa-info"></i></div></center>
                            <h5 class="f_600 f_p t_color3">Make smarter decisions about your people</h5>
                            <p class="f_400">Act on precise, rich workforce analytics and employee information about their skills, demographics, insurance, and more.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="action_area_two">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 d-flex align-items-center">
                    <div class="action_content">
                        <h2 class="f_600 h_60 h2-hr" style="">A complete on-premise human resource management solution that will help you maximize every amount you invest in your employees.</h2>
                        <center><a href="http://baba.software/payroll/register_subscriber" class="btn_three btn_hover wow fadeInLeft">START FOR FREE NOW</a></center>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="action_img wow fadeInRight">
                        <img src="static-assets/images/HR/action_image.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>



        
    <?php include 'footer.php';?>
    </body>
</html>