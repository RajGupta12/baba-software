<style>
        /* sanjiv */
        .powerful-features-single-step i {
            font-size: 24px;
            /* color: #4275ff; */
            color: #1AA2CD;
            height: 80px;
            width: 80px;
            margin-left: 126px;
            margin-bottom: 30px;
            border-radius: 50%;
            text-align: center;
            line-height: 80px;
            display: block;
            border: 2px solid #89da3f;
            transition: .3s all ease-in-out;
        }




        /* sanjiv */

        .powerful-features-single-step {
            border: none;
            outline: none;
            box-shadow: 2px 3px 5px #F5F8FE;
            padding: 20px;
            transition: .3s all ease-in-out;
            background-color:#F5F8FE;
        }

        /* sanjiv */
        .powerful-features-single-step:hover i {
            /* background: #d9e8ff; */
            background-image: linear-gradient(to bottom, #89DA3F, white);
            cursor: pointer;
            background-color:white;
             

        }

        .powerful-features-single-step:hover {
            transform: translateY(-5%);
            box-shadow: 2px 4px 12px grey;
        }

        .powerfull-features-video.position-relative img {
            width: 100%;
        }

        .features-text span {
            font-size: 24px;
            color: #42495b;
        }

        .features-text p {
            margin-bottom: 0;
            margin-top: 7px;
        }

        .features-img img {
            margin-left: -25px;
        }

        .powerfull-features-img {
            position: relative;
        }

        /* sanjiv */
        .powerfull-features-img img {
            width: 300px;
            height: 500px;
            margin-left: 30px;
        }
    </style>
    <!-- Powerful-Features-area-start -->
    <section id="features" class="powerful-features gray-bg pt-120 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section-header mb-80 text-center" style="margin-bottom:30px;">
                        <h2>Powerful Features</h2>
                        <p style="color:#2196F3;">All The Tools Your Team Needs To Transform Your Business</p>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <div class="powerful-features-single-step   " style="margin-bottom:40px;">
                        <i class="fal fa fa-share-alt mt-10  "></i>
                        <div class="features-text text-center fix pr-30">
                            <span>Payroll BABA</span>
                            <p>Calculate salary in minutes and transfer it to employees bank accounts directly from Baba Payroll.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <div class="powerful-features-single-step mb-70" style="margin-bottom:40px;">
                        <i class="fal fa fa-anchor mt-10  "></i>
                        <div class="features-text text-center fix pr-30">
                            <span>CRM Baba</span>
                            <p> Mobile and social CRM tools offered by Baba CRM put you exactly where your customers are.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <div class="powerful-features-single-step mb-70" style="margin-bottom:40px;">
                        <i class="fal fa fa-bullhorn mt-10  "></i>
                        <div class="features-text text-center fix pr-30">
                            <span>ATS Baba</span>
                            <p>A single core solution to streamlines the entire hiring process.</p>
                        </div>

                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <div class="powerful-features-single-step    ">
                        <i class="fas fa-bow-arrow mt-10  "></i>
                        <div class="features-text pl-30 text-center
                         fix">
                            <span>LMS Baba</span>
                            <p>E-learning systems provides access to materials and courses for training and development purposes.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="powerful-features-single-step mb-70">
                        <i class="fal fa fa-spade mt-10  "></i>
                        <div class="features-text text-center pl-30 fix">
                            <span>AR Baba</span>
                            <p>Managing your Accounts Receivables effectively.</p>
                        </div>

                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <div class="powerful-features-single-step mb-70">
                        <i class="fal fas fa-laptop-code mt-10  "></i>
                        <div class="features-text text-center pl-30 fix">
                            <span>Sales Baba</span>
                            <p>Monitor and manage your sales anytime, from anywhere.</p>
                        </div>

                    </div>
                </div>
                <!-- <div class="col-xl-4 col-lg-4">
                    <div class="powerful-features-single-step mb-70 mt-80">
                        <i class="fal fa-share-alt mt-10 f-right"></i>
                        <div class="features-text text-right fix pr-30">
                            <span>Easy Instalations</span>
                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>

                    <div class="powerful-features-single-step mb-70">
                        <i class="fal fa-anchor mt-10 f-right"></i>
                        <div class="features-text text-right fix pr-30">
                            <span>Real Time Customizat </span>
                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>

                    <div class="powerful-features-single-step mb-70">
                        <i class="fal fa-bullhorn mt-10 f-right"></i>
                        <div class="features-text text-right fix pr-30">
                            <span>Customer Support</span>
                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>

                    </div>
                </div> -->

                <!-- <div class="col-xl-4 col-lg-4">
                    <div class="powerfull-features-img">
                        <img src="http://localhost/uploads/gallery/powerfull-features01.png" alt="">
                    </div>
                </div> -->

                <!-- <div class="col-xl-4 col-lg-4">
                    <div class="powerful-features-single-step mb-70 mt-80">
                        <i class="fal fa-bow-arrow mt-10 f-left"></i>
                        <div class="features-text pl-30 fix">
                            <span>Easy Editable</span>
                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>


                    <div class="powerful-features-single-step mb-70">
                        <i class="fal fa-spade mt-10 f-left"></i>
                        <div class="features-text pl-30 fix">
                            <span>Clean &amp; Unique Design</span>
                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>

                    </div>

                    <div class="powerful-features-single-step mb-70">
                        <i class="fal fa-laptop-code mt-10 f-left"></i>
                        <div class="features-text pl-30 fix">
                            <span>Clean Code</span>
                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>

                    </div>

                </div> -->

            </div>
        </div>
    </section>
    <!-- Powerful-Features-area-end -->
    <!-- Powerful-Features-video-area-start -->
    <!-- <section class="powerful-features-video pt-205 pb-130">
        <div class="container">
            <div class="powerfull-features-video position-relative">
                <img src="http://localhost/Ultimate_SaaS/uploads/gallery/powerfull-features-video.jpg" alt="">
                <a href="https://www.youtube.com/watch?v=odRqfyYijc4" class="video-icon popup-video"><i class="fal fa-play"></i></a>
            </div>
        </div>
    </section> -->
    <!-- Powerful-Features-video-area-end -->