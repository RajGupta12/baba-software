
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Partner with baba | Baba Partner Program</title>
      <meta name="description" content="Explore Baba flexible partnership programs to connect with new opportunities and leverage resources for accelerated business growth.">
      <meta name="keywords" content="consulting, sales, technical services, presentation software, productivity applications, CRM, project management, payroll, recruitment, LMS">
        <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>

      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="on" http-equiv="cleartype"/>

      <meta property="og:url" content="">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="">
      <meta property="og:description" content="">

      <meta property="og:image" content="" />
      <meta name="og:image" content="" />

      <meta name="twitter:title" content="" />
      <meta name="twitter:description" content="" />
      <meta property="twitter:image" content="" />
      <meta content="true" name=""/>
      <meta content="320" name=""/>
      <meta content="yes" name=""/>
      <meta content="black-translucent" name=""/>
      <meta content="telephone=no" name=""/>
      <meta content="address=no" name=""/>
      <?php include 'header.php';?>
      
<section id="" data-scroll-target="#" class="first-fold align-center pattern-gradient-light" >
  <div class="container  banner-content l-banner " data-scroll-target="">     
    <h1>Connect with Baba Partner</h1>
    <p class="sub-text">Are you looking for company services, training, or need to customize Baba products? Baba partners can help.</p>
    <div class="mt-md">
      <a title="" class="button button--solid " id="" target="" href=""> become a partner</a>
    </div>
  </div>
</section>
    
<section id="" class="l-section" data-scroll-target="">
  <div class="l-page container l-grid">
    <div class="l-section-heading">   
      <h2 class="align-center">Baba Software Partnership Opportunities</h2>
      <p class="align-center">Explore our flexible partnership programs to connect with new opportunities and leverage resources for accelerated business growth.</p>
    </div> 

  <div class="row">
    <div class="l-col-widget col-sm-4">   
      <div class="icon-big fn-big" style="margin: 0 auto; background-image: url(https://website-assets-fw.freshworks.com/attachments/cjsvdeg0u03htgmg0v84uofd9-solution.svg);"></div>
      <div class="align-center  ">
      <h6>Solution Partner</h6>
      <p>Leverage your technical and sales capabilities to consult, sell, customize and implement Baba Software products for businesses globally.</p>
      <!--p><a href=""><span class="forward--link link mt-xs">Learn more</span></a></p-->
      </div>
    </div>
    <div class="l-col-widget col-sm-4">
      <div class="icon-big " style="margin: 0 auto; background-image: url(https://website-assets-fw.freshworks.com/attachments/cjsvdi6od03pggmg0y3m70d0u-isv-c1509dc3.svg);"></div>
      <div class="align-center  ">
      <h6>ISV Partner</h6>
      <p>Integrate your offerings with any of the Baba Software products to create combined solutions that deliver ‘wow’ moments for customers.</p>
      <!--p><span class="forward--link link mt-xs"><a href="">Learn more</a></span></p-->
      </div>
    </div>
    <div class="l-col-widget col-sm-4">
      <div class="icon-big " style="margin: 0 auto; background-image: url(https://website-assets-fw.freshworks.com/attachments/cjsvdeg5003eeghg0rx7llsvc-affiliate.svg);"></div>
      <div class="align-center  ">
      <h6>Affiliate Partner</h6>
      <p>Recommend our customer engagement platform to your network and reap rich rewards at minimum investment.</p>
      <!--p><span class="forward--link link mt-xs"><a href="">Learn more</a></span></p-->
      </div>
    </div>
  </div>
  </div>
</section>


<section id="106273310059320" class="section-bucket l-section gradient-light " data-scroll-target="">

<div class="feature-screenshot-left ">
  <div class="l-page container">
    <div class="row">
      <div class=" feature-screenshot-left__content col-md-5">
        <h2 class="feature-heading">Build and grow with the Baba Software Marketplace</h2>
        <p>Build, market, and sell apps through our marketplace by joining the Baba Software Developer Community. You’ll be at the forefront of cutting-edge technology, building tools to help thousands of businesses worldwide. Anyone who builds technology, whether an individual developer, startup, or ISV, can join our Baba Software marketplace to develop on or integrate with the Baba Software products and to distribute their technology to customers.</p>
        <!--p class="forward--link link mt-xs"><a href="">Learn more about the Marketplace</a></p-->
      </div>
      <div class="feature-screenshot-left__media feature-screenshot__media-container col-md-7">
        <div class="lazy-image-wrapper loading-optimized" style=" padding-top: 79.5%">
          <img class="original-image fadeIn" alt="" title="" src="static-assets/images/sample/apps-vendor.png" srcset="" data-src="" data-srcset=""/>
        </div>
      </div>
    </div>
  </div>
</div>

</section>

      
<section id="51573812234670" class="section-bucket l-section  " data-scroll-target="">
      
<div class="feature-screenshot-right ">
  <div class="l-page container">
    <div class="row">
      <div class=" feature-screenshot-right__content col-md-7">     
        <h2 class="feature-heading">Strategic partnerships that power your businesses</h2>
        <p>Baba Software services and solutions are bolstered by strategic alliances, with leading technology partners, that enhance business value and accelerate influence across global markets. Through these close partnerships, Baba Software drives focus on highly innovative solutions and customized co-selling initiatives and joint GTM activities.</p>
      </div>
      <div class="col-md-5">
          <img class="original-image fadeIn" alt="" title="" src="static-assets/images/sample/alliance-partners.png" srcset="" data-src="" data-srcset=""/>
      </div>
    </div>
  </div>
</div>

</section>
   
<section id="" data-scroll-target="#" class=" align-center pattern-light    " >
  <div class="container  banner-content l-banner " data-scroll-target="">   
    <h2>Baba Software Partner Directory</h2>
    <p class="sub-text">Talk to the specialists and get all the help you need. Our partners across the world will provide consulting, sales, and technical services, recommendations on Apps and integrations for Baba Software solutions, implementation and customization services, and more.</p>
    <div class="mt-md">
      <a title="" class="button button--ghost " id="" target="" href=""> find a partner</a>
    </div>
  </div>
</section>

<?php include 'footer.php';?>
  </body>
</html>
