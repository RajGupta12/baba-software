<!-- blog-area-start -->
<section id="latest-blog" class="blog-area pt-120 pb-65">
    <div class="container">
        <!-- Section-header start -->
        <div class="section-header text-center mb-80">
            <h2>Latest Blog</h2>

            <p>Lorem ipsum dolor sit amet consectetur adipie</p>

        </div>

        <!-- Section-header end -->
        <div class="row">
            <div class="col-xl-4 col-lg-4">
                <!-- Blog-wrapper start -->
                <div class="blog-wrapper mb-30">
                    <div class="blog-img"> <img src="http://localhost/Ultimate_SaaS/uploads/gallery/blog01.jpg" alt="blog01"> </div>

                    <div class="blog-text">
                        <h4><a href="#">iOS Performance Tricks Make Your App Feel Performant</a></h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide idunt utabore et dolore magna aliqua.</p>

                        <hr class="mb-20">

                        <div class="block-link"> <span><i class="fal fa-user"></i>David Watson</span> <span><i class="fal fa-calendar-alt"></i>15 Apr 2019</span> </div>

                    </div>

                </div>

                <!-- Blog-wrapper end -->
            </div>

            <div class="col-xl-4 col-lg-4">
                <!-- Blog-wrapper start -->
                <div class="blog-wrapper mb-30">
                    <div class="blog-img"> <img src="http://localhost/Ultimate_SaaS/uploads/gallery/blog02.jpg" alt="blog01"> </div>

                    <div class="blog-text">
                        <h4><a href="#">New JavaScript Feature That Will Change How Regex</a></h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide idunt utabore et dolore magna aliqua.</p>

                        <hr class="mb-20">

                        <div class="block-link"> <span><i class="fal fa-user"></i>David Watson</span> <span><i class="fal fa-calendar-alt"></i>15 Apr 2019</span> </div>

                    </div>

                </div>

                <!-- Blog-wrapper end -->
            </div>

            <div class="col-xl-4 col-lg-4">
                <!-- Blog-wrapper start -->
                <div class="blog-wrapper mb-30">
                    <div class="blog-img"> <img src="http://localhost/Ultimate_SaaS/uploads/gallery/blog03.jpg" alt="blog01"> </div>

                    <div class="blog-text">
                        <h4><a href="#">Webhosting Compared Test The Uptime Of Hosts</a></h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide idunt utabore et dolore magna aliqua.</p>

                        <hr class="mb-20">

                        <div class="block-link"> <span><i class="fal fa-user"></i>David Watson</span> <span><i class="fal fa-calendar-alt"></i>15 Apr 2019</span> </div>

                    </div>

                </div>

                <!-- Blog-wrapper end -->
            </div>

            <div class="col-xl-4 col-lg-4">
                <!-- Blog-wrapper start -->
                <div class="blog-wrapper mb-30">
                    <div class="blog-img"> <img src="http://localhost/Ultimate_SaaS/uploads/gallery/blog03.jpg" alt="blog01"> </div>

                    <div class="blog-text">
                        <h4><a href="#">Webhosting Compared Test The Uptime Of Hosts</a></h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide idunt utabore et dolore magna aliqua.</p>

                        <hr class="mb-20">

                        <div class="block-link"> <span><i class="fal fa-user"></i>David Watson</span> <span><i class="fal fa-calendar-alt"></i>15 Apr 2019</span> </div>

                    </div>

                </div>

                <!-- Blog-wrapper end -->
            </div>

            <div class="col-xl-4 col-lg-4">
                <!-- Blog-wrapper start -->
                <div class="blog-wrapper mb-30">
                    <div class="blog-img"> <img src="http://localhost/Ultimate_SaaS/uploads/gallery/blog02.jpg" alt="blog01"> </div>

                    <div class="blog-text">
                        <h4><a href="#">Webhosting Compared Test The Uptime Of Hosts</a></h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide idunt utabore et dolore magna aliqua.</p>

                        <hr class="mb-20">

                        <div class="block-link"> <span><i class="fal fa-user"></i>David Watson</span> <span><i class="fal fa-calendar-alt"></i>15 Apr 2019</span> </div>

                    </div>

                </div>

                <!-- Blog-wrapper end -->
            </div>

            <div class="col-xl-4 col-lg-4">
                <!-- Blog-wrapper start -->
                <div class="blog-wrapper mb-30">
                    <div class="blog-img"> <img src="http://localhost/Ultimate_SaaS/uploads/gallery/blog03.jpg" alt="blog01"> </div>

                    <div class="blog-text">
                        <h4><a href="#">Webhosting Compared Test The Uptime Of Hosts</a></h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide idunt utabore et dolore magna aliqua.</p>

                        <hr class="mb-20">

                        <div class="block-link"> <span><i class="fal fa-user"></i>David Watson</span> <span><i class="fal fa-calendar-alt"></i>15 Apr 2019</span> </div>

                    </div>

                </div>

                <!-- Blog-wrapper end -->
            </div>

        </div>

    </div>

</section> <!-- blog-area-end -->