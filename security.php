<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Online Recruiting Software | Applicant Tracking System - Baba Recruit</title>
    <meta name="description" content="Hire better & faster with Baba, an all-in-one recruitment software for business owners, hiring managers & recruitment agencies. Start a free trial today!">
    <meta name="keywords" content="recruitment system, recruitment management system, application tracking system, best application tracking system, recruitment software, best recruitment software">
    <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <meta content="IE=edge" http-equiv="X-UA-Compatible">

    <?php include 'header.php';?>
        <style>
            .h0,
            .h1,
            .h2,
            .h3,
            .h4,
            .h5,
            .h6,
            .h7,
            .pl-ads .plan-tile-small p.pricing,
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                font-weight: 500;
                color: #3F51B5;
                font-family: Sailec-Medium, Helvetica, sans-serif;
                margin: 10px 0;
            }
            
            p {
                font-family: National-Book, Helvetica, sans-serif;
                font-size: 1rem;
                line-height: 1.75;
                color: #252424;
                margin: 10px 0;
            }
        </style>
        <br>
        <div class="wrappers" style="background: aliceblue;">
            <div class="container">
                <br>
            <div class="title-wthree text-center py-lg-5">
                <h3 class="agile-title" style="font-family: 'Bree Serif', serif;">
              Security
                </h3>
                <span></span>
            </div>

            <center><h5>Introduction</h5></center>
            <p>Baba provides Software as a Service(SaaS) products to millions of users worldwide to solve their business problems. Security is a key component in our offerings, and is reflected in our people, process, and products. This page covers topics like data security, operational security, and physical security to explain how we offer security to our customers.</p>
            <br>

             <center><h5>Overview</h5></center>
            <p>Our security strategy involves the following components</p>
            <ul>
                <li>Organizational security</li>
                <li>Physical security</li>
                <li>Infrastructure security</li>
                <li>Data security</li>
                <li>Identity and access control</li>
                <li>Operational security</li>
                <li>Incident management</li>
                <li>Responsible disclosures</li>
                <li>Vendor management</li>
                <li>Customer controls for security</li>
             
            </ul>
            <br>

            <center><h5>Organizational security</h5></center>
            <p>We have an Information Security Management System (ISMS) in place which takes into account our security objectives and the risks and mitigations concerning all the interested parties. We employ strict policies and procedures encompassing the security, availability, processing, integrity, and confidentiality of customer data.</p>
            <br>


            <h5>Employee background checks</h5>
            <p>Each employee undergoes a process of background verification. We hire reputed external agencies to perform this check on our behalf. We do this to verify their criminal records, previous employment records if any, and educational background. Until this check is performed, the employee is not assigned tasks that may pose risks to users. </p>
            <br>

            <h5>Security Awareness</h5>
            <p>Each employee, when inducted, signs a confidentiality agreement and acceptable use policy, after which they undergo training in information security, privacy, and compliance. Furthermore, we evaluate their understanding through tests and quizzes to determine which topics they need further training in. We provide training on specific aspects of security, that they may require based on their roles.
            <br><br>We educate our employees continually on information security, privacy, and compliance in our internal community where our employees check in regularly, to keep them updated regarding the security practices of the organization. We also host internal events to raise awareness and drive innovation in security and privacy.
            </p>
            <br>

            <h5>Dedicated security and privacy teams</h5>
            <p>We have dedicated security and privacy teams that implement and manage our security and privacy programs. They engineer and maintain our defense systems, develop review processes for security, and constantly monitor our networks to detect suspicious activity. They provide domain-specific consulting services and guidance to our engineering teams.</p>
            <br>

            <h5>Internal audit and compliance</h5>
            <p>We have a dedicated compliance team to review procedures and policies in Baba to align them with standards, and to determine what controls, processes, and systems are needed to meet the standards.This team also does periodic internal audits and facilitates independent audits and assessments by third parties.</p>
            <br>

            <h5>Endpoint security</h5>
            <p>All workstations issued to Baba employees run up-to-date OS version and are configured with anti-virus software. They are configured such that they comply with our standards for security, which require all workstations to be properly configured, patched, and be tracked and monitored by Baba's endpoint management solutions. These workstations are secure by default as they are configured to encrypt data at rest, have strong passwords, and get locked when they are idle. Mobile devices used for business purposes are enrolled in the mobile device management system to ensure they meet our security standards.</p>
            <br>

            <center><h5>Physical security</h5></center>
            <h5>At workplace</h5>
            <p>We control access to our resources (buildings, infrastructure and facilities), where accessing includes consumption, entry, and utilization, with the help of access cards. We provide employees, contractors, vendors, and visitors with different access cards that only allow access strictly specific to the purpose of their entrance into the premises. Human Resource (HR) team establishes and maintains the purposes specific to roles. We maintain access logs to spot and address anomalies. </p>
            <br>

            <h5>At Data Centers</h5>
            <p>At our Data Centers, a co location provider takes responsibility of the building, cooling, power, and physical security, while we provide the servers and storage. Access to the Data Centers is restricted to a small group of authorized personnel. Any other access is raised as a ticket and allowed only after the approval of respective managers. Additional two-factor authentication and biometric authentication are required to enter the premises. Access logs, activity records, and camera footage are available in case an incident occurs.</p>
            <br>

            <h5>Monitoring</h5>
            <p>We monitor all entry and exit movements throughout our premises in all our business centers and data centers through CCTV cameras deployed according to local regulations. Back-up footage is available up to a certain period, depending on the requirements for that location.</p>
            <br>

            <center><h5>Infrastructure security</h5></center>
            <h5>Network security</h5>
            <p>Our network security and monitoring techniques are designed to provide multiple layers of protection and defense. We use firewalls to prevent our network from unauthorized access and undesirable traffic. Our systems are segmented into separate networks to protect sensitive data. Systems supporting testing and development activities are hosted in a separate network from systems supporting Baba's production infrastructure.
            <br><br>We monitor firewall access with a strict, regular schedule. A network engineer reviews all changes made to the firewall everyday. Additionally, these changes are reviewed every three months to update and revise the rules. Our dedicated Network Operations Center team monitors the infrastructure and applications for any discrepancies or suspicious activities. All crucial parameters are continuously monitored using our proprietary tool and notifications are triggered in any instance of abnormal or suspicious activities in our production environment.
            </p>
            <br>

            <h5>Network redundancy</h5>
            <p>All the components of our platform are redundant. We use a distributed grid architecture to shield our system and services from the effects of possible server failures. If there's a server failure, users can carry on as usual because their data and Baba services will still be available to them.
                We additionally use multiple switches, routers, and security gateways to ensure device-level redundancy. This prevents single-point failures in the internal network.
                </p>
            <br>

            <h5>DDoS prevention</h5>
            <p>We use technologies from well-established and trustworthy service providers to prevent DDoS attacks on our servers. These technologies offer multiple DDoS mitigation capabilities to prevent disruptions caused by bad traffic, while allowing good traffic through. This keeps our websites, applications, and APIs highly available and performing.</p>
            <br>

            <h5>Server hardening</h5>
            <p>All servers provisioned for development and testing activities are hardened (by disabling unused ports and accounts, removing default passwords, etc.). The base Operating System (OS) image has server hardening built into it, and this OS image is provisioned in the servers, to ensure consistency across servers.</p>
            <br>

            <h5>Intrusion detection and prevention</h5>
            <p>Our intrusion detection mechanism takes note of host-based signals on individual devices and network-based signals from monitoring points within our servers. Administrative access, use of privileged commands, and system calls on all servers in our production network are logged. Rules and machine intelligence built on top of this data give security engineers warnings of possible incidents. At the application layer, we have our proprietary WAF which operates on both whitelist and blacklist rules.
                <br><br>At the Internet Service Providers (ISP) level, a multi-layered security approach is implemented with scrubbing, network routing, rate limiting, and filtering to handle attacks from network layer to application layer.This system provides clean traffic, reliable proxy service, and a prompt reporting of attacks, if any. 
                </p>
            <br>

            <center><h5>Data security</h5></center>
            <h5>Secure by design</h5>
            <p>Every change and new feature is governed by a change management policy to ensure all application changes are authorised before implementation into production. Our Software Development Life Cycle (SDLC) mandates adherence to secure coding guidelines, as well as screening of code changes for potential security issues with our code analyser tools, vulnerability scanners, and manual review processes.
            <br><br>Our robust security framework based on OWASP standards, implemented in the application layer, provides functionalities to mitigate threats such as SQL injection,Cross site scripting and application layer DOS attacks. 
            </p>
            <br>

            <h5>Data isolation</h5>
            <p>Our framework distributes and maintains the cloud space for our customers. Each customer's service data is logically separated from other customers' data using a set of secure protocols in the framework. This ensures that no customer's service data becomes accessible to another customer.
            The service data is stored on our servers when you use our services. Your data is owned by you, and not by Baba. We do not share this data with any third-party without your consent.
            </p>
            <br>

            <h5>Encryption</h5>
            <p><strong> transit:</strong> All customer data transmitted to our servers over public networks is protected using strong encryption protocols.We mandate all connections to our servers use Transport Layer Security (TLS 1.2/1.3) encryption with strong ciphers, for all connections including web access,API access,our mobile apps, and IMAP/POP/SMTP email client access. This ensures a secure connection by allowing the authentication of both parties involved in the connection, and by encrypting data to be transferred.Additionally for email, our services leverages opportunistic TLS by default.TLS encrypts and delivers email securely, mitigating eavesdropping between mail servers where peer services support this protocol.
                We have full support for Perfect Forward Secrecy (PFS) with our encrypted connections, which ensures that even if we were somehow compromised in the future, no previous communication could be decrypted. We have enabled HTTP Strict Transport Security header (HSTS) to all our web connections.This tells all modern browsers to only connect to us over an encrypted connection, even if you type a URL to an insecure page at our site. Additionally, on the web we flag all our authentication cookies as secure.
                <strong> rest: </strong> data at rest is encrypted using 256-bit Advanced Encryption Standard (AES).We own and maintain the keys using our in-house Key Management Service (KMS). We provide additional layers of security by encrypting the data encryption keys using master keys. The master keys and data encryption keys are physically separated and stored in different servers with limited access.
                <br><br>We encrypt data at two major levels:
                <br>

                1. Application layer encryption: Encryption of relational databases, file stores, backup, logs, and cache<br>
                2. Hardware based full disk encryption<br>
                Please click here for detailed information about encryption at Baba.<br>
                </p>
            <br>

            <h5>Data retention and disposal</h5>
            <p>We hold the data in your account as long as you choose to use Baba Services. Once you terminate your Baba user account, your data will get deleted from the active database during the next clean-up that occurs once every 6 months. The data deleted from the active database will be deleted from backups after 3 months. In case of your unpaid account being inactive for a continuous period of 120 days, we will terminate it after giving you prior notice and option to back-up your data.
            <br><br>A verified and authorized vendor carries out the disposal of unusable devices. Until such time, we categorize and store them in a secure location. Any information contained inside the devices is formatted before disposal. We degauss failed hard drives and then physically destroy them using a shredder. We crypto-erase and shred failed Solid State Devices (SSDs).
            </p>
            <br>

            <center><h5>Identity and Access control</h5></center>
            <h5>Single Sign-On (SSO)</h5>
            <p>Baba offers single sign-on (SSO) that lets users access multiple services using the same sign-in page and authentication credentials. When you sign in to any Baba service, it happens only through our integrated Identity and Access Management (IAM) service.We also support SAML for single sign-on that makes it possible for customers to integrate their company's identity provider like LDAP,ADFS when they login to Baba services
            <br><br>SSO simplifies login process, ensures compliance, provides effective access control and reporting, and reduces risk of password fatigue, and hence weak passwords.
            </p>
            <br>

            <h5>Multi-Factor Authentication</h5>
            <p>It provides an extra layer of security by demanding an additional verification that the user must possess, in addition to the password. This can greatly reduce the risk of unauthorized access if a user’s password is compromised. Currently, different modes like biometric Touch ID or Face ID, Push Notification, QR code, and Time-based OTP are supported.</p>
            <br>

            <h5>Administrative access</h5>
            <p>We employ technical access controls and internal policies to prohibit employees from arbitrarily accessing user data. We adhere to the principles of least privilege and role-based permissions to minimize the risk of data exposure.
            <br><br>Access to production environments is maintained by a central directory and authenticated using a combination of strong passwords, two-factor authentication, and passphrase-protected SSH keys. Furthermore, we facilitate such access through a separate network with stricter rules and hardened devices. Additionally, we log all the operations and audit them periodically.
            </p>
            <br>

            <center><h5>Operational security</h5></center>
            <h5>Logging and Monitoring</h5>
            <p>We monitor and analyse information gathered from services, internal traffic in our network, and usage of devices and terminals. We record this information in the form of event logs, audit logs, fault logs, administrator logs, and operator logs. These logs are automatically monitored and analyzed to a reasonable extent that helps us identify anomalies such as unusual activity in employees’ accounts or attempts to access customer data. We store these logs in a secure server isolated from full system access, to manage access control centrally and ensure availability. 
            <br>Detailed audit logging covering all update and delete operations performed by the user are available to the customers in every Baba service.
            </p>
            <br>

            <h5>Vulnerability management</h5>
            <p>We have a dedicated vulnerability management process that actively scans for security threats using a combination of certified third-party scanning tools and in-house tools, and with automated and manual penetration testing efforts. Furthermore, our security team actively reviews inbound security reports and monitors public mailing lists, blog posts, and wikis to spot security incidents that might affect the company’s infrastructure.
                <br><br>Once we identify a vulnerability requiring remediation, it is logged, prioritized according to the severity, and assigned to an owner. We further identify the associated risks and track the vulnerability until it is closed by either patching the vulnerable systems or applying relevant controls.
                </p>
            <br>

            <h5>Malware and spam protection</h5>
            <p>We scan all user files using our automated scanning system that’s designed to stop malware from being spread through Baba's ecosystem. Our custom anti-malware engine receives regular updates from external threat intelligence sources and scans files against blacklisted signatures and malicious patterns. Furthermore, our proprietary detection engine bundled with machine learning techniques, ensures customer data is protected from malware. 
            <br><br>Baba supports Domain-based Message Authentication, Reporting, and Conformance (DMARC) as a way to prevent spam. DMARC uses SPF and DKIM to verify that messages are authentic. We also use our proprietary detection engine for identifying abuse of Baba services like phishing and spam activities. Additionally, we have a dedicated anti-spam team to monitor the signals from the software and handle abuse complaints. For more information, click here
            </p>
            <br>

            <h5>Backup</h5>
            <p>We run full backups once a week and incremental backups everyday. Backup data in a DC is stored in the same location and encrypted at rest, as the original data. We additionally restore and validate backups every week. All backed up data is retained for three months.
            <br><br>If a customer requests for data recovery within the retention period, we will restore their data from the backup and make it available to them.
            </p>
            <br>

            <h5>Disaster recovery and business continuity</h5>
            <p>Application data is stored on resilient storage that is replicated across data centers. Data in the primary DC is replicated in the secondary in near real time. In case of failure of the primary DC, secondary DC takes over and the operations are carried on smoothly with minimal or no loss of time. Both the centers are equipped with multiple ISPs.
            <br>We have power back-up, temperature control systems and fire-prevention systems as physical measures to ensure business continuity. These measures help us achieve resilience. In addition to the redundancy of data, we have a business continuity plan for our major operations such as support and infrastructure management.
            </p>
            <br>

            <center><h5>Incident Management</h5></center>
            <h5>Reporting</h5>
            <p>We have a dedicated incident management team. We notify you of the incidents in our environment that apply to you, along with suitable actions that you may need to take. We track and close the incidents with appropriate corrective actions. Whenever applicable, we will provide you with necessary evidences regarding incidents that apply to you. Furthermore, we implement controls to prevent recurrence of similar situations.
            <br><br>We respond to the security or privacy incidents you report to us through support@baba.software, with high priority. For general incidents, we will notify users through our blogs, forums, and social media. For incidents specific to an individual user or an organization, we will notify the concerned party through email (using their primary email address of the Organisation administrator registered with us). 
            </p>
            <br>

            
            <h5>Breach notification</h5>
            <p>As data controllers, we notify the concerned Data Protection Authority of a breach within 72 hours after we become aware of it, according to the General Data Protection Regulation (GDPR). Depending on specific requirements, we notify the customers too, when necessary. As data processors, we inform the concerned data controllers without undue delay. </p>
            <br>

            <center><h5>Vendor and Third-party supplier management</h5></center>
            <p>We evaluate and qualify our vendors based on our vendor management policy. We onboard new vendors after understanding their processes for delivering us service, and performing risk assessments. We take appropriate steps to ensure our security stance is maintained by establishing agreements that require the vendors to adhere to confidentiality, availability, and integrity commitments we have made to our customers. We monitor the effective operation of the organization’s process and security measures by conducting periodic reviews of their controls.</p>
            <br>

            <center><h5>Customer controls for security</h5></center>
           
            <ul>
                <li>So far, we have discussed what we do to offer security on various fronts to our customers.Here are the things that you as a customer can do to ensure security from your end:</li>
                <li>Choose a unique, strong password and protect it.</li>
                <li>Use multi-factor authentication</li>
                <li>Use the latest browser versions, mobile OS and updated mobile applications to ensure they are patched against vulnerabilities and to use latest security features</li>
                <li>Exercise reasonable precautions while sharing data from our cloud environment.</li>
                <li>Classify your information into personal or sensitive and label them accordingly.</li>
                <li>Monitor devices linked to your account, active web sessions, and third-party access to spot anomalies in activities on your account, and manage roles and privileges to your account.</li>
                <li>Be aware of phishing and malware threats by looking out for unfamiliar emails, websites, and links that may exploit your sensitive information by impersonating Baba or other services you trust.</li>
            
             
            </ul>
            <br>

          

             <center><h5>Conclusion</h5> </center>
            <p>Security of your data is your right and a never-ending mission of Baba. We will continue to work hard to keep your data secure, like we always have. For any further queries on this topic, feel free to contact us at support@baba.software.</p>
            <br>
          
          
        </div>
    </div>
<?php include 'footer.php';?>
</body>
</html>