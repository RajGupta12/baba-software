
<!DOCTYPE html>
<html lang="en">
  <head>
<meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>About us - Baba</title>
      <meta name="description" content="We give service businesses a system to run their operations, streamlining client work, automating processes and letting professionals to do the work they love.">
      <meta name="keywords" content="payroll management, payroll software, CRM software, Customer relationship management system, recruitment software, Application tracking system, Corporate LMS, online e-learning software, project management, project management software">
        <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>

      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="on" http-equiv="cleartype"/>

      <meta property="og:url" content="">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="">
      <meta property="og:description" content="">

      <meta property="og:image" content="" />
      <meta name="og:image" content="" />

      <meta name="twitter:title" content="" />
      <meta name="twitter:description" content="" />
      <meta property="twitter:image" content="" />
      <meta content="true" name=""/>
      <meta content="320" name=""/>
      <meta content="yes" name=""/>
      <meta content="black-translucent" name=""/>
      <meta content="telephone=no" name=""/>
      <meta content="address=no" name=""/>
      <?php include 'header.php';?>

      
<section class="l-section first-fold banner-badges-gradient" data-scroll-target="" id ="">
  <div class="l-article document">     
  <h2 class="align-center">About us</h2>
  <p class="mt-sm">Baba Software offers you with a low price, reliable and secure foundation to use when building and delivering customer-specific software as a service solution. The software helps companies build a successful business by providing valuable business, technical, marketing, and go-to-market support.</p>
  <p class="mt-sm">In Baba Software tools and services are working in the same place as your team. Information puts inboxes and flows into channels of shared teams. Using the # 1 platform, we assist our clients find fresh paths to achievement. Our idea is bring all your teams, projects and clients under one roof.</p>
  <p class="mt-sm">At Baba Software, we believe your business deserves better software - software that’s ready to go, easy to setup and use, and requires minimal customization. All of our products live up to this promise and are backed by our world-class support. And the best part is, you don’t have to break the bank to get them working.</p>
  </div>
</section>

<section class="l-section first-fold banner-badges-gradient" data-scroll-target="" id ="">
  <div class="l-article document">
    <h2 class="align-center">Mission</h2>
    <p>Our mission is to help people see data in new ways, discover insights, unlock endless possibilities to make real work happen.</p>

    <h2 class="align-center">Vision</h2>
    <h6>A private company with a public vision</h6>
    <p>A  foundational software for building a platform for all your projects where business can easily plan, collaborate and deliver projects of all sizes on time using Baba Software with all the right tools put at one place.</p>

    <h6>We mainly work on 4 keys</h6>
      <ul>
        <li>Trust</li>
        <li>Growth</li>
        <li>Innovation</li>
        <li>Equality</li>
      </ul>
  </div> 
</section>      



<?php include 'footer.php';?>

  </body>
</html>
