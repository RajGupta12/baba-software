
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Baba Software | Products</title>
      <meta name="description" content="">
      <meta name="keywords" content="">
        <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>

      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="on" http-equiv="cleartype"/>

      <meta property="og:url" content="">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="">
      <meta property="og:description" content="">

      <meta property="og:image" content="../website-assets-fw.freshworks.com/attachments/cjwkf54sf005pn9g0vf6vj4mq-og-image-b.full.jpg" />
      <meta name="og:image" content="../website-assets-fw.freshworks.com/attachments/cjwkf54sf005pn9g0vf6vj4mq-og-image-b.full.jpg" />

      <meta name="twitter:title" content="" />
      <meta name="twitter:description" content="" />
      <meta property="twitter:image" content="../website-assets-fw.freshworks.com/attachments/cjwkf54sf005pn9g0vf6vj4mq-og-image-b.full.jpg" />
      <meta content="true" name=""/>
      <meta content="320" name=""/>
      <meta content="yes" name=""/>
      <meta content="black-translucent" name=""/>
      <meta content="telephone=no" name=""/>
      <meta content="address=no" name=""/>
      <?php include 'header.php';?>

      
<section id="" data-scroll-target="#" class="first-fold align-center pattern-gradient-light" >
  <div class="container  banner-content l-banner " data-scroll-target="">   
  <h1>Refreshing business software that your teams will love</h1>
  <p class="sub-text">All of our products are ready to go, easy to use and offer great value to any kind of business</p>
  </div>
</section>     

<section class="l-section " data-scroll-target="">
    <div class="l-page container l-grid">
      <div class="product-listing-table">
        <div class="product-wrapper">
            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">BABA Payroll</span-->
                <a class="logo logo-fteam" href="payroll.php"></a>    
                <p>Calculate salary in minutes and transfer it to employees' bank accounts directly from Baba Payroll.</p>   
                <a title="" class="button button--white button--small hover-fdesk" id="" target="" href="payroll.php"> START TRIAL</a>      
                <a href="payroll.php" class ="link forward--link">Learn more</a>
            </div>
            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">Baba CRM</span-->
                <a class="logo logo-fchat" href=""></a>     
                <p>Mobile and social CRM tools offered by Baba CRM put you exactly where your customers are.</p>      
                <a title="" class="button button--white button--small hover-fchat" id="" target="" href="crm-baba.php"> START TRIAL</a>     
              <a href= "crm-baba.php" class ="link forward--link">Learn more</a>
            </div>

            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">BABA Recruitment( ATS)</span-->
                <a class="logo logo-fsales" href="ats.php"></a>    
                <p>Solutions Business Manager is a leading Business Process Management Suite. </p>      
                <a title="" class="button button--white button--small hover-fsales" id="" target="" href="ats.php"> START TRIAL</a>     
                <a href="ats.php" class="link forward--link">Learn more</a>
            </div>
        </div>
      </div>

      <div class="product-listing-table">
        <div class="product-wrapper">
          <div class="product-listing product-listing-col-4 align-center">
            <!--span class="tag tag-product-listing">BABA LMS</span-->
            <a class="logo logo-fservice" href="lms-baba.php"></a>     
            <p>Baba's Guide to Applicant Tracking Systems and how to optimize your resume for ATS. Get hired faster.</p>     
            <a title="" class="button button--white button--small hover-fservice" id="" target="" href="lms-baba.php"> START TRIAL</a>     
            <a href="lms-baba.php" class="link forward--link">Learn more</a>
          </div>

          <div class="product-listing product-listing-col-4 align-center">
            <!--span class="tag tag-product-listing"> BABA iPPM</span-->
            <a class="logo logo-fcaller" href="ppm-baba.php"></a>   
            <p>e-Learning systems provides access to online materials and courses for training and development purposes</p>   
            <a title="" class="button button--white button--small hover-fcaller" id="" target="" href="ppm-baba.php"> START TRIAL</a>     
           <a href="ppm-baba.php" class="link forward--link">Learn more</a>
          </div>

          <div class="product-listing product-listing-col-4 align-center" style="visibility: hidden;">
            <!--span class="tag tag-product-listing">HUMAN RESOURCES SOFTWARE</span-->
              <a class="logo logo-fdesk" href="ppm-baba.php"></a>    
              <p>An employee engagement platform and custom dashboard for futuristic businesses to collaborate</p>     
              <a title="ppm-baba.php" class="button button--white button--small hover-fteam" id="" target="" href="ppm-baba.php"> START TRIAL</a>      
              <a href="ppm-baba.php" class="link forward--link">Learn more</a>
          </div>
        </div>
      </div>

    </div>
</section>


      
<section class="l-section  " data-scroll-target="" data-scroll-target="" id ="">
       
<div id="154616788318707" data-interval="false" class="carousel testimonial-full-width slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
         <div class="carousel-item item active">
						<div class="row testimonial-card">
							<div class="col-md-6 testimonial-photo">
										
<div class="" style="">
  <img class="" data-src="" data-srcset="" src="static-assets/images/sample/what-is-digital-marketing.jpg" srcset="" />    
</div>
  
  <div class="video-popup-modal" id="video-popup-d5i2vkz4kl">
    <div class="video-popup-modal-content">
      <div class="video-popup-close icon-close" id="video-popup-close-d5i2vkz4kl"></div>
      <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
        <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
            <div class="wistia_embed wistia_async_d5i2vkz4kl videoFoam=true" style="height:100%;width:100%">&nbsp;
            </div>
        </div>
      </div>
    </div>
  </div>
									   
  <div class="video-popup-initial-state" id="video-popup-trigger-d5i2vkz4kl" data-video-popup-target="d5i2vkz4kl" >
    <div class="play-button ">
      <span class="icon-play"></span>
    </div>
  </div>

							</div>
							<div class="col-md-6 testimonial-content-outer">
								<div class="testimonial-logo-container">								
<div class="" style="">
  <img class="" data-src="" data-srcset="" src="static-assets/images/sample/logo.jpg" srcset="" />
</div>
								</div>
								<div class="testimonial-content">
									<p class="testimonial-quote">Baba Software is intuitive, it’s easy to customize to make it fit in your company’s branding and the out-of-the-box functionality it develops is really unparalleled. It makes my life a lot easier.</p>
									<div class="author-bio">
										<h6 class="author-name">Dipti</h6> <!-- author-name -->
										<p class="author-designation">Tech Support Manager</p> <!-- author-designation -->
										<p class="author-company">Jobs Exchange</p> <!-- author-company -->
									</div>
								</div>
						  </div>
					  </div>
				 </div>
      
		</div>
		<div class="carousel-controls-wrapper mt-md">
		  <div class="carousel-indicators-wrapper">
			<ol class="carousel-indicators">
					<li data-target="#154616788318707" data-slide-to="0" class="active"></li>
					<li data-target="#154616788318707" data-slide-to="1" class=""></li>
					<li data-target="#154616788318707" data-slide-to="2" class=""></li>
			</ol>
			<a class="left carousel-control" href="#154616788318707" role="button" data-slide="prev">
			  <span class="icon-arrow-button-left" aria-hidden="true"></span>
			</a>
			<a class="right carousel-control" href="#154616788318707" role="button" data-slide="next">
			   <span class="icon-arrow-button-right" aria-hidden="true"></span>
			</a>
		  </div>
		</div>
</div>

</section>      

<section class="l-section" data-scroll-target="" id ="">
  <div class="l-page container">
      <div class="l-section-heading align-center">    
  <h2 class="align-center">Trusted by over <strong>many</strong> businesses around the world</h2>
      </div>
        
<div class="customer-logos">

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/1.gif" srcset=""/>  
        </div>
      </div>
    
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/2.gif" srcset=""/>
        </div>
      </div>
          
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/3.gif" srcset=""/>
        </div>
      </div>      
      
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/4.gif" srcset=""/> 
        </div>
      </div>
     
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/5.gif" srcset=""/>
        </div>
      </div>        
      
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/6.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/7.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/8.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/9.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/10.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/11.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/12.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/13.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/14.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/15.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/16.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/17.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/18.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/19.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/20.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/21.gif" srcset=""/>  
        </div>
      </div>

    </div>
</section>


<?php include 'footer.php';?>
  </body>
</html>
