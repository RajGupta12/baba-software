
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Online Recruiting Software | Applicant Tracking System - Baba Recruit</title>
    <meta name="description" content="Hire better & faster with Baba, an all-in-one recruitment software for business owners, hiring managers & recruitment agencies. Start a free trial today!">
    <meta name="keywords" content="recruitment system, recruitment management system, application tracking system, best application tracking system, recruitment software, best recruitment software">
    <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <?php include 'header.php';?>
    <!-- signin -->
        <section class="signin py-5">
            <div class="container">
                <div class="row main-content-agile" style="margin-top: 7em;">
                    <div class="col-lg-6 col-md-9 mx-auto">	
                        <div class="sub-main-w3 text-center">	
                            <h3>Log In with email</h3>
                            <p class="mt-2 mb-4">Enter your email and password to get Login.</p>
                            <form action="#" method="post">
                                <div class="icon1">
                                    <input placeholder="Username or Email" name="mail" type="email" required="">
                                </div>
                                <div class="icon2">
                                    <input  placeholder="Password" name="Password" type="password" required="">
                                </div><br>
                                <label class="anim">
                                <input type="checkbox" class="checkbox">
                                    <span>Remember Me</span> 
                                    <a href="#">Forgot Password</a>
                                </label> 
                                    <div class="clearfix"></div>
                                <input type="submit" value="Sign in">
                                <p>Don’t know an Account ? <a href="signup.php" class="ml-2"><strong>Create your Account</strong></a></p>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-8 mx-auto">
                    <img src="static-assets/images/favicon/img1.jpg" alt="" class="img-fluid" style="height:500px;"/>
                    </div>
                </div>
            </div>
        </section>
        <!-- //signin -->

    

    <?php include 'footer.php';?>
  </body>
</html>
