
<!DOCTYPE html>
<html lang="en">
  
<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Baba - The one place to run your entire business</title>
      <meta name="google-site-verification" content="Y5g3NYXJGrjrQ1zuN3D2yxDtSy08t00i21dWPjEGrBw" />
      <meta name="google-site-verification" content="4jAcgF28fX2xr0bph0hU7XxmIqWXTtXGQJYT8n8ATGQ" />
      <meta name="description" content="Explore the Baba suite of business software that will make your life easier. Be it Payroll, CRM, Recruitment, LMS, IPPM, BABA Software has a solution for it all.">
      <meta name="keywords" content="Payroll Software, Payroll management, Leave management software, CRM Software, Customer Relationship Management System, best recruitment software, Application tracking system, ATS, e-learning software, enterprise e-learning solution, best project management software">
    <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link href="static-assets/css/index.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://kit.fontawesome.com/a076d05399.js"></script>
    

      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="on" http-equiv="cleartype"/>
      <meta name="google-site-verification" content="FoIxTuI3r__QWjBP-AVwiwtevEn8wErVr0aARIQGv3I" />

      <meta property="og:url" content="">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="">
      <meta property="og:description" content="">

      <meta property="og:image" content="" />
      <meta name="og:image" content="" />

      <meta name="twitter:title" content="" />
      <meta name="twitter:description" content="" />
      <meta property="twitter:image" content="" />
      <meta content="true" name=""/>
      <meta content="320" name=""/>
      <meta content="yes" name=""/>
      <meta content="black-translucent" name=""/>
      <meta content="telephone=no" name=""/>
      <meta content="address=no" name=""/>
 
      <?php include 'header.php';?>

      <style>
     
    .banner {
    background: url(static-assets/images/favicon/ban11.png)no-repeat center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    background-size: cover;
}
      </style>


<!-- banner -->
<div class="banner"  id="home" >
	<div class="layer">
		<div class="container">
			
			<div class="banner-text-w3pvt">
				<!-- banner slider-->
				<div class="csslider infinity" id="slider1">
					<input type="radio" name="slides" checked="checked" id="slides_1" />
					<input type="radio" name="slides" id="slides_2" />
					<input type="radio" name="slides" id="slides_3" />
					<ul class="banner_slide_bg">
						<li>
							<div class="w3ls_banner_txt"><br><br>
                <h3 class="b-w3ltxt text-capitalize mt-md-4 b-h2 h3-class">The one place where everything comes together to run your entire business.</h3>
                <h5 class="h5-font h5-class" >Baba Software makes day-to-day work easier, more transparent and efficient for thousands of companies.</h5>						
							</div>
						</li>
					
					</ul>
					<div class="navigation">
						<div>
							<label for="slides_1"></label>
						</div>
					</div>
				</div>
				<!-- //banner slider-->
			</div>
		</div>
	</div>
</div>
<!-- //banner -->

<!-- about bottom -->
<section class="about-bottom py-5" id="about">
	<div class="container py-md-5 py-3">	
		<div class="row" style="margin-top: 4em;"> 
			<div class="col-lg-6 left-img">
			<img src="static-assets/images/favicon/st11.png" class="img-fluid img-banner" alt="baba-banner-image" style="height: 430px;"/>
			</div> 
			<div class="col-lg-6 mt-lg-0 mt-4">
				<div class="row inner-heading">
					<div class="col-md-2" style="    margin-top: 11px;">
						<span class="fa fa-bullseye"></span>
					</div>
					<div class="col-md-10">
						<h4 class="mt-md-0 mt-2">Every Business Size </h4>
						<p class="mt-3">Solutions for large, midsized and small, fast growing businesses. </p>
					</div>

					<div class="col-md-2 mt-5">
					<span class="fa fa-industry"></span>
					</div>
					<div class="col-md-10 mt-md-5">
						<h4 class="mt-md-0 mt-2">Every Industry </h4>
						<p class="mt-3">Industry-specific functionality spanning a broad range of businesses.</p>
					</div>


					<div class="col-md-2 mt-5">
					<span class="fa fa-user"></span>
					</div>
					<div class="col-md-10 mt-md-5">
						<h4 class="mt-md-0 mt-2">Every Role </h4>
						<p class="mt-3">Solutions addressing the needs of all the team as per their role. </p>
					</div>

					<div class="col-md-2 mt-5">
					<span class="fa fa-desktop"></span>
					</div>
					<div class="col-md-10 mt-md-5">
						<h4 class="mt-md-0 mt-2">Current Software </h4>
						<p class="mt-3">Replacing QuickBooks, Microsoft Dynamics, Sage, SAP and other business systems. </p>
					</div>

					<div class="col-md-2 mt-5">
					<span class="fa fa-check-circle"></span>
					</div>
					<div class="col-md-10 mt-md-5">
						<h4 class="mt-md-0 mt-2">Suite Success </h4>
						<p class="mt-3">Our mission is to make software easier to implement and run.</p>
					</div>
				</div>
			</div>
		</div>
			
	</div>
</section>
<!-- //about bottom -->

<!-- steps -->
<div class="steps div-class">
		<div class="container" style="margin-top: 3em;">
			<h3 class="head">All the tools your team needs to transform your business</h3>
			<p class="urna">Baba Software helps you with every aspect of your better business processes with all the right tools put at one place.</p>
			<div class="wthree_steps_grids">
				<div class="col-md-4 wthree_steps_grid">
					<div class="wthree_steps_grid1 wthree_steps_grid1_after">
						<div class="wthree_steps_grid1_sub">
            			<span class="fas fa-coins"></span>
						</div>
					</div>
					<h1>Payroll baba </h1>
					<p>Calculate salary in minutes and transfer it to employees' bank accounts directly from Baba Payroll.</p>
				</div>
				<div class="col-md-4 wthree_steps_grid">
					<div class="wthree_steps_grid1 wthree_steps_grid1_after">
						<div class="wthree_steps_grid1_sub">
						<span class="fas fa-handshake"></span>
						</div>
					</div>
					<h1> CRM baba</h1>
					<p>Mobile and social CRM tools offered by Baba CRM put you exactly where your customers are.</p>
				</div>
				<div class="col-md-4 wthree_steps_grid">
					<div class="wthree_steps_grid1">
						<div class="wthree_steps_grid1_sub">
							<span class="fas fa-file-word"></span>
						</div>
					</div>
					<h1>ATS baba </h1>
					<p>A single core solution to streamlines the entire hiring process.</p>
				</div><br>
				<div class="col-md-4 wthree_steps_grid">
					<div class="wthree_steps_grid1 wthree_steps_grid1_after">
						<div class="wthree_steps_grid1_sub">
							<span class="fas fa-graduation-cap"></span>
						</div>
					</div>
					<h1>LMS baba</h1>
					<p>e-Learning systems provides access to online materials and courses for training and development purposes</p>
				</div>
				<div class="col-md-4 wthree_steps_grid">
					<div class="wthree_steps_grid1">
						<div class="wthree_steps_grid1_sub">
							<span class="fas fa-hand-holding-usd"></span>
						</div>
					</div>
					<h1>AR baba</h1>
					<p>Managing your Accounts Receivables effectively.</p>
        		</div>

       			 <div class="col-md-4 wthree_steps_grid">
					<div class="wthree_steps_grid1">
						<div class="wthree_steps_grid1_sub">
						<span class="fas fa-cart-arrow-down"></span>
						</div>
					</div>
					<h1>Sales baba </h1>
					<p>Monitor and manage your Sales anytime, from anywhere.</p>
        		</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //steps -->


<!-- testimonials -->
<section class="testi py-5" id="testi">
	<div class="container py-md-5 py-3">
			<h5 class="heading mb-2">Here’s what our customers have been saying about us</h5>
		<div class="row">
			<div class="col-lg-6 mb-4 margin-top">
				<div class="row testi-cgrid border-right-grid">
					<div class="col-sm-4 testi-icon mb-sm-0 mb-3">
						<img src="static-assets/images/favicon/test1.jpg" alt="baba-test-1" class="img-fluid"/>
					</div>
					<div class="col-sm-8">
						<p class="mx-auto"><span class="fa fa-quote-left"></span> It's a very user friendly product, we're impressed, their support team is the best thing about BABA Software. It's a brilliant software and can be recommended to any company.</p>
						<h6 class="b-w3ltxt mt-3"><span>customer</span></h6>
					</div>
				</div>
			</div>
			<div class="col-lg-6 mb-4">
				<div class="row testi-cgrid border-left-grid">
					<div class="col-sm-4 testi-icon mb-sm-0 mb-3">
						<img src="static-assets/images/favicon/test2.jpg" alt="baba-test-2" class="img-fluid"/>
					</div>
					<div class="col-sm-8">
						<p class="mx-auto"><span class="fa fa-quote-left"></span>It has excellent experience in using highly customizable BABA Software and helps you to manage customer relationship better, will recommend to all.</p>
						<h6 class="b-w3ltxt mt-3"><span>customer</span></h6>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- testimonials -->

      
       

<?php include 'footer.php';?>


  </body>


</html>
