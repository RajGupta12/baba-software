
<!DOCTYPE html>
<html lang="en">
  
<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Baba - The one place to run your entire business</title>
      <meta name="description" content="Explore the Baba suite of business software that will make your life easier. Be it Payroll, CRM, Recruitment, LMS, IPPM, BABA Software has a solution for it all.">
      <meta name="keywords" content="Payroll Software, Payroll management, Leave management software, CRM Software, Customer Relationship Management System, best recruitment software, Application tracking system, ATS, e-learning software, enterprise e-learning solution, best project management software">
        <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>

      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="on" http-equiv="cleartype"/>
      <meta name="google-site-verification" content="FoIxTuI3r__QWjBP-AVwiwtevEn8wErVr0aARIQGv3I" />

      <meta property="og:url" content="">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="">
      <meta property="og:description" content="">

      <meta property="og:image" content="" />
      <meta name="og:image" content="" />

      <meta name="twitter:title" content="" />
      <meta name="twitter:description" content="" />
      <meta property="twitter:image" content="" />
      <meta content="true" name=""/>
      <meta content="320" name=""/>
      <meta content="yes" name=""/>
      <meta content="black-translucent" name=""/>
      <meta content="telephone=no" name=""/>
      <meta content="address=no" name=""/>
      <?php include 'header.php';?>
      
    
<section class="first-fold " data-scroll-target="" id ="">
  <div class="l-page container pb-lg l-home banner-right-image">
    <div class="row ">
      <div class="col-md-6 mobile-center-desktop-left-align mb-lg banner-text-content mt-10 ">     
       <h2 class="box-shadow">Baba Software's mission is to make insanely productive</h2>
        <p class="sub-text bg-success">Baba Software makes day-to-day work easier, more transparent and efficient for thousands of companies</p>
        <div class="mt-md">
          <a href="schedule.php"><div title="Get Started" class="button button--solid in-page-scroll mb-sm" id="" target="" data-scroll-to="product-listing-section">Get Started</div></a>
        </div>
      </div>
      <div class="col-md-6 ">
        <div class="" style="">
          <img class="lazy-image" src="static-assets/images/sample/3.png"/>  
        </div>
      </div>
    </div>
  </div>
</section>

<section class="l-section section-light-grey" data-scroll-target="product-listing-section">
    <div class="l-section-heading align-center">      
      <h2>Tools for teams, from startup to enterprise</h2>
<p class="sub-text">Baba Software provides the tools to help every team unleash their full potential.</p>
    </div>
    <div class="l-page container l-grid">
      <div class="product-listing-table">
        <div class="product-wrapper">
            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">BABA Payroll</span-->
                <a class="logo logo-fteam" href="payroll.php"></a>    
                <p>Calculate salary in minutes and transfer it to employees' bank accounts directly from Baba Payroll.</p>   
                <a title="" class="button button--white button--small hover-fdesk" id="" target="" href="payroll.php"> START TRIAL</a>      
                <a href="payroll.php" class ="link forward--link">Learn more</a>
            </div>
            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">Baba CRM</span-->
                <a class="logo logo-fchat" href=""></a>     
                <p>Mobile and social CRM tools offered by Baba CRM put you exactly where your customers are.</p>      
                <a title="" class="button button--white button--small hover-fchat" id="" target="" href="crm-baba.php"> START TRIAL</a>     
              <a href= "crm-baba.php" class ="link forward--link">Learn more</a>
            </div>

            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">BABA Recruitment( ATS)</span-->
                <a class="logo logo-fsales" href="ats.php"></a>    
                <p>Solutions Business Manager is a leading Business Process Management Suite. </p>      
                <a title="" class="button button--white button--small hover-fsales" id="" target="" href="ats.php"> START TRIAL</a>     
                <a href="ats.php" class="link forward--link">Learn more</a>
            </div>
        </div>
      </div>

      <div class="product-listing-table">
        <div class="product-wrapper">
          <div class="product-listing product-listing-col-4 align-center">
            <!--span class="tag tag-product-listing">BABA LMS</span-->
            <a class="logo logo-fservice" href="lms-baba.php"></a>     
            <p>Baba's Guide to Applicant Tracking Systems and how to optimize your resume for ATS. Get hired faster.</p>     
            <a title="" class="button button--white button--small hover-fservice" id="" target="" href="lms-baba.php"> START TRIAL</a>     
            <a href="lms-baba.php" class="link forward--link">Learn more</a>
          </div>

          <div class="product-listing product-listing-col-4 align-center">
            <!--span class="tag tag-product-listing"> BABA iPPM</span-->
            <a class="logo logo-fcaller" href="ppm-baba.php"></a>   
            <p>e-Learning systems provides access to online materials and courses for training and development purposes</p>   
            <a title="" class="button button--white button--small hover-fcaller" id="" target="" href="ppm-baba.php"> START TRIAL</a>     
           <a href="ppm-baba.php" class="link forward--link">Learn more</a>
          </div>

          <div class="product-listing product-listing-col-4 align-center" style="visibility: hidden;">
            <!--span class="tag tag-product-listing">HUMAN RESOURCES SOFTWARE</span-->
              <a class="logo logo-fdesk" href="ppm-baba.php"></a>    
              <p>An employee engagement platform and custom dashboard for futuristic businesses to collaborate</p>     
              <a title="ppm-baba.php" class="button button--white button--small hover-fteam" id="" target="" href="ppm-baba.php"> START TRIAL</a>      
              <a href="ppm-baba.php" class="link forward--link">Learn more</a>
          </div>
        </div>
      </div>

    </div>
</section> 


 <section class=" section-light-grey  " data-scroll-target="" id ="">
  <div class="l-page container align-center">
  </div>
 </section>      
 <section class="pb-xl section-light-grey  " data-scroll-target="" id ="">
  <div class="l-page container align-center">      
  <a title="View All Products" class="button button--solid " id="" target="" href="products.php"> View All Products</a>
  </div>
 </section>

      

<section class="l-section" data-scroll-target="" id ="">
  <div class="l-page container">
      <div class="l-section-heading align-center">      
       <h2>Learn more about our customers</h2>
      </div>

    <div class="customer-logos">

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/1.gif" srcset=""/>  
        </div>
      </div>
    
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/2.gif" srcset=""/>
        </div>
      </div>
          
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/3.gif" srcset=""/>
        </div>
      </div>      
      
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/4.gif" srcset=""/> 
        </div>
      </div>
     
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/5.gif" srcset=""/>
        </div>
      </div>        
      
      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/6.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/7.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/8.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/9.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/10.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/11.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/12.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/13.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/14.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/15.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/16.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/17.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/18.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/19.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/20.gif" srcset=""/>  
        </div>
      </div>

      <div class="customer-logo">
        <div class="" style="">
          <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/21.gif" srcset=""/>  
        </div>
      </div>

    </div>
  </div>
</section>
      
<section class="l-section baba-bg" data-scroll-target="" data-scroll-target="" id ="">
  <div class="l-page container align-center">
    <div class="l-section-heading align-center">              
      <h2>Here’s what our customers have been saying about us</h2>
    </div>  
<div id="131189999694936" data-interval="false" class="carousel testimonial-all-center slide carousel-fade mt-lg" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
      <div class="carousel-item item active">
        <div class="row testimonial-card all-center">
          <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 testimonial-content">
            <i class="icon-quote-open testimonial-quote-icon"></i>
            <div class="testimonial-logo align-center">             
              <div class="" style="">
                <img class="lazy-image" data-src="" data-srcset="" src="" srcset=""/>
              </div>
            </div>
            <p class="testimonial-quote">“We're impressed, the best thing about BABA Software is their support team. It is a wonderful software and can be recommended to any company”</p>
            <div class="col-md-10 col-md-offset-1 author-bio">
              <div class="image-small">              
                <div class="" style="">
                  <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/client.jpg" srcset="" />
                </div>
              </div>
              <div class="author-bio-details">
                <h6 class="author-name">Suman Kumar Singh</h6>
                <div class="author-designation">Director of Support</div>
                <div class="author-company">IT-SCIENT</div>
              </div>
            </div>
            <i class="icon-quote-close testimonial-quote-icon"></i>
          </div>
        </div>
      </div>

      <!--div class="carousel-item item ">
        <div class="row testimonial-card all-center">
          <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 testimonial-content">
            <i class="icon-quote-open testimonial-quote-icon"></i>
            <div class="testimonial-logo align-center">
              <div class="" style="">
                <img class="lazy-image" data-src="" data-srcset="" src="" srcset=""/>
              </div>
            </div>
            <p class="testimonial-quote">“After much time and consideration of all applicants to my project post, I choose BABA Software (payroll) as they are really best as the reviews present them there service is really appreciable. After completion of the job, I can safely say that this reputation is fully deserved. 10/10.”</p>
            <div class="col-md-10 col-md-offset-1 author-bio">
                <div class="image-small">
                  <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/paatham.png" srcset=""/>
                  </div>
                </div>
              <div class="author-bio-details">
                <h6 class="author-name">Arun</h6>
                <div class="author-designation">CEO</div>
                <div class="author-company">Paatham</div>
              </div>
            </div>
            <i class="icon-quote-close testimonial-quote-icon"></i>
          </div>
        </div>
      </div-->

  </div>


    <!--div class="carousel-controls-wrapper  mt-md">
      <div class="carousel-indicators-wrapper">
        <ol class="carousel-indicators">
            <li data-target="#131189999694936" data-slide-to="0" class="active"></li>
            <li data-target="#131189999694936" data-slide-to="1" class=""></li>
            <li data-target="#131189999694936" data-slide-to="2" class=""></li>
            <li data-target="#131189999694936" data-slide-to="3" class=""></li>
        </ol>
        <a class="left carousel-control" href="#131189999694936" role="button" data-slide="prev">
          <span class="icon-arrow-button-left" aria-hidden="true"></span>
        </a>
        <a class="right carousel-control" href="#131189999694936" role="button" data-slide="next">
          <span class="icon-arrow-button-right" aria-hidden="true"></span>
        </a>
      </div>
    </div-->
</div>


     </div>
</section>

      

<section class="l-section">
  <div class="l-section-heading align-center ">     
  <h2>Inside Baba Software</h2>
  <p class="sub-text">Baba products are free to try, easy to set up, and work seamlessly together.</p>
  </div>

  <div class="l-page container l-grid">
      <div class="row">
        <div class="l-col-widget col-sm-4 col-sm-offset-2">
            <div class="press-release-item">
              <div class="" style="">
                <img class="lazy-image" src="static-assets/images/sample/1.jpg"/>
              </div>
              <div class="align-center press-release-title">
                <h6>Customer-for-life software you can swear by</h6>
                <!--p><span class="forward--link link mt-xs">Read more</span></p-->
              </div>
            </div>
        </div>

        <div class="l-col-widget col-sm-4 ">
            <div class="press-release-item">
              <div class="" style="">
                <img class="lazy-image" src="static-assets/images/sample/2.jpg"/> 
              </div>
              <div class="align-center press-release-title">
                <h6>We’re building great things and we need your talent</h6>
                <!--p><span class="forward--link link mt-xs">Explore careers</span></p-->
              </div>
            </div>
        </div>
      </div>
  </div>
</section>


<?php include 'footer.php';?>

  </body>


</html>
