
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Online Recruiting Software | Applicant Tracking System - Baba Recruit</title>
    <meta name="description" content="Hire better & faster with Baba, an all-in-one recruitment software for business owners, hiring managers & recruitment agencies. Start a free trial today!">
    <meta name="keywords" content="recruitment system, recruitment management system, application tracking system, best application tracking system, recruitment software, best recruitment software">
    <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <?php include 'header.php';?>

<!-- signin -->
<section class="signin py-5">
	<div class="container">
		<div class="row main-content-agile" style="margin-top: 7em;">
			<div class="col-md-6">	
				<div class="sub-main-w3 text-center">	
					<h3>Sign in to Baba Software</h3>
					<p class="mt-2 mb-4">We’re ready to set up your free trial of Baba Software.</p>
					<form action="#" method="post">
						<div class="icon1">
							<input placeholder="Full Name" name="name" type="text" required="">
						</div>
						<div class="icon1">
							<input placeholder="Email" name="mail" type="email" required="">
						</div>
						<div class="icon2">
							<input  placeholder="Phone number" name="phone" type="text" required="">
						</div>
						<div class="icon2">
							<input  placeholder="Password" name="Password" type="password" required="">
						</div>

						<div class="icon2" style="display: flex;margin-top: 1px;">
							<p>
								<input type="radio" id="test2" name="radio-group">
								<label for="test2">Individual</label>
							</p>
							<p>
								<input type="radio" id="test3" name="radio-group">
								<label for="test3" style="margin-left: 1em;">Corporate HR</label>
							</p><br>
					
						</div>
						

						<input type="submit" value="Create Account">
						<p>Already have an Account ? <a href="login.php" class="ml-2"><strong>Login to your Account</strong></a></p>


					</form>
				</div>
			</div>
			<div class="col-md-6">
			<img src="static-assets/images/favicon/img1.jpg" alt="" class="img-fluid" style="height:500px;"/>
			</div>
		</div>
	</div>
</section><br><br>
<!-- //signin -->




    <?php include 'footer.php';?>
  </body>
</html>
