
<!DOCTYPE html>
<html lang="en">
  <head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Support - Baba</title>
    <meta name="description" content="You can get technical support with Baba products. Go ahead, ask us anything about our products here.">
    <meta name="keywords" content="support">
    <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>

    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <?php include 'header.php';?>

      
<section id="" data-scroll-target="#" class="first-fold align-center pattern-gradient-light    " >
  <div class="container  banner-content l-banner " data-scroll-target="">    
  <h1>Got questions? <br />Baba Software Support can help you</h1>
  </div>
</section>

<section class="l-section section-light-grey" data-scroll-target="product-listing-section">
    <div class="l-section-heading align-center">      
      <h2>Tools for teams, from startup to enterprise</h2>
<p class="sub-text">Baba Software provides the tools to help every team unleash their full potential.</p>
    </div>
    <div class="l-page container l-grid">
      <div class="product-listing-table">
        <div class="product-wrapper">
            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">BABA Payroll</span-->
                <a class="logo logo-fteam" href="payroll.php"></a>    
                <p>Calculate salary in minutes and transfer it to employees' bank accounts directly from Baba Payroll.</p>   
                <a title="" class="button button--white button--small hover-fdesk" id="" target="" href=""> Price</a>      
                <a href="" class ="link forward--link">FAQ</a>
            </div>
            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">Baba CRM</span-->
                <a class="logo logo-fchat" href=""></a>     
                <p>Mobile and social CRM tools offered by Baba CRM put you exactly where your customers are.</p>      
                <a title="" class="button button--white button--small hover-fchat" id="" target="" href=""> Price</a>     
              <a href= "" class ="link forward--link">FAQ</a>
            </div>

            <div class="product-listing product-listing-col-4 align-center">
              <!--span class="tag tag-product-listing">BABA Recruitment( ATS)</span-->
                <a class="logo logo-fsales" href="ats.php"></a>    
                <p>Solutions Business Manager is a leading Business Process Management Suite. </p>      
                <a title="" class="button button--white button--small hover-fsales" id="" target="" href=""> Price</a>     
                <a href="ats.php" class="link forward--link">FAQ</a>
            </div>
        </div>
      </div>

      <div class="product-listing-table">
        <div class="product-wrapper">
          <div class="product-listing product-listing-col-4 align-center">
            <!--span class="tag tag-product-listing">BABA LMS</span-->
            <a class="logo logo-fservice" href=""></a>     
            <p>Baba's Guide to Applicant Tracking Systems and how to optimize your resume for ATS. Get hired faster.</p>     
            <a title="" class="button button--white button--small hover-fservice" id="" target="" href=""> Price</a>     
            <a href="" class="link forward--link">FAQ</a>
          </div>

          <div class="product-listing product-listing-col-4 align-center">
            <!--span class="tag tag-product-listing"> BABA iPPM</span-->
            <a class="logo logo-fcaller" href=""></a>   
            <p>e-Learning systems provides access to online materials and courses for training and development purposes</p>   
            <a title="" class="button button--white button--small hover-fcaller" id="" target="" href=""> Price</a>     
           <a href="" class="link forward--link">FAQ</a>
          </div>

          <div class="product-listing product-listing-col-4 align-center" style="visibility: hidden;">
            <!--span class="tag tag-product-listing">HUMAN RESOURCES SOFTWARE</span-->
              <a class="logo logo-fdesk" href=""></a>    
              <p>An employee engagement platform and custom dashboard for futuristic businesses to collaborate</p>     
              <a title="" class="button button--white button--small hover-fteam" id="" target="" href=""> Price</a>      
              <a href="" class="link forward--link">FAQ</a>
          </div>
        </div>
      </div>

    </div>
</section>


<?php include 'footer.php';?>
  </body>
</html>
