
<script src='static-assets/js/70421.js'></script>
    <link href="static-assets/css/site.css" rel="stylesheet" />
    <style>
      .box-shadow{
        background: #7abd11;
        padding: 10px;
        color: #fff;
        line-height: 42px;
        box-shadow: 0px 5px 8px 1px #3f3f3f;
        margin: 0px;
        z-index: 2;
        position: inherit;
      }
      .bg-success{
        background: #96d00b;
        color:#fff;
        padding: 10px;
      }
      .mt-10{
        margin-top: 80px;
      }
      .baba-bg{
        background-image:url(static-assets/images/sample/pattern_light_top-left.svg);
      }
      .sticky {
        position: fixed;
        top: 0;
        width: 100%;
      }
      .menu-title{
        font-size: 15px;
      }

        @media screen and (max-width: 991px){
.nav-main-menu.show {
     left: 0px; 
    transition: all .8s;
    -webkit-transition: all .8s;
    -moz-transition: all .8s;
    -o-transition: all .8s;
    margin: 0;
    width: 100%;
   opacity: 1;
    padding: 15px 15px 5px 15px;
     transition: all .5s;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -o-transition: all .5s;
    top: 0;
    background-color: #fff;
    margin: 0;
    z-index: 99;
}

}

    </style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144362603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144362603-1');
</script>
<link rel="canonical" href="https://baba.software/" />
    
  </head>
  <body  class=" nav-transparent  no-super " data-product-name="fworks" data-lang=en>      
  
<header>
<nav class="nav-opaque-bg" id="myHeader">    
  <div class="nav-primary-wrapper">
    <div class="nav-primary l-page">
      <div class="nav-logo-wrapper">
        <a href="index" class="logo logo-fworks"></a>
        <span class="nav-burger">
          <span class="nav-burger-icon"></span>
        </span>
      </div>
      <ul class="l-nav-list nav-main-menu">
        <li class="nav-main-item card-nav has-sub-menu">
          <span class="nav-label">Products</span>
          <i class="icon-arrow-down nav-sub-toggle"></i>        
        <ul class="l-nav-list nav-sub-menu">
          <div class="nav-featured-product-row">
             <div class="row">
              <div class="col-md-4">
                <li class="nav-sub-item fdesk-bg nav-logo-tagline">
                  <a href="payroll-baba.php" class="nav-sub-label">
                    <span class="menu-title"> Payroll BABA </span>
                    <span class="desc">All-In-One Payroll Software To Streamline Your Operations.</span>
                    <span class="label"> Payroll BABA </span>
                  </a>
                </li>
              </div>
              <div class="col-md-4">
                <li class="nav-sub-item fsales-bg nav-logo-tagline">
                  <a  href="http://baba.software/crm-baba" class="nav-sub-label">
                    <span class="menu-title">CRM Baba </span>
                    <span class="desc">Make Pipeline Management Simple With CRM Baba.</span>
                    <span class="label">CRM  Baba </span>
                  </a>
                </li>
              </div>
              <div class="col-md-4">
                <li class="nav-sub-item fcaller-bg nav-logo-tagline">
                  <a href="http://baba.software/ats-baba" class="nav-sub-label">
                    <span class="menu-title">ATS BABA (Recruitment) </span>
                    <span class="desc">Modern Recruiting Tools Help You Find And Hire The Right Talent Fast.</span>
                    <span class="label">ATS BABA (Recruitment) </span>
                  </a>
                </li>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <li class="nav-sub-item fservice-bg nav-logo-tagline">
                  <a href="http://baba.software/lms-baba.php" class="nav-sub-label">
                    <span class="menu-title">LMS BABA </span>
                    <span class="desc">Do Better Work Insight To Lead Skills To Deliver.</span>
                    <span class="label">LMS BABA </span>
                  </a>
                </li>
              </div>
            <div class="col-md-4">
              <li class="nav-sub-item fchat-bg nav-logo-tagline">
                <a href="http://baba.software/ar-baba" class="nav-sub-label">
                  <span class="menu-title">AR BABA </span>
                  <span class="desc">Automates the entire Accounts Receivable & Collections Process.</span>
                  <span class="label">AR BABA </span>
                </a>
              </li>
            </div>
          
            <div class="col-md-4">
              <li class="nav-sub-item fchat-bg nav-logo-tagline">
                <a href="  http://baba.software/sales-baba" class="nav-sub-label">
                  <span class="menu-title">SALES BABA </span>
                  <span class="desc">The All-in-One Sales Software To Accelerate Your Sales Process.</span>
                  <span class="label">SALES BABA </span>
                </a>
              </li>
            </div>
          </div>
          
        <div class="row">
              <div class="col-md-4">
                <li class="nav-sub-item fservice-bg nav-logo-tagline">
                  <a href="http://baba.software/hr-baba" class="nav-sub-label">
                    <span class="menu-title">HR BABA </span>
                    <span class="desc">Centralized HR Information system accessible anywhere in a secure manner with HR Baba software. </span>
                    <span class="label">HR BABA </span>
                  </a>
                </li>
              </div>
              
              <!--<div class="col-md-4">-->
              <!--  <li class="nav-sub-item fservice-bg nav-logo-tagline">-->
              <!--    <a href="http://baba.software/lms_baba" class="nav-sub-label">-->
              <!--      <span class="menu-title">BABA IPPM</span>-->
              <!--      <span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span>-->
              <!--      <span class="label">BABA IPPM</span>-->
              <!--    </a>-->
              <!--  </li>-->
              <!--</div>-->
            
            
           
          </div>
        </div>
                      
        </ul>
      </li>
    <!--<li class="nav-main-item caps-heading-12 has-sub-menu">-->
    <!--    <span class="nav-label">Company</span>-->
    <!--    <i class="icon-arrow-down nav-sub-toggle"></i>-->
    <!--    <ul class="l-nav-list nav-sub-menu">-->
    <!--      <li class="nav-sub-item">-->
    <!--        <a class="nav-sub-label" href="about">-->
    <!--           <span class="label">About</span>-->
    <!--        </a>-->
    <!--      </li>-->
    <!--      <li class="nav-sub-item">-->
    <!--        <a class="nav-sub-label" href="partner">-->
    <!--          <span class="label">Partners</span>-->
    <!--        </a>-->
    <!--      </li>-->
    <!--      <li class="nav-sub-item">-->
    <!--        <a class="nav-sub-label" href="career">-->
    <!--          <span class="label">Careers</span>-->
    <!--        </a>-->
    <!--      </li>-->
    <!--      <li class="nav-sub-item">-->
    <!--        <a class="nav-sub-label" href="contact">-->
    <!--          <span class="label">Contact</span>-->
    <!--        </a>-->
    <!--      </li>-->
    <!--    </ul>-->
    <!--</li>-->
       
    
      
      
      <li class="nav-main-item caps-heading-12">
        <a class="nav-label" href="solution">Solutions</a>
      </li>
      
             <li class="nav-main-item caps-heading-12">
               <a class="nav-label" href="pricing">Pricing</a>
              </li>

        <li class="nav-main-item caps-heading-12">
          <a class="nav-label" href="support">Support</a>
        </li>
        
  
            
         <li class="nav-main-item caps-heading-12">
          <a class="nav-label" href="schedule">Schedule Demo</a>
        </li>
                      
          <li class="nav-main-item caps-heading-12">
          <a class="nav-label" href="Blog.php">Blog</a>
        </li>              
              <li class="nav-main-item caps-heading-12 nav-main-item-button hide-in-mobile"></li>
          </ul>
        </div>
      </div>
    </nav>
    </header>

    <script type="text/javascript">
      $('.nav-burger').click(function() {
 $ ('.nav-main-menu').toggleClass('show');
});
    </script>>