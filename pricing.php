
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Online Recruiting Software | Applicant Tracking System - Baba Recruit</title>
    <meta name="description" content="Hire better & faster with Baba, an all-in-one recruitment software for business owners, hiring managers & recruitment agencies. Start a free trial today!">
    <meta name="keywords" content="recruitment system, recruitment management system, application tracking system, best application tracking system, recruitment software, best recruitment software">
    <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <?php include 'header.php';?><br><br><br>


            <!-- Pricing -->
            <section class="pricing py-5">
            	<div class="container py-md-3">
            		
            		<table class="mt-5 table-responsive">
            		<tbody>
            		<tr>
            			
            			<td class="price">
            				<h4 class="mt-3">Free</h4>
            				<h4>$0</h4>
            				<p></p>
            			</td>
            			<td class="price">
            				<h4 class="mt-3">Standard</h4>
							<h4>0.99 Cents User</h4>
							<h4>INR : 49.00/User</h4>
            				
            			</td>
            			<td class="price">
							<h4 class="mt-3">Premium</h4>
							<h4>0.99 Cents User</h4>
            				<h4>INR : 49.00/User</h4>
            				
            			</td>
            		</tr>
            
            		<tr>
            			
            			<td class="price">
            				<a href="#">Free Trial</a>
            			</td>
            			<td class="price">
            				<a href="#">Buy Now</a>
            			</td>
            			<td class="price">
            				<a href="#">Buy now</a>
            			</td>
            		</tr>
					
					<tr class="bg-light head">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; 3 Apps</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; 50 Apps</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Unlimited Apps</td>
					</tr>	
					<tr class="bg-light head">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; 25,000 Records</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Unlimited Records<sup style="font-size:20px;">*</sup></td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Unlimited Records</td>
					</tr>	
					<tr class="bg-light head">
						
						<td class="data"></td>
						<td class="data"></td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Starts at 10 users</td>
					</tr>

					<tr class="bg-light">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Choose from over 50 ready-to-use apps</td>
						<td class="data" style="color:#f33c13;"> Everything in Basic +</td>
						<td class="data" style="color:#f33c13;"> Everything in Premium +</td>
					</tr>
					<tr class="bg-light">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Customize design for web, mobile and tablet interfaces</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Customer Portal (add-on)</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; 1,000 Customer Portal users</td>
					</tr>

					<tr class="bg-light">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Forms, reports, pages, and workflows </td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Developer Sandbox</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Download and publish rebranded apps on iOS or AndroidDist</td>
					</tr>

					<tr class="bg-light">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Third-party integrations</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Record Audit</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Premium support**</td>
					</tr>

					<tr class="bg-light">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Collaboration and sharing</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Application backup</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Active Directory integration & user provisioning Seamle</td>
					</tr>

					<tr class="bg-light">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; 24*5 live support</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Multi-language support</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Enhanced operating limits</td>
					</tr>

					<tr class="bg-light">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; AI capabilities</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Offline mobile access</td>
						
					</tr>

					<tr class="bg-light">
						
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Schema builder</td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Payment gateway integration</td>
						
					</tr>
					
					<tr class="bg-light">

						<td class="data"></td>
						<td class="data"><i class="fas fa-check"></i>&nbsp;&nbsp; Domain Restriction</td>
						
					</tr>
					<tr class="bg-light">
						
						<td class="data"></td>
						<td class="data"  style="padding-bottom:30px;"><i class="fas fa-check"></i>&nbsp;&nbsp; Email Management</td>
						
					</tr>
            		
            		</tbody>
            		</table>
            	</div>
            </section>
            <!-- Pricing -->

    <br><br>

    <?php include 'footer.php';?>
  </body>
</html>