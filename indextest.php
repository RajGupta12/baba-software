<!-- features area start -->
<!-- <section id="features" class="features-area pt-100 pb-70">    <div class="container">        <div class="section-header text-center mb-50">            <h2>Features designed for you</h2>
            <p>We believe we have created the most efficient SaaS landing page for your users. Landing page<br>with                features that will convince you to use it for your SaaS business. </p>
        </div>
        <div class="row">            <div class="col-xl-4 col-lg-4 col-md-6">                <div class="fetures-wrapper text-center">                    <div class="fetures-icon mb-20"><img src="http://localhost/uploads/gallery/icon1.png" alt=""></div>
                    <div class="fetures-text">                        <h4>Responsive <span>Layout Template</span></h4>
                        <p>Responsive code that makes your landing page look good on all devices (desktops, tablets, and                            phones). Created with mobile specialists.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">                <div class="fetures-wrapper active text-center">                    <div class="fetures-icon mb-20"><img src="http://localhost/uploads/gallery/icon1.png" alt=""></div>
                    <div class="fetures-text">                        <h4><span>SaaS Landing Page</span> Analysis</h4>
                        <p>A perfect structure created after we analized trends in SaaS landing page designs. Analysis                            made to the most popular SaaS businesses.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">                <div class="fetures-wrapper mb-30 text-center">                    <div class="fetures-icon mb-20"><img src="http://localhost/uploads/gallery/icon1.png" alt=""></div>
                    <div class="fetures-text">                        <h4><span>Smart</span> BEM <span>Grid</span></h4>
                        <p>Blocks, Elements and Modifiers. A smart HTML/CSS structure that can easely be reused. Layout                            driven by the purpose of modularity.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> features area end -->
<style>
    /* sanjiv */
    .powerful-features-single-step i {
        font-size: 24px;
        /* color: #4275ff; */
        color: #1AA2CD;
        height: 80px;
        width: 80px;
        margin-left: 126px;
        margin-bottom: 30px;
        border-radius: 50%;
        text-align: center;
        line-height: 80px;
        display: block;
        border: 2px solid #89da3f;
        transition: .3s all ease-in-out;
    }

    /* sanjiv */
    .powerful-features-single-step {
        border: none;
        outline: none;
        box-shadow: 2px 3px 5px #F5F8FE;
        padding: 20px;
        transition: .3s all ease-in-out;
        background-color: #F5F8FE;
    }

    /* sanjiv */
    .powerful-features-single-step:hover i {
        /* background: #d9e8ff; */
        background-image: linear-gradient(to bottom, #89DA3F, white);
        cursor: pointer;
        background-color: white;
    }

    .powerful-features-single-step:hover {
        transform: translateY(-5%);
        box-shadow: 2px 4px 12px grey;
    }

    .powerfull-features-video.position-relative img {
        width: 100%;
    }

    .features-text span {
        font-size: 24px;
        color: #42495b;
    }

    .features-text p {
        margin-bottom: 0;
        margin-top: 7px;
    }

    .features-img img {
        margin-left: -25px;
    }

    .powerfull-features-img {
        position: relative;
    }

    /* sanjiv */
    .powerfull-features-img img {
        width: 300px;
        height: 500px;
        margin-left: 30px;
    }
</style>
<section class="about-bottom   " id="about">
    <div class="container py-md-5 py-3">
        <div class="row">
            <div class="col-lg-6 left-img"> <img src="http://localhost/baba-soft/uploads/gallery/st11.png" class="img-fluid" alt="" style="height: 430px;"> </div>
            <div class="col-lg-6 mt-lg-0 mt-4">
                <div class="row inner-heading">
                    <div class="col-md-2" style="    margin-top: 11px;"> <span class="fa fa-bullseye" aria-hidden="true"></span> </div>
                    <div class="col-md-10">
                        <h4 class="mt-md-0 mt-2">Every Business Size </h4>
                        <p class="mt-3">Solutions for large, midsized and small, fast growing businesses. </p>
                    </div>
                    <div class="col-md-2  "> <span class="fa fa-industry" aria-hidden="true"></span> </div>
                    <div class="col-md-10  ">
                        <h4 class="mt-md-0 mt-2">Every Industry </h4>
                        <p class="mt-3">Industry-specific functionality spanning a broad range of businesses.</p>
                    </div>
                    <div class="col-md-2  "> <span class="fa fa-user" aria-hidden="true"></span> </div>
                    <div class="col-md-10 ">
                        <h4 class="mt-md-0 mt-2">Every Role </h4>
                        <p class="mt-3">Solutions addressing the needs of all the team as per their role. </p>
                    </div>
                    <div class="col-md-2  "> <span class="fa fa-desktop" aria-hidden="true"></span> </div>
                    <div class="col-md-10  ">
                        <h4 class="mt-md-0 mt-2">Current Software </h4>
                        <p class="mt-3">Replacing QuickBooks, Microsoft Dynamics, Sage, SAP and other business systems. </p>
                    </div>
                    <div class="col-md-2  "> <span class="fa fa-check-circle" aria-hidden="true"></span> </div>
                    <div class="col-md-10 mb-5">
                        <h4 class="mt-md-0 mt-2">Suite Success </h4>
                        <p class="mt-3">Our mission is to make software easier to implement and run.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- Powerful-Features-area-start -->
<section id="features" class="powerful-features gray-bg pt-120 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-header mb-80 text-center" style="margin-bottom:30px;">
                    <h2>Powerful Features</h2>
                    <p>All The Tools Your Team Needs To Transform Your Business</p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="powerful-features-single-step   " style="margin-bottom:40px;"> <i class="fas fa-coins   mt-10  "></i>
                    <div class="features-text text-center fix pr-30"> <span>Payroll BABA</span>
                        <p>Calculate salary in minutes and transfer it to employees bank accounts directly from Baba Payroll.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="powerful-features-single-step mb-70" style="margin-bottom:40px;"> <i class="fas fa-handshake mt-10  "></i>
                    <div class="features-text text-center fix pr-30"> <span>CRM Baba</span>
                        <p> Mobile and social CRM tools offered by Baba CRM put you exactly where your customers are.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="powerful-features-single-step mb-70" style="margin-bottom:40px;"> <i class="fas fa-file-word mt-10  "></i>
                    <div class="features-text text-center fix pr-30"> <span>ATS Baba</span>
                        <p>A single core solution to streamlines the entire hiring process.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="powerful-features-single-step    "> <i class="fas fa-graduation-cap mt-10  "></i>
                    <div class="features-text pl-30 text-center                         fix"> <span>LMS Baba</span>
                        <p>E-learning systems provides access to materials and courses for training and development purposes.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="powerful-features-single-step mb-70"> <i class="fas fa-hand-holding-usd mt-10  "></i>
                    <div class="features-text text-center pl-30 fix"> <span>AR Baba</span>
                        <p>Managing your Accounts Receivables effectively.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="powerful-features-single-step mb-70"> <i class="fas fa-cart-arrow-down mt-10  "></i>
                    <div class="features-text text-center pl-30 fix"> <span>Sales Baba</span>
                        <p>Monitor and manage your sales anytime, from anywhere.</p>
                    </div>
                </div>
            </div>
            <!-- <div class="col-xl-4 col-lg-4">                    <div class="powerful-features-single-step mb-70 mt-80">                        <i class="fal fa-share-alt mt-10 f-right"></i>                        <div class="features-text text-right fix pr-30">                            <span>Easy Instalations</span>                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>
                    <div class="powerful-features-single-step mb-70">                        <i class="fal fa-anchor mt-10 f-right"></i>                        <div class="features-text text-right fix pr-30">                            <span>Real Time Customizat </span>                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>
                    <div class="powerful-features-single-step mb-70">                        <i class="fal fa-bullhorn mt-10 f-right"></i>                        <div class="features-text text-right fix pr-30">                            <span>Customer Support</span>                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>
                </div>
 -->
            <!-- <div class="col-xl-4 col-lg-4">                    <div class="powerfull-features-img">                        <img src="http://localhost/uploads/gallery/powerfull-features01.png" alt="">                    </div>
                </div>
 -->
            <!-- <div class="col-xl-4 col-lg-4">                    <div class="powerful-features-single-step mb-70 mt-80">                        <i class="fal fa-bow-arrow mt-10 f-left"></i>                        <div class="features-text pl-30 fix">                            <span>Easy Editable</span>                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>
                    <div class="powerful-features-single-step mb-70">                        <i class="fal fa-spade mt-10 f-left"></i>                        <div class="features-text pl-30 fix">                            <span>Clean & Unique Design</span>                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>
                    <div class="powerful-features-single-step mb-70">                        <i class="fal fa-laptop-code mt-10 f-left"></i>                        <div class="features-text pl-30 fix">                            <span>Clean Code</span>                            <p>Lorem ipsum dolor sit amet consectr ncididunt ut labore et dolore</p>
                        </div>
                    </div>
                </div>
 -->
        </div>
    </div>
</section><!-- Powerful-Features-area-end -->
<!-- Powerful-Features-video-area-start -->
<!-- <section class="powerful-features-video pt-205 pb-130">        <div class="container">            <div class="powerfull-features-video position-relative">                <img src="http://localhost/Ultimate_SaaS/uploads/gallery/powerfull-features-video.jpg" alt="">                <a href="https://www.youtube.com/watch?v=odRqfyYijc4" class="video-icon popup-video"><i class="fal fa-play"></i></a>            </div>
        </div>
    </section> -->
<!-- Powerful-Features-video-area-end -->
<section class="testi py-5 " id="testi">
    <div class="container py-md-5 py-3">
        <h5 class="heading mb-2">Here’s what our customers have been saying about us</h5>
        <div class="row">
            <div class="col-lg-6 mb-4">
                <div class="row testi-cgrid border-right-grid">
                    <div class="col-sm-4 testi-icon mb-sm-0 mb-3"> <img src="http://localhost/baba-soft/uploads/gallery/test1.jpg" alt="" class="img-fluid"> </div>
                    <div class="col-sm-8">
                        <p class="mx-auto"><span class="fa fa-quote-left" aria-hidden="true"></span> It's a very user friendly product, we're impressed, their support team is the best thing about BABA Software. It's a brilliant software and can be recommended to any company.</p>
                        <h6 class="b-w3ltxt mt-3"><span>customer</span></h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="row testi-cgrid border-left-grid">
                    <div class="col-sm-4 testi-icon mb-sm-0 mb-3"> <img src="http://localhost/baba-soft/uploads/gallery/test2.jpg" alt="" class="img-fluid"> </div>
                    <div class="col-sm-8">
                        <p class="mx-auto"><span class="fa fa-quote-left" aria-hidden="true"></span>It has excellent experience in using highly customizable BABA Software and helps you to manage customer relationship better, will recommend to all.</p>
                        <h6 class="b-w3ltxt mt-3"><span>customer</span></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- our-team area end -->
<!-- <section class="our-team bg-light pt-100 pb-70">    <div class="container">        <div class="row">            <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2">                <div class="section-header text-center mb-70">                    <h2>Creative Heads</h2>
                    <p>Generally, every customer wants a product or service that solves their problem, worth their <br>                        money, and is delivered with amazing customer service </p>
                </div>
            </div>
        </div>
        <div class="row">            <div class="col-xl-4 col-lg-4 col-md-4">                <div class="team-box mb-30">                    <div class="team-img text-center"><img src="http://localhost/uploads/gallery/team01.jpg" alt="image">                        <div class="team-overlay"> -->
<!--Team Social-->
<!-- <ul class="team-social">
                                <li><a class="facebook-bg" href="#"><i class="fab fa-facebook-f"></i></a><br></li>
                                <li><a class="twitter-bg" href="#"><i class="fab fa-twitter"></i></a><br></li>
                                <li><a class="instagram-bg" href="#"><i class="fab fa-instagram"></i></a><br></li>
                                <li><a class="pinterest-bg" href="#"><i class="fab fa-pinterest-p"></i></a><br></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-text text-center">                        <h5>Alex Walkin</h5>
                        <span>Owner / Co-founder</span>                    </div>
                    <div class="progess-wrapper">                        <div class="single-skill mb-30">                            <div class="bar-title">                                <h4>Marketing Online <span class="f-right">80%</span></h4>
                            </div>
                            <div class="progress">                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                             </div>
                        </div>
                        <div class="single-skill mb-20">                            <div class="bar-title">                                <h4>SEO Services <span class="f-right">70%</span></h4>
                            </div>
                            <div class="progress">                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4">                <div class="team-box mb-30">                    <div class="team-img text-center"><img src="http://localhost/uploads/gallery/team03.png" alt="image">                        <div class="team-overlay"> -->
<!--Team Social-->
<!-- <ul class="team-social">
                                <li><a class="facebook-bg" href="#"><i class="fab fa-facebook-f"></i></a><br></li>
                                <li><a class="twitter-bg" href="#"><i class="fab fa-twitter"></i></a><br></li>
                                <li><a class="instagram-bg" href="#"><i class="fab fa-instagram"></i></a><br></li>
                                <li><a class="pinterest-bg" href="#"><i class="fab fa-pinterest-p"></i></a><br></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-text text-center">                        <h5>Teena Hhon</h5>
                        <span>Lead Designer</span>                    </div>
                    <div class="progess-wrapper">                        <div class="single-skill mb-30">                            <div class="bar-title">                                <h4>Web Designing<span class="f-right">75%</span></h4>
                            </div>
                            <div class="progress">                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="single-skill mb-20">                            <div class="bar-title">                                <h4>Print Media<span class="f-right">90%</span></h4>
                            </div>
                            <div class="progress">                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4">                <div class="team-box mb-30">                    <div class="team-img text-center"><img src="http://localhost/uploads/gallery/team02.jpg" alt="image">                        <div class="team-overlay"> -->
<!--Team Social-->
<!-- <ul class="team-social">
                                <li><a class="facebook-bg" href="#"><i class="fab fa-facebook-f"></i></a><br></li>
                                <li><a class="twitter-bg" href="#"><i class="fab fa-twitter"></i></a><br></li>
                                <li><a class="instagram-bg" href="#"><i class="fab fa-instagram"></i></a><br></li>
                                <li><a class="pinterest-bg" href="#"><i class="fab fa-pinterest-p"></i></a><br></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-text text-center">                        <h5>David Emron</h5>
                        <span>Web Development</span>                    </div>
                    <div class="progess-wrapper">                        <div class="single-skill mb-30">                            <div class="bar-title">                                <h4>PHP Programer<span class="f-right">90%</span></h4>
                            </div>
                            <div class="progress">                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="single-skill mb-20">                            <div class="bar-title">                                <h4>Java Scripts<span class="f-right">80%</span></h4>
                            </div>
                            <div class="progress">                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> our-team area end -->