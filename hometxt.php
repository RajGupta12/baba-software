<!-- features area start -->
<section id="features" class="features-area pt-100 pb-70">
    <div class="container">
        <div class="section-header text-center mb-50">
            <h2>Features designed for you</h2>

            <p>We believe we have created the most efficient SaaS landing page for your users. Landing page<br>with
                features that will convince you to use it for your SaaS business. </p>
        </div>

        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="fetures-wrapper text-center">
                    <div class="fetures-icon mb-20"><img src="http://localhost/uploads/gallery/icon1.png" alt=""></div>

                    <div class="fetures-text">
                        <h4>Responsive <span>Layout Template</span></h4>

                        <p>Responsive code that makes your landing page look good on all devices (desktops, tablets, and
                            phones). Created with mobile specialists.</p>
                    </div>

                </div>

            </div>

            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="fetures-wrapper active text-center">
                    <div class="fetures-icon mb-20"><img src="http://localhost/uploads/gallery/icon1.png" alt=""></div>

                    <div class="fetures-text">
                        <h4><span>SaaS Landing Page</span> Analysis</h4>

                        <p>A perfect structure created after we analized trends in SaaS landing page designs. Analysis
                            made to the most popular SaaS businesses.</p>
                    </div>

                </div>

            </div>

            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="fetures-wrapper mb-30 text-center">
                    <div class="fetures-icon mb-20"><img src="http://localhost/uploads/gallery/icon1.png" alt=""></div>

                    <div class="fetures-text">
                        <h4><span>Smart</span> BEM <span>Grid</span></h4>

                        <p>Blocks, Elements and Modifiers. A smart HTML/CSS structure that can easely be reused. Layout
                            driven by the purpose of modularity.</p>
                    </div>

                </div>

            </div>

        </div>

    </div>

</section> <!-- features area end -->
<!-- our-team area end -->
<section class="our-team bg-light pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2">
                <div class="section-header text-center mb-70">
                    <h2>Creative Heads</h2>

                    <p>Generally, every customer wants a product or service that solves their problem, worth their <br>
                        money, and is delivered with amazing customer service </p>
                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4">
                <div class="team-box mb-30">
                    <div class="team-img text-center"><img src="http://localhost/uploads/gallery/team01.jpg" alt="image">
                        <div class="team-overlay">
                            <!--Team Social-->
                            <ul class="team-social">

                                <li><a class="facebook-bg" href="#"><i class="fab fa-facebook-f"></i></a><br></li>

                                <li><a class="twitter-bg" href="#"><i class="fab fa-twitter"></i></a><br></li>

                                <li><a class="instagram-bg" href="#"><i class="fab fa-instagram"></i></a><br></li>

                                <li><a class="pinterest-bg" href="#"><i class="fab fa-pinterest-p"></i></a><br></li>

                            </ul>

                        </div>

                    </div>

                    <div class="team-text text-center">
                        <h5>Alex Walkin</h5>
                        <span>Owner / Co-founder</span>
                    </div>

                    <div class="progess-wrapper">
                        <div class="single-skill mb-30">
                            <div class="bar-title">
                                <h4>Marketing Online <span class="f-right">80%</span></h4>
                            </div>

                            <div class="progress">
                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>

                            </div>

                        </div>

                        <div class="single-skill mb-20">
                            <div class="bar-title">
                                <h4>SEO Services <span class="f-right">70%</span></h4>
                            </div>

                            <div class="progress">
                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-xl-4 col-lg-4 col-md-4">
                <div class="team-box mb-30">
                    <div class="team-img text-center"><img src="http://localhost/uploads/gallery/team03.png" alt="image">
                        <div class="team-overlay">
                            <!--Team Social-->
                            <ul class="team-social">

                                <li><a class="facebook-bg" href="#"><i class="fab fa-facebook-f"></i></a><br></li>

                                <li><a class="twitter-bg" href="#"><i class="fab fa-twitter"></i></a><br></li>

                                <li><a class="instagram-bg" href="#"><i class="fab fa-instagram"></i></a><br></li>

                                <li><a class="pinterest-bg" href="#"><i class="fab fa-pinterest-p"></i></a><br></li>

                            </ul>

                        </div>

                    </div>

                    <div class="team-text text-center">
                        <h5>Teena Hhon</h5>
                        <span>Lead Designer</span>
                    </div>

                    <div class="progess-wrapper">
                        <div class="single-skill mb-30">
                            <div class="bar-title">
                                <h4>Web Designing<span class="f-right">75%</span></h4>
                            </div>

                            <div class="progress">
                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>

                            </div>

                        </div>

                        <div class="single-skill mb-20">
                            <div class="bar-title">
                                <h4>Print Media<span class="f-right">90%</span></h4>
                            </div>

                            <div class="progress">
                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-xl-4 col-lg-4 col-md-4">
                <div class="team-box mb-30">
                    <div class="team-img text-center"><img src="http://localhost/uploads/gallery/team02.jpg" alt="image">
                        <div class="team-overlay">
                            <!--Team Social-->
                            <ul class="team-social">

                                <li><a class="facebook-bg" href="#"><i class="fab fa-facebook-f"></i></a><br></li>

                                <li><a class="twitter-bg" href="#"><i class="fab fa-twitter"></i></a><br></li>

                                <li><a class="instagram-bg" href="#"><i class="fab fa-instagram"></i></a><br></li>

                                <li><a class="pinterest-bg" href="#"><i class="fab fa-pinterest-p"></i></a><br></li>

                            </ul>

                        </div>

                    </div>

                    <div class="team-text text-center">
                        <h5>David Emron</h5>
                        <span>Web Development</span>
                    </div>

                    <div class="progess-wrapper">
                        <div class="single-skill mb-30">
                            <div class="bar-title">
                                <h4>PHP Programer<span class="f-right">90%</span></h4>
                            </div>

                            <div class="progress">
                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>

                            </div>

                        </div>

                        <div class="single-skill mb-20">
                            <div class="bar-title">
                                <h4>Java Scripts<span class="f-right">80%</span></h4>
                            </div>

                            <div class="progress">
                                <div class="progress-bar wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section> <!-- our-team area end -->