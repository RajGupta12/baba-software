
<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Baba Software</title>
      <meta name="description" content="Page Not Found | Freshworks">
      <meta name="keywords" content="">
        <link rel="icon" size="16x16" type="image/png" href="/static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="96x96" type="image/png" href="/static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="32x32" type="image/png" href="/static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="192x192" type="image/png" href="/static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="16x16" type="image/png" href="/static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="96x96" type="image/png" href="/static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="32x32" type="image/png" href="/static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="192x192" type="image/png" href="/static-assets/images/favicon/fworks.png"/>

      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="on" http-equiv="cleartype"/>
      <meta content="true" name="HandheldFriendly"/>
      <meta content="320" name="MobileOptimized"/>
      <meta content="yes" name="apple-mobile-web-app-capable"/>
      <meta content="black-translucent" name="apple-mobile-web-app-status-bar-style"/>
      <meta content="telephone=no" name="format-detection"/>
      <meta content="address=no" name="format-detection"/>
    <?php include 'header.php';?>
<div class="error-page-container">
  <section id="" data-scroll-target="" class="align-center pattern-gradient-light first-fold ">
    <div class="container banner-content l-banner">
        <img src='static-assets/images/sample/404.png' />
      
      
  <p>Something went wrong<br />
You can browse our site or look for something specific.</p>

<p><span class="backward--link link"><a href="index.php">Take me home</a></span></p>



    </div>
  </section>
</div>
    <?php include 'footer.php';?>
  </body>
</html>
