
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Baba Careers | Join The Fastest Growing SaaS Start Up</title>
      <meta name="description" content="We might be looking for someone just like you. Find out if you have what it takes to work in the fastest growing startups in the country Baba Software.">
      <meta name="keywords" content="startup careers, software careers, Baba software, startup jobs, saas careers, Baba careers">
        <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png"/>
        <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png"/>

      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="on" http-equiv="cleartype"/>

      <meta property="og:url" content="">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="">
      <meta property="og:description" content="">

      <meta property="og:image" content="" />
      <meta name="og:image" content="" />

      <meta name="twitter:title" content="" />
      <meta name="twitter:description" content="" />
      <meta property="twitter:image" content="" />
      <meta content="true" name=""/>
      <meta content="320" name=""/>
      <meta content="yes" name=""/>
      <meta content="black-translucent" name=""/>
      <meta content="telephone=no" name=""/>
      <meta content="address=no" name=""/>
      <?php include 'header.php';?>
    

<section class="container">
  <img class="" data-src="" data-srcset="" src="static-assets/images/sample/career-banner.jpg" srcset=""/>
</section>

      
<section class="l-section" data-scroll-target="" id ="">
  <div class="l-article document">
    <h1 class="align-center">We’re expanding our team Join us</h1>
    <p class="sub-text">When talented people comes together they make great things happen</p>
    <p class="mt-sm">It is our task to unleash the potential in each team, and we understand that when they are diverse, teams perform best, and each team member feels they belong. It is the distinctive contributions of all Baba Software employees that drive our achievement, and we are dedicated to creating a culture in which everyone can do meaningful job and be acknowledged for their efforts. Each member is crucial to our achievement, and we are always looking for new talent.</p>
    <!--div class="align-center pt-sm">  
    <a title="" class="button button--solid " id="" target="" href=""> view openings</a>
    </div-->
    <p class="sub-text">So, if you:</p>
    <ol type="1">
      <li>Are motivated and hard-working</li>
      <li>Adore working in the challenging world of tech</li>
      <li>Are looking for a place where you will get the chance to advance your skills and abilities and develop new ones</li>
    </ol>  
Check our job openings and email us. We would appreciate you to be on our team.

  </div>
</section>

      
<section id="" class="l-section" data-scroll-target="">
  <div class="l-page container l-grid">
    <div class="l-section-heading">  
    <h2 class="align-center">Our Values </h2>
    <p class="align-center sub-text">Learn about the values that guide our business, our product development, and our brand. As our company continues to evolve and grow, our values remain constant.</p>
    </div>
    <div class="align-center">

        <div class="big-icon-widget l-col-widget">  
        <div class="feature-description-icon-d icon-full-width " style="margin: 0 auto; background-image: url(static-assets/images/sample/cjp88dsma00dsm7fzal3lgwil-craftsmanship-2x.full.png);"></div>
          <div class="  ">
          <div class="align-center">
          <h6>Open company to all</h6>
          <p>Openness is our root level. By default, information is accessible internally and sharing is a first principle. And we know that talking your mind needs brains of equal components (what to say), consideration (when to say it), and caring (how it is said).</p>
          </div>
        </div>
        </div>

        <div class="big-icon-widget l-col-widget">  
        <div class="feature-description-icon-d icon-full-width " style="margin: 0 auto; background-image: url(static-assets/images/sample/cjp88dt9i009gm2fzue4mtlpw-smart-risks-2x.full.png);"></div>
        <div class="  ">
        <div class="align-center">
        <h6>Build with your core and balance</h6>
        <p>"Measure twice, cut once." That's good advice whether you're constructing a birdhouse or a company. Passion and urgency infuse everything we do together with wisdom to take full and careful consideration of alternatives. Then we're making the cut and getting to work.</p>
        </div> 
        </div>
        </div>

        <div class="big-icon-widget l-col-widget">
          <div class="feature-description-icon-d icon-full-width " style="margin: 0 auto; background-image: url(static-assets/images/sample/cjp88dtdc00dum7fzaju8szb0-urgency-2x.full.png);"></div>
          <div class="  ">
          <div class="align-center">
          <h6>Play, as a team</h6>
          <p>We spend a great deal of our time at job. So the better the more time you don't feel like "work." Without taking us too seriously, we can be severe. We try to place first what's correct for the team – in a conference room.</p>
          </div> 
          </div>
        </div>

        <div class="big-icon-widget l-col-widget">
          <div class="feature-description-icon-d icon-full-width " style="margin: 0 auto; background-image: url(static-assets/images/sample/cjp88dst4009cm2fz4pe4hf1e-speak-up-2x.full.png);"></div>
          <div class="  ">
          <div class="align-center">
          <h6>Don’t delay the customers</h6>
          <p>Our lifeblood is our customers. We are doomed without happy customers. So considering the customer perspective - collectively, not just a handful - comes first.</p>
          </div> 
          </div>
        </div>

        <div class="big-icon-widget l-col-widget">    
          <div class="feature-description-icon-d icon-full-width " style="margin: 0 auto; background-image: url(static-assets/images/sample/8.png);"></div>
          <div class="  ">
          <div class="align-center">
          <h6>Be the change</h6>
          <p>All Baba software employees should have the courage and ingenuity to spark change – to create our goods, our people, our place better. Continuous improvement is a duty shared. It's an independent action.</p>
          </div> 
        </div>
      </div> 
    </div>
  </div>
</section>



<?php include 'footer.php';?>
  </body>
</html>
