<style>
  .footer-nav a:hover {
    color: #FFEB3B;
}
</style>

<footer style=" padding:0px;
    background-color: #373737;
    text-align: inherit;">          
  <div class="footer-main">
    <div class="footer-navs l-page clearfix-lg">
      <div class="row">
        <div class="col-md-4 footer-left-section">
          <div class="row">           
            <div class="col-sm-6">
              <div class="footer-nav-title footer-nav-title-logo">
                <p class="logo logo-fworks footer-logo"></p>
                <i class="icon-arrow-down"></i>
              </div>
              <ul class="footer-nav">
                <li><a href="https://baba.software/payroll-baba" class="">Payroll BABA</a></li>
                <li><a href="https://baba.software/crm-baba" class="">CRM BABA</a></li>
                <li><a href="https://baba.software/ats-baba" class="">ATS BABA</a></li>
                <li><a href="https://baba.software/lms-baba" class="">LMS BABA</a></li>
                <li><a href="https://baba.software/ar-baba" class="">AR BABA</a></li>
                <li><a href="https://baba.software/sales-baba" class="">SALES BABA</a></li>
                <li><a href="https://baba.software/hr-baba" class="">HR BABA</a></li>
                 
              </ul>
            </div>

            <div class="col-sm-6">
              <p class="footer-nav-title caps-heading-12">COMPANY<i class="icon-arrow-down"></i></p>
                <ul class="footer-nav">
                  <li><a href="about-us" class="">About us</a></li>
                  <li><a href="partner" class="">Partner</a></li>
                  <li><a href="pricing" class="">Pricing</a></li>
                  <li><a href="solution" class="">Solution</a></li>
                  <li><a href="support" class="">Support</a></li>
                  <li><a href="contact-us" class="">Contact us</a></li>
                    <li><a href="Blog" class="">Blog</a></li>
                </ul>
            </div>
          </div>
        </div>

        <div class="col-md-8 footer-right-section">
          <div class="row">
            <div class="col-sm-4">
              <p class="footer-nav-title caps-heading-12">EVENTS<i class="icon-arrow-down"></i></p>
                <ul class="footer-nav">
                  <li><a href=" " class="">Customer First Summit</a></li>
                </ul>
            </div>

            <div class="col-sm-4">
              <p class="footer-nav-title caps-heading-12">CASE STUDIES<i class="icon-arrow-down"></i></p>
                <ul class="footer-nav">
                  <li><a href=" " class="">Citizens Advice</a></li>
                </ul>
            </div>

            <div class="col-sm-4">
              <p class="footer-nav-title caps-heading-12">CONNECT WITH US<i class="icon-arrow-down"></i></p>
                <ul class="footer-nav social-connect-nav">
                  <li><a href=" " target=""><i class="icon-facebook"></i></a></li>
                  <li><a href=" " target=""><i class="icon-twitter"></i></a></li>
                  <li><a href=" " target=""><i class="icon-youtube"></i></a></li>
                  <li><a href=" " target=""><i class="icon-linkedin"></i></a></li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-copyrights">
    <div class="l-page clearfix-lg">
      <div class="footer-nav copyrights-nav hide-in-mobile">
        <a href="terms-condition">Terms of service</a>
        <a href="privacy">Privacy Notice</a>
        <a href="security">Security</a>
      </div>
      <p class="footer-copyrights-text">Copyright © Baba Software. All Rights Reserved</p>
    </div>
  </div>
</footer>

    <link href="static-assets/css/non-critical.css" rel="stylesheet" />
    <div id="modal-backdrop"></div>
    <div id="form-modal-holder"></div>
    <div id="eu-cookie-policy-popup">
      <div class="l-page">
        <div class="popup-container">
          <div class="text col-md-9"><p>We use cookies to offer you a better browsing experience, analyse site traffic, personalize content, and serve targeted advertisements. Read about how we use cookies in our <a href=" ">Privacy Notice</a>. Our <a href=" ">Cookie Policy</a> provides information about managing cookie settings.</p>
          </div>
          <div class="options col-md-3">
            <div class="button button--solid button--small button--black accept-cookies">
              Accept Cookies
            </div>
          </div>
        </div>
      </div>
    </div>
              

        
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>