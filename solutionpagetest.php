<!-- our-gallery strat-->
<!-- <div class="our-gallery pt-120 pb-100">
    <div class="container">
        <div class="section-header mb-80 text-center">
            <h2>Our Latest Works</h2>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod</p>

        </div>

        <div class="row">
            <div class="col-xl-4">
                <div class="gallery-item mb-30" data-gallery="http://localhost/Ultimate_SaaS/uploads/gallery/blog01.jpg">
                    <div class="gallery-text"> <a href="http://localhost/Ultimate_SaaS/uploads/gallery/blog01.jpg" class="popup-image"><i class="fal fa-plus"></i></a>
                        <p>Be bold and make a statement</p>

                    </div>

                </div>

            </div>

            <div class="col-xl-4">
                <div class="gallery-item mb-30" data-gallery="http://localhost/Ultimate_SaaS/uploads/gallery/blog02.jpg">
                    <div class="gallery-text"> <a href="http://localhost/Ultimate_SaaS/uploads/gallery/blog02.jpg" class="popup-image"><i class="fal fa-plus"></i></a>
                        <p>Be bold and make a statement</p>

                    </div>

                </div>

            </div>

            <div class="col-xl-4">
                <div class="gallery-item mb-30" data-gallery="http://localhost/Ultimate_SaaS/uploads/gallery/blog03.jpg">
                    <div class="gallery-text"> <a href="http://localhost/Ultimate_SaaS/uploads/gallery/blog03.jpg" class="popup-image"><i class="fal fa-plus"></i></a>
                        <p>Be bold and make a statement</p>

                    </div>

                </div>

            </div>

            <div class="col-xl-4">
                <div class="gallery-item mb-30" data-gallery="http://localhost/Ultimate_SaaS/uploads/gallery/blog02.jpg">
                    <div class="gallery-text"> <a href="http://localhost/Ultimate_SaaS/uploads/gallery/blog02.jpg" class="popup-image"><i class="fal fa-plus"></i></a>
                        <p>Be bold and make a statement</p>

                    </div>

                </div>

            </div>

            <div class="col-xl-4">
                <div class="gallery-item mb-30" data-gallery="http://localhost/Ultimate_SaaS/uploads/gallery/blog03.jpg">
                    <div class="gallery-text"> <a href="http://localhost/Ultimate_SaaS/uploads/gallery/blog03.jpg" class="popup-image"><i class="fal fa-plus"></i></a>
                        <p>Be bold and make a statement</p>

                    </div>

                </div>

            </div>

            <div class="col-xl-4">
                <div class="gallery-item mb-30" data-gallery="http://localhost/Ultimate_SaaS/uploads/gallery/blog01.jpg">
                    <div class="gallery-text"> <a href="http://localhost/Ultimate_SaaS/uploads/gallery/blog01.jpg" class="popup-image"><i class="fal fa-plus"></i></a>
                        <p>Be bold and make a statement</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div> -->
<section class="first-fold gradient-light" data-scroll-target="" id="">
    <div class="l-page container pb-lg l-home banner-right-image">
        <div class="row ">
            <div class="col-md-6  mobile-center-desktop-left-align mb-lg banner-text-content ">
                <h1>Build Great Relationships With Customers For Life Software</h1>
                <p>Create effortless organisation with a centralised hub, where everything is in one place. Our cloud Baba software gives full visibility of your customer interactions and builds upon the strength of your team and relationships.</p>

                <p>Software that’s easy-to-use doesn’t necessarily mean it's only for small organizations. We have designed for Baba Software for large businesses because time-to-value is so short. Small businesses grow into big businesses with us. And big businesses choose us for our flexibility, scalability, and extensibility. Baba Software is easy to set up and simple to use. So easy, you’ll be able to train your squad in minutes.</p>

            </div>
            <div class="col-md-6 ">
                <div class="" style="">
                    <img src="https://website-assets-fw.freshworks.com/attachments/cjtr77dgz013jvcg0fdysnsx8-solutions-banner.svg">
                </div>
            </div>
        </div>
    </div>
</section>


<section id="24988571728496" class="section-bucket l-section  " data-scroll-target="">
    <div class="feature-screenshot-left ">
        <div class="l-page container">
            <div class="row">
                <div class=" feature-screenshot-left__content col-md-6">
                    <h3 class="feature-heading">Empower Your Sales Team</h3>
                    <p>Our sales software from Baba enables you to operate more intelligently, manage and track your leads and clients so that you can grow your company. The Sales Baba greatest advantage is bringing the entire sales process under one roof. We have everything in one place, making it more effective by streamlining it. Baba sales, which monitors all interactions and defines your greatest opportunities as they move through your pipeline.</p>
                </div>
                <div class="feature-screenshot-left__media feature-screenshot__media-container col-md-6">
                    <div class="" style="">
                        <img class="" src="https://website-assets-fw.freshworks.com/attachments/cjtr79tts0176vdg0m0wl14yh-ultimate-customer-engagement.svg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="107763215579208" class="section-bucket l-section  " data-scroll-target="">
    <div class="feature-screenshot-right ">
        <div class="l-page container">
            <div class="row">
                <div class=" feature-screenshot-right__content col-md-6">
                    <h3 class="feature-heading">Digital Evolution With IT Team</h3>
                    <p>Plan, track, and collaborate using the preferred online Baba software IT for more than a million businesses. Be right where your customers are with apps to help your business engage with them. Simplify IT with an ITSM solution that is extremely easy yet strong. In just a few hours, Baba Software can be deployed with unrivaled time-to-value and low TCO. There was no need for engineers and consultants. Baba Software contains a host of ITIL-aligned characteristics in addition to multi-channel assistance, asset management and strong analytics, all available via a smooth, intuitive user interface. As we strongly believe happy employees = happy customers.</p>
                </div>
                <div class="feature-screenshot-right__media feature-screenshot__media-container col-md-6">
                    <div class="" style="">
                        <img class=" " src="https://website-assets-fw.freshworks.com/attachments/cjtr7bm02016gvcg061zh5rzo-expand-engagement.svg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="29673928927608" class="section-bucket l-section  " data-scroll-target="">
    <div class="feature-screenshot-left ">
        <div class="l-page container">
            <div class="row">
                <div class=" feature-screenshot-left__content col-md-6">
                    <h3 class="feature-heading">Lift Your Customer With Support Team</h3>
                    <p>The distinction between average and outstanding can be a solid customer support presence. Your market reputation often relies strongly on your client support quality and the resulting data that word-of-mouth shares. The comprehensive customer support functions of Baba CRM enable you to raise your efforts to the next level and retain long-term satisfied clients.</p>

                    <p>Baba Software Support enables customers streamline all discussions with their customers in one location, automate repetitive job and save time. Invite anyone to fix tickets using Baba Software-agents, peers, or internal company associates. In order to drive efficiency enhancement, evaluate and report contact center metrics such as First Reply Time or Average Handle Time.</p>
                </div>
                <div class="feature-screenshot-left__media feature-screenshot__media-container col-md-6">
                    <div class="" style="">
                        <img class=" " src="https://website-assets-fw.freshworks.com/attachments/cjtr7dp7k018qvcg0d9nlp9aa-simplicity-experience.svg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="l-section" data-scroll-target="" id="">
    <div class="l-page container">
        <div class="l-section-heading align-center">
            <h2 class="align-center">Trusted by over <strong>many</strong> businesses around the world</h2>
        </div>
        <div class="customer-logos">

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/1.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/2.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/3.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/4.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/5.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/6.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/7.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/8.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/9.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/10.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/11.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/12.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/13.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/14.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/15.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/16.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/17.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/18.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/19.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/20.gif" srcset="">
                </div>
            </div>

            <div class="customer-logo">
                <div class="" style="">
                    <img class="lazy-image" data-src="" data-srcset="" src="static-assets/images/sample/21.gif" srcset="">
                </div>
            </div>

        </div>
    </div>
</section>

<section id="" data-scroll-target="#" class=" align-center pattern-gradient-light    ">
    <div class="container  banner-content l-banner " data-scroll-target="">
        <h1>Say hello to the Baba Software customer-for-life experience</h1>
        <p class="sub-text">All products come with a free trial. No credit card required. No strings attached.</p>
        <div class="mt-md">
            <a title="" class="button button--solid " id="" target="" href=""> GET STARTED</a>
        </div>
    </div>
</section>
<!-- our-gallery end-->