<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Baba - Contact Us</title>
  <meta name="description" content="Find contact information for Baba Offices, sales enquiries, technical support, features, trials, pricing, need a demo, or anything else, our team is ready to answer all your questions">
  <meta name="keywords" content="Contact us">
  <link rel="icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png" />
  <link rel="icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png" />
  <link rel="icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png" />
  <link rel="icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png" />
  <link rel="apple-touch-icon" size="16x16" type="image/png" href="static-assets/images/favicon/fworks.png" />
  <link rel="apple-touch-icon" size="96x96" type="image/png" href="static-assets/images/favicon/fworks.png" />
  <link rel="apple-touch-icon" size="32x32" type="image/png" href="static-assets/images/favicon/fworks.png" />
  <link rel="apple-touch-icon" size="192x192" type="image/png" href="static-assets/images/favicon/fworks.png" />

  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="on" http-equiv="cleartype" />

  <meta property="og:url" content="">
  <meta property="og:type" content="website" />
  <meta property="og:title" content="">
  <meta property="og:description" content="">

  <meta property="og:image" content="" />
  <meta name="og:image" content="" />

  <meta name="twitter:title" content="" />
  <meta name="twitter:description" content="" />
  <meta property="twitter:image" content="" />
  <meta content="true" name="" />
  <meta content="320" name="" />
  <meta content="yes" name="" />
  <meta content="black-translucent" name="" />
  <meta content="telephone=no" name="" />
  <meta content="address=no" name="" />
  <?php include 'header.php'; ?>


  <section id="" data-scroll-target="#" class="first-fold align-center pattern-gradient-light    ">
    <div class="container  banner-content l-banner " data-scroll-target="">


      <h1 class="align-center">Contact Us</h1>

      <h2 class="sub-text">We look forward to hearing from you!</h2>



    </div>
  </section>






  <section class="pb-xl ">
    <div class="l-page container" data-scroll-target="" id="">
      <div class="row">
        <div class="col-md-6 col-sm-12 mt-xl">


          <div class="icon-big " style=" background-image: url(static-assets/images/sample/cjpv3wcq0003llpfzzxodiien-enquiry.svg);"></div>
          <div class="  ">
            <h3>Sales Enquiries</h3>

            <p class="pt-xs sub-text">Interested in any of our products? Talk to our experts today</p>

            <p class="pt-xs sub-text"><strong>US:</strong> |<strong> India:</strong> </p>

            <p class="pt-xs sub-text"><strong>Email:</strong> info@baba.software</p>

            <p class="pt-xs sub-text">(Availability 24*5)</p>
          </div>



          <div class="mt-md mb-md border-grey ">
            <div class="hr"></div>
          </div>


          <div class="icon-big " style=" background-image: url(static-assets/images/sample/cjpv3wcqc004ulufzissvblbw-support.svg);"></div>
          <div class="  ">
            <h3>Support Enquiries</h3>

            <p class="sub-text">If you have questions, comments or suggestions about our website or services or product please get in touch with us.</p>

            <p class="pt-xs sub-text">Questions? Comments? Suggestions?<br />
              Contact us at <a mailto="info@baba.software">info@baba.software</a> / <a mailto="support@baba.software">support@baba.software</a> or complete the form below.
            </p>
          </div>



        </div>
        <div class="col-md-6 col-sm-12 pt-lg">


          <div class="form-wrapper contact-form-wrapper form-white-bg">
            <h3 class="mb-md">Get Support</h3>
            <p>Let us know how we're doing</p>
            <form autocomplete="off" id="" class="form-field-container fwork-contactform" novalidate="novalidate">
              <input style="display:none" class="list-id" name="list-id" value="8DA3990E-F903-41ED-906C-E8FCC0414C98">
              <fieldset>
                <div class="name-field-wrapper">
                  <div class="name-field">
                    <div class="form-field">
                      <i class="icon-user"></i>
                      <input type="text" name="first-name" class="first-name-form" aria-describedby="first-name-error" data-first-name-required="First name is required">
                      <label class="form-placeholder">First Name</label>
                      <div class="error-wrapper">
                      </div>
                    </div>
                  </div>
                  <div class="name-field ">
                    <div class="form-field">
                      <input type="text" name="last-name" class="last-name-form" aria-describedby="last-name-error" data-last-name-required="Last name is required">
                      <label class="form-placeholder">Last Name</label>
                      <div class="error-wrapper">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-field">
                  <i class="icon-email"></i>
                  <input type="email" name="email" class="email-form" aria-describedby="email-error" data-email-required="Please enter an email" data-email-invalid="Please enter a valid email address">
                  <label class="form-placeholder">Email</label>
                  <div class="error-wrapper">
                  </div>
                </div>
                <div class="form-field field-dropdown">
                  <i class="icon-agent"></i>
                  <select name="query-contact" aria-describedby="query-contact-error" class="" aria-invalid="true" style="opacity: 1;" data-select-required>
                    <option class="hide"></option>
                    <option value="Support">Support</option>
                    <option value="Sales">Sales</option>
                    <option value="Press and Media">Press and Media</option>
                  </select>
                  <i class="icon-arrow-down"></i>
                  <label class="form-placeholder">Type of query</label>
                  <div class="error-wrapper">

                  </div>
                </div>
                <div class="form-field">
                  <i class="icon-mobile"></i>
                  <input type="text" name="phone" class="phone-form" aria-describedby="phone-error" data-phone-required="Phone number is required" data-phone-invalid="Only numbers and hyphen allowed">
                  <label class="form-placeholder">Phone Number</label>
                  <div class="error-wrapper">

                  </div>
                </div>
                <div class="form-field form-textarea form-message">
                  <textarea cols="10" rows="10" class="message-form" name="message" data-message-required="Please enter your message"></textarea>
                  <label class="form-placeholder">Your message</label>
                  <div class="error-wrapper"></div>
                </div>
              </fieldset>
              <fieldset>
                <input type="submit" value="Submit" name="btn--form" class="button button--solid button--block">
                <div class="signup-terms align-center">
                  <p>By clicking on <strong>SUBMIT</strong>, you acknowledge having read our <a href="" target="">Privacy notice</a></p>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>



  <section id="" class="l-section section-light-bg" data-scroll-target="">
    <div class="l-page container l-grid">
      <div class="l-section-heading">
        <h2 class="align-center">Office locations</h2>
      </div>

      <div class="row">
        <div class="l-col-widget col-sm-4">
          <div class="icon-small " style=" background-image: url(static-assets/images/sample/cjn1dneol00gb08g01sn09622-usa.svg);"></div>
          <div class="  ">
            <h6>HQ - Fremont, USA</h6>
            <p class="no-margin">4575 Hilo Street</p>
            <p class="no-margin">Fremont, CA 94538 USA</p>
            <p class="no-margin">Reception/General enquiries: <a href="tel:+1 510-516-7800">+1 510-516-7800</a></p>
            <p class="no-margin">Availability: (Mon-Fri) 9AM to 5PM PST</p>
          </div>
        </div>
        <!-- <div class="l-col-widget col-sm-4">
          <div class="icon-small " style=" background-image: url(static-assets/images/sample/cjn1dneoi00hb0rg08yxffq9u-india.svg);"></div>
          <div class="  ">
            <h6>Noida, India</h6>
            <p class="no-margin">A-40 I Thum Tower-B 7th Floor</p>
            <p class="no-margin">Unit No. 26 Sector-62 Noida - 201309</p>
            <p class="no-margin">Reception/General enquiries: <a href="">+91-878.956.2315</a></p>
            <p class="no-margin">Availability: (Mon-Fri) 9AM to 5PM IST</p>
          </div>
        </div>
        <div class="l-col-widget col-sm-4">
          <div class="icon-small " style=" background-image: url(static-assets/images/sample/cjn1dneoi00hb0rg08yxffq9u-india.svg);"></div>
          <div class="  ">
            <h6>Jamshedpur, India</h6>
            <p class="no-margin">Adityapur Auto Cluster</p>
            <p class="no-margin">Near Adityapur Toll Bridge, Tata Kandra Main Road</p>
            <p class="no-margin">Adityapur, Jamshedpur, Jharkhand 832109, INDIA</p>
            <p class="no-margin">Reception/General enquiries: <a href="">+91-878.956.2315</a></p>
            <p class="no-margin">Availability: (Mon-Fri) 9AM to 5PM IST</p>
          </div>
        </div> -->
      </div>
    </div>
  </section>



  <?php include 'footer.php'; ?>
  </body>

</html>