<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Version_121 extends CI_Migration
{
    function __construct()
    {
        parent::__construct();
    }

    public function up()
    {
        $this->db->query("ALTER TABLE `tbl_allowed_ip` ADD `companies_id` INT(11) NULL DEFAULT NULL AFTER `allowed_ip_id`;");
        $this->db->query("UPDATE `tbl_config` SET `value` = '1.2.1' WHERE `tbl_config`.`config_key` = 'version';");
    }
}
