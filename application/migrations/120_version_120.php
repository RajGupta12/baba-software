<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Version_120 extends CI_Migration
{
    function __construct()
    {
        parent::__construct();
    }

    public function up()
    {
        $this->db->query("ALTER TABLE `tbl_return_stock` ADD `companies_id` INT(11) NULL DEFAULT NULL AFTER `return_stock_id`;");
        $this->db->query("UPDATE `tbl_config` SET `value` = '1.2.0' WHERE `tbl_config`.`config_key` = 'version';");
    }
}
