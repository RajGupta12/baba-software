<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms_media_model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    function fetch_details($limit, $start, $st = 'img', $media_type = NULL) {
        $output = '';
        $this->db->select("*");
        $this->db->like('img_name', $st);
        $this->db->like('file_type', $media_type);
        $this->db->from("front_cms_media_gallery");
        $this->db->order_by("id", "DESC");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();
    }

    function count_all($st = NULL, $media_type = NULL) {
        $this->db->like('file_type', $media_type);
        $this->db->like('img_name', $st);
        $query = $this->db->get("front_cms_media_gallery");
        return $query->num_rows();
    }

    public function remove($id) {
        $this->db->where('id', $id);
        $this->db->delete('front_cms_media_gallery');
        if ($this->db->affected_rows() > 0)
            return true;
        else
            return false;
    }

//    public function get($id = null) {
//        $this->db->select()->from('front_cms_media_gallery');
//        if ($id != null) {
//            $this->db->where('id', $id);
//        } else {
//            $this->db->order_by('id');
//        }
//        $query = $this->db->get();
//        if ($id != null) {
//            return $query->row_array();
//        } else {
//            return $query->result();
//        }
//    }
}
