<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setup extends MY_Controller
{
    protected $subsciptions_id = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('settings_model');

        if (!empty($_GET)) {
            $subsciptions_id = $_GET['result'];
            $this->subsciptions_id = url_decode($subsciptions_id);
            $is_active = get_active_subs($this->subsciptions_id);
            if (!empty($is_active)) {
                redirect('login');
            }
        }
    }

    public function index($id = null)
    {
        $data['title'] = lang('welcome_to') . ' ' . config_item('company_name');
        $data['step'] = 1;
        $email = $this->input->post('email', true);
        if (!empty($email)) {
            $data = $this->settings_model->array_from_post(array('activation_token', 'username', 'email', 'password'));
            $activation_token = $this->input->post('activation_token', true);
            $username = $this->input->post('username', true);
            $email_error = $this->check_subscription_email($email, true);
            $activation_token_error = $this->check_existing_activation_token($activation_token, $email, true);
            if (!empty($email_error['error'])) {
                $data['step'] = 1;
                $data['email_error'] = $email_error['error'];
                $data['error'] = true;
            }
            if (!empty($activation_token_error['error'])) {
                $data['step'] = 1;
                $data['activation_token_error'] = $activation_token_error['error'];
                $data['error'] = true;
            }
            if (empty($data['error']) && isset($_POST['step']) && $_POST['step'] == 1) {
                $data['step'] = 2;
                // get all timezone
                $data['timezones'] = $this->settings_model->timezones();
            } elseif (isset($_POST['step']) && $_POST['step'] == 2) {
                $data['step'] = 3;
            } elseif (isset($_POST['step']) && $_POST['step'] == 3) {
                $validate_result = $this->validate_email($_POST);
                if (!empty($validate_result['error'])) {
                    $data['step'] = 3;
                } else {
                    $data['step'] = 4;
                }
            } elseif (isset($_POST['step']) && $_POST['step'] == 4) {
                $subscription = get_row('tbl_subscriptions', array('subscriptions_id' => $this->subsciptions_id));
                if (!empty($subscription)) {
                    // create config
                    $companies_id = $subscription->subscriptions_id;
                    $this->update_user_details($subscription);

                    $this->create_config($companies_id);
                    // create working days
                    $this->create_working_days($companies_id);
                    // create  menu
                    $this->create_menu($companies_id);

                    $this->create_dashboard($companies_id);
                    // create client menu
                    $this->create_client_menu($companies_id);
                    // create email template
                    $this->create_email_template($companies_id);

                    $this->update_subscription($subscription);

                    $data['step'] = 5;
                } else {
                    $data['step'] = 4;
                }
            } elseif (isset($_POST['step']) && $_POST['step'] == 5) {
                redirect('login');
            }
        }
        $this->load->view('admin/settings/setup', $data);
    }

    private function create_config($companies_id = null)
    {
        $config_data = $this->db->where('companies_id', null)->get('tbl_config')->result_array();
        $configs = array();
        foreach ($config_data as $config) {
            $config['companies_id'] = $companies_id;
            array_push($configs, $config);
        }
        $this->db->insert_batch('tbl_config', $configs);
        return true;
    }

    private function create_working_days($companies_id = null)
    {
        $all_working_day = $this->db->where('companies_id', null)->get('tbl_working_days')->result();
        $working_day = array();
        if (!empty($all_working_day)) {
            foreach ($all_working_day as $working_days) {
                $working_days->working_days_id = null;
                $working_days->companies_id = $companies_id;
                array_push($working_day, $working_days);
            }
            $this->db->insert_batch('tbl_working_days', $working_day);
        }
        return true;
    }

    function create_menu($companies_id = null)
    {
        $all_menu = $this->db->where('companies_id', null)->get('tbl_menu')->result();
        $menu = array();
        if (!empty($all_menu)) {
            foreach ($all_menu as $menus) {
                $menus->menu_id = null;
                $menus->companies_id = $companies_id;
                array_push($menu, $menus);
            }
        }
        $this->db->insert_batch('tbl_menu', $menu);
        
        $all_company_menu = $this->db->where(array('companies_id' => $companies_id, 'parent !=' => 0))->get('tbl_menu')->result();
        if (!empty($all_company_menu)) {
            foreach ($all_company_menu as $c_menu) {
                $parent_label = $this->db->where(array('menu_id' => $c_menu->parent))->get('tbl_menu')->row();
                $new_id = get_any_field('tbl_menu', array('label' => $parent_label->label, 'companies_id' => $companies_id), 'menu_id');
                update('tbl_menu', array('companies_id' => $companies_id, 'parent' => $parent_label->menu_id), array('parent' => $new_id));
            }
        }
        return true;
    }

    private function create_dashboard($companies_id = null)
    {
        $all_menu = $this->db->where('companies_id', null)->get('tbl_dashboard')->result();
        $menu = array();
        if (!empty($all_menu)) {
            foreach ($all_menu as $menus) {
                $menus->id = null;
                $menus->companies_id = $companies_id;
                array_push($menu, $menus);
            }
        }
        $this->db->insert_batch('tbl_dashboard', $menu);
        return true;
    }

    private function create_client_menu($companies_id = null)
    {
        $all_client_menu = $this->db->where('companies_id', null)->get('tbl_client_menu')->result();
        $menu = array();
        if (!empty($all_client_menu)) {
            foreach ($all_client_menu as $client_menus) {
                $client_menus->menu_id = null;
                $client_menus->companies_id = $companies_id;
                array_push($menu, $client_menus);
            }
        }
        $this->db->insert_batch('tbl_client_menu', $menu);
        return true;
    }

    private function create_email_template($companies_id = null)
    {
        $all_email_templates = $this->db->where('companies_id', null)->get('tbl_email_templates')->result();
        $templates = array();
        if (!empty($all_email_templates)) {
            foreach ($all_email_templates as $email_templates) {
                $email_templates->email_templates_id = null;
                $email_templates->companies_id = $companies_id;
                array_push($templates, $email_templates);
            }
        }
        $this->db->insert_batch('tbl_email_templates', $templates);
        return true;
    }

    private function update_subscription($subscription)
    {
        $s_data['status'] = 'running';
        $s_data['is_trial'] = 'Yes';
        $this->settings_model->_table_name = 'tbl_subscriptions';
        $this->settings_model->_primary_key = 'subscriptions_id';
        $this->settings_model->save($s_data, $subscription->subscriptions_id);

        $sh_data['status'] = 'running';
        $sh_info = get_row('tbl_subscriptions_history', array('subscriptions_id' => $subscription->subscriptions_id, 'status' => 'pending'));
        if (!empty($sh_info)) {
            $this->settings_model->_table_name = '  tbl_subscriptions_history';
            $this->settings_model->_primary_key = 'id';
            $this->settings_model->save($sh_data, $sh_info->id);
        } else {
            $plan_info = get_row('tbl_frontend_pricing', array('id' => $subscription->pricing_id));
            $amount_info = get_row('tbl_currencywise_price', array('frontend_pricing_id' => $subscription->pricing_id, 'currency' => $subscription->currency));
            if (!empty($amount_info)) {
                if ($subscription->frequency == 'yearly') {
                    $amount = $amount_info->yearly;
                } else {
                    $amount = $amount_info->monthly;
                }
            } else {
                $amount = '';
            }
            $sh_data['subscriptions_id'] = $subscription->subscriptions_id;
            $sh_data['currency'] = $subscription->currency;
            $sh_data['frequency'] = $subscription->frequency;
            $sh_data['amount'] = $amount;
            $sh_data['ip'] = $this->input->ip_address();
            $sh_data['name'] = $plan_info->name;
            $sh_data['employee_no'] = $plan_info->employee_no;
            $sh_data['client_no'] = $plan_info->client_no;
            $sh_data['project_no'] = $plan_info->project_no;
            $sh_data['invoice_no'] = $plan_info->invoice_no;
            $sh_data['leads'] = $plan_info->leads;
            $sh_data['accounting'] = $plan_info->accounting;
            $sh_data['bank_account'] = $plan_info->bank_account;
            $sh_data['online_payment'] = $plan_info->online_payment;
            $sh_data['calendar'] = $plan_info->calendar;
            $sh_data['mailbox'] = $plan_info->mailbox;
            $sh_data['live_chat'] = $plan_info->live_chat;
            $sh_data['tickets'] = $plan_info->tickets;
            $sh_data['tasks'] = $plan_info->tasks;
            $sh_data['filemanager'] = $plan_info->filemanager;
            $sh_data['stock_manager'] = $plan_info->stock_manager;
            $sh_data['recruitment'] = $plan_info->recruitment;
            $sh_data['attendance'] = $plan_info->attendance;
            $sh_data['payroll'] = $plan_info->payroll;
            $sh_data['leave_management'] = $plan_info->leave_management;
            $sh_data['performance'] = $plan_info->performance;
            $sh_data['training'] = $plan_info->training;
            $sh_data['reports'] = $plan_info->reports;
            $sh_data['disk_space'] = $plan_info->disk_space;
            $sh_data['status'] = 'running';

            $this->settings_model->_table_name = 'tbl_subscriptions_history';
            $this->settings_model->_primary_key = 'id';
            $this->settings_model->save_data($sh_data);
        }
        return true;
    }

    public function update_user_details($subscription)
    {
        $dep['deptname'] = $_POST['department'];
        $dep['companies_id'] = $subscription->subscriptions_id;
        $this->settings_model->_table_name = 'tbl_departments';
        $this->settings_model->_primary_key = 'departments_id';

        $departments_id = $this->settings_model->save_data($dep);

        if ($departments_id != 0) {
            $des['departments_id'] = $departments_id;
            $des['designations'] = $_POST['designation'];
            $this->settings_model->_table_name = '  tbl_designations';
            $this->settings_model->_primary_key = 'designations_id';
            $designations_id = $this->settings_model->save_data($des);
        } // save user details start
        $login_data['email'] = $_POST['email'];
        $login_data['username'] = $_POST['username'];
        $login_data['password'] = $this->hash($_POST['password']);
        $login_data['role_id'] = 1;
        $login_data['activated'] = 1;
        $login_data['companies_id'] = $subscription->subscriptions_id;
        $login_data['super_admin'] = null;
        $login_data['created'] = date('Y-m-d H:i:s');
        $login_data['media_path_slug'] = slug_it($_POST['username']);

        $existing_username = get_row('tbl_users', array('username' => $_POST['username']));

        if (empty($existing_username)) {
            $this->settings_model->_table_name = ' tbl_users';
            $this->settings_model->_primary_key = 'user_id';
            $user_id = $this->settings_model->save_data($login_data);
            if ($user_id != 0) {
                $acc_data['user_id'] = $user_id;
                $acc_data['fullname'] = $_POST['fullname'];
                $acc_data['city'] = $_POST['company_city'];
                $acc_data['country'] = $_POST['company_country'];
                $acc_data['address'] = $_POST['company_address'];
                $acc_data['designations_id'] = $designations_id;

                $this->settings_model->_table_name = '  tbl_account_details';
                $this->settings_model->_primary_key = 'account_details_id';
                $this->settings_model->save_data($acc_data);

            }
        } else {
            $this->settings_model->_table_name = ' tbl_users';
            $this->settings_model->_primary_key = 'user_id';
            $this->settings_model->save_data($login_data, $existing_username->user_id);
        }
        // save user details end
        return true;
    }

    public function validate_email($post_data)
    {
        $config = array();
        // If postmark API is being used
        if ($post_data['use_postmark'] == 'TRUE') {
            $config = array(
                'api_key' => $post_data['postmark_api_key']
            );
            $this->load->library('postmark', $config);
            $this->postmark->from($post_data['postmark_from_address'], $post_data['company_name']);
            $this->postmark->to($post_data['email']);
            $this->postmark->subject('SMTP Setup Testing');
            $this->postmark->message_plain('This is test SMTP email. <br />If you received this message that means that your SMTP settings is Corrects.');
            $this->postmark->message_html('This is test SMTP email. <br />If you received this message that means that your SMTP settings is Corrects.');

            $this->postmark->send();
        } else {
            // If using SMTP
//            if (config_item('protocol') == 'smtp') {
//                $this->load->library('encrypt');
//                $config = array(
//                    'protocol' => config_item('protocol'),
//                    'smtp_host' => config_item('smtp_host'),
//                    'smtp_port' => config_item('smtp_port'),
//                    'smtp_user' => config_item('smtp_user'),
//                    'smtp_pass' => config_item('smtp_pass'),
//                    'smtp_crypto' => config_item('email_encryption'),
//                    'crlf' => "\r\n"
//                );
//            }
            // Send email
            $config['useragent'] = 'UniqueCoder LTD';
//            $config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = "html";
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['crlf'] = "\r\n";
            $config['protocol'] = $post_data['protocol'];
            $config['smtp_host'] = $post_data['smtp_host'];
            $config['smtp_port'] = $post_data['smtp_port'];
            $config['smtp_timeout'] = '30';
            $config['smtp_user'] = $post_data['smtp_user'];
            $config['smtp_pass'] = decrypt($post_data['smtp_pass']);
            $config['smtp_crypto'] = $post_data['smtp_encryption'];

            $this->load->library('email', $config);
            $this->email->from($post_data['email'], $post_data['company_name']);
            $this->email->to($post_data['email']);

            $this->email->subject('SMTP Setup Testing');
            $this->email->message('This is test SMTP email. <br />If you received this message that means that your SMTP settings is Corrects.');
            $send = $this->email->send();
            $result['success'] = 1;

            return $result;

        }
    }


    public function hash($string)
    {
        $encryption_key = 'I6PnEPbQNLslYMj7ChKxDJ2yenuHLkXn';
        return hash('sha512', $string . $encryption_key);
    }

    public function register()
    {
        $data['title'] = lang('welcome_to') . ' ' . config_item('company_name');
        $data['subview'] = $this->load->view('login/register', $data, TRUE);
        $this->load->view('login', $data);
    }

    public function check_existing_user_name($username = null, $front = null)
    {
        if (!empty($this->input->post('name', true))) {
            $username = $this->input->post('name', true);
        }
        if (!empty($username)) {
            $check_user_name = $this->admin_model->check_user_name($username);
            if (!empty($check_user_name)) {
                $result['error'] = lang("name_already_exist");
            } else {
                $result['success'] = 1;
            }
            if (empty($front)) {
                echo json_encode($result);
                exit();
            } else {
                return $result;
            }
        }
    }


    public function check_subscription_email($email = null, $front = null)
    {

        if (!empty($this->input->post('name', true))) {
            $email = $this->input->post('name', true);
            $username = $this->input->post('username', true);
        }
        if (!empty($email)) {
            $result['email'] = $email;

            $where = array('email' => $email);
            $check_subs_email = $this->admin_model->check_by($where, 'tbl_subscriptions');
            $check_email_address = $this->admin_model->check_by(array('email' => $email), 'tbl_users');
            if (!empty($username)) {
                $check_username = $this->admin_model->check_by(array('username' => $username), 'tbl_users');
            }
            if (!empty($check_subs_email) || !empty($check_email_address) || !empty($check_username)) {
                if (!empty($check_email_address)) {
                    $result['error'] = lang("this_email_already_exist_someone_already_login");
                } elseif (!empty($check_username)) {
                    $result['error'] = lang('your_username_already_used', $check_username);
                } elseif ($check_subs_email->status != 'pending') {
                    $result['error'] = lang('your_email_already_used', $check_subs_email->status);
                } else {
                    $result['success'] = 1;
                }
            } else {
                $result['error'] = lang('we_did_not_found_your_email');
            }
            if (empty($front)) {
                echo json_encode($result);
                exit();
            } else {
                return $result;
            }
        }
    }

    public function check_existing_activation_token($activation_token = null, $email = null, $front = null)
    {
        if (!empty($this->input->post('name', true))) {
            $activation_token = $this->input->post('name', true);
            $email = $this->input->post('name_2', true);
        }
        if (!empty($email) || !empty($activation_token)) {
            $check_email = $this->admin_model->check_by(array('email' => $email, 'activation_tocken' => $activation_token), 'tbl_subscriptions');
            if (!empty($check_email)) {
                $result['success'] = 1;
            } else {
                $result['error'] = lang('we_did_not_found_your_activation_token');
            }
            if (empty($front)) {
                echo json_encode($result);
                exit();
            } else {
                return $result;
            }
        }
    }


    public function check_username()
    {

        if (!empty($this->input->post('name', true))) {
            $username = $this->input->post('username', true);
        }
        if (!empty($username)) {

            $check_username = $this->admin_model->check_by(array('username' => $username), 'tbl_users');
            if (!empty($check_username)) {
                $result['success'] = 1;
            } else {
                $result['error'] = lang('your_username_already_used', $check_username);
            }
            echo json_encode($result);
            exit();
        }
    }

}

