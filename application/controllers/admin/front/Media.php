<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Media extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('cms_media_model');
        $this->load->library('imageResize');
        if (empty(super_admin())) {
            redirect('404');
        }
    }

    public function index()
    {
        $data['active'] = 1;
        $data['title'] = lang('media');
        $data['dropzone'] = true;

        $data['subview'] = $this->load->view('admin/front/media/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function pageList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'front_cms_pages';
            $this->datatables->join_table = array('front_cms_page_contents');
            $this->datatables->join_where = array('front_cms_page_contents.page_id=front_cms_pages.pages_id');
            $this->datatables->column_search = array('title', 'front_cms_page_contents.content_type');
            $this->datatables->order = array('front_cms_pages.pages_id' => 'asc');
            $fetch_data = make_datatables();

            $data = array();
            $edited = can_action('4', 'edited');
            $deleted = can_action('4', 'deleted');

            foreach ($fetch_data as $_key => $pages) {
                $action = null;
                $sub_array = array();

                $sub_array[] = $pages->title;
                $sub_array[] = '<a target="_blank" href="' . base_url() . $pages->url . '">' . base_url() . $pages->url . '<a>';

                if ($pages->content_type == "gallery") {
                    $sub_array[] = '<span class="label label-green">' . $pages->content_type . '</span>';
                } elseif ($pages->content_type == "events") {
                    $sub_array[] = '<span class="label label-info">' . $pages->content_type . '</span>';
                } elseif ($pages->content_type == "notice") {
                    $sub_array[] = '<span class="label label-warning">' . $pages->content_type . '</span>';
                } else {
                    $sub_array[] = '<span class="label label-default">' . lang("standard") . '</span>';
                }

                if (!empty($edited)) {
                    $action .= btn_edit('admin/front/page/index/' . $pages->pages_id) . ' ';
                }
                if (!empty($deleted) && $pages->page_type != "default") {
                    $action .= ajax_anchor(base_url("admin/front/page/delete_page/$pages->pages_id"), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key));
                }

                $sub_array[] = $action;
                $data[] = $sub_array;
            }
            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function add_media($id = null)
    {
        $data['title'] = lang('new') . ' ' . lang('announcements'); //Page title

        $data['subview'] = $this->load->view('admin/front/media/add_media', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data); //page load
    }

    public function save_media()
    {
        $this->cms_media_model->_table_name = "front_cms_media_gallery"; //table name
        $this->cms_media_model->_primary_key = "id";
        $created = can_action(153, 'created');
        if (!empty($created)) {
            $upload_file = array();

            $files = $this->input->post("files", true);
            $target_path = getcwd() . "/uploads/gallery/";
            //process the fiiles which has been uploaded by dropzone
            if (!empty($files) && is_array($files)) {
                foreach ($files as $key => $file) {
                    if (!empty($file)) {
                        $file_name = $this->input->post('file_name_' . $file, true);
                        $new_file_name = move_temp_file($file_name, $target_path);
                        $file_ext = explode(".", $new_file_name);
                        $is_image = check_image_extension($new_file_name);
                        $size = $this->input->post('file_size_' . $file, true) / 1000;

                        if ($new_file_name) {
                            $up_data = array(
                                "fileName" => $new_file_name,
                                "path" => "uploads/gallery/",
                                "fullPath" => getcwd() . "/uploads/gallery/" . $new_file_name,
                                "ext" => '.' . end($file_ext),
                                "size" => round($size, 2),
                                "is_image" => $is_image,
                            );
                            array_push($upload_file, $up_data);
                        }
                    }
                }
            }

            if (!empty($upload_file)) {
                foreach ($upload_file as $u_value) {
                    $data = array(
                        'dir_path' => $u_value["path"],
                        'img_name' => $u_value["fileName"],
                        'file_type' => $u_value["ext"],
                        'file_size' => $u_value["size"],
                        'thumb_name' => $u_value["fileName"],
                        'thumb_path' => $u_value["path"]
                    );
                    /*Safe Files*/
                    $id = $this->cms_media_model->save($data);
                }
            }

            // youtube video
            $video_url = $this->input->post('vid_url', true);
            if (!empty($video_url)) {
                $this->addVideo($video_url);
            }
            if (!empty($id)) {
                // save into activities
                $activities = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'media',
                    'module_field_id' => $id,
                    'activity' => "add_media",
                    'icon' => 'fa-ticket',
                    'value1' => 'Save Media image',
                );

                // Update into tbl_project
                $this->cms_media_model->_table_name = "tbl_activities"; //table name
                $this->cms_media_model->_primary_key = "activities_id";
                $this->cms_media_model->save($activities);
            }
            // messages for user
            $type = "success";
            $message = lang('add') . ' ' . lang('media');
            set_message($type, $message);

            redirect('admin/front/media');
        }
    }

    public function addVideo($video_url = null)
    {

        $youtube = "https://www.youtube.com/oembed?url=" . $video_url . "&format=json";
        $curl = curl_init($youtube);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $return = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($httpcode == 200) {
            $upload_response = $this->imageresize->resizeVideoImg($return);
            if ($upload_response) {
                $upload_response = json_decode($upload_response);
                $data = array(
                    'vid_url' => $video_url,
                    'vid_title' => $upload_response->vid_title,
                    'img_name' => $upload_response->store_name,
                    'file_type' => $upload_response->file_type,
                    'file_size' => $upload_response->file_size,
                    'thumb_name' => $upload_response->store_name,
                    'thumb_path' => $upload_response->thumb_path,
                    'dir_path' => $upload_response->dir_path,
                );

                $this->cms_media_model->_table_name = "front_cms_media_gallery"; //table name
                $this->cms_media_model->_primary_key = "id";
                $this->cms_media_model->save($data);
            }
        }
    }

    public function getPage()
    {
        $keyword = $this->input->get('keyword');
        $file_type = $this->input->get('file_type');
        $is_gallery = $this->input->get('is_gallery');
        if (!isset($is_gallery)) {
            $is_gallery = 1;
        }
        $this->load->model("cms_media_model");
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->cms_media_model->count_all($keyword, $file_type);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = '&gt;';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = "&lt;";
        $config["prev_tag_open"] = "<li>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = $this->uri->segment(5);
        $start = ($page - 1) * $config["per_page"];
        $result = $this->cms_media_model->fetch_details($config["per_page"], $start, $keyword, $file_type);
        $img_data = array();
        $check_empty = 0;
        if (!empty($result)) {
            $check_empty = 1;
            foreach ($result as $res_key => $res_value) {
                $div = $this->genrateDiv($res_value, $is_gallery);
                $img_data[] = $div;
            }
        }

        $output = array(
            'pagination_link' => $this->pagination->create_links(),
            'result_status' => $check_empty,
            'result' => $img_data,
        );
        echo json_encode($output);
        exit();
    }

    public function genrateDiv($result, $is_gallery)
    {
        $is_image = "0";
        $is_video = "0";
        if ($result->file_type == '.png' || $result->file_type == '.jpg' || $result->file_type == '.jpeg' || $result->file_type == '.JPEG' || $result->file_type == '.gif') {
            $file = base_url() . $result->dir_path . $result->img_name;
            $file_src = base_url() . $result->dir_path . $result->img_name;
            $is_image = 1;
        } elseif ($result->file_type == 'video') {
            $file = base_url() . $result->thumb_path . $result->img_name;
            $file_src = $result->vid_url;

            $is_video = 1;
        } elseif ($result->file_type == 'text/plain') {
            $file = base_url('backend/images/txticon.png');
            $file_src = base_url() . $result->dir_path . $result->img_name;
        } elseif ($result->file_type == 'application/zip' || $result->file_type == 'application/x-rar') {
            $file = base_url('backend/images/zipicon.png');
            $file_src = base_url() . $result->dir_path . $result->img_name;
        } elseif ($result->file_type == 'application/pdf') {
            $file = base_url('backend/images/pdficon.png');
            $file_src = base_url() . $result->dir_path . $result->img_name;
        } elseif ($result->file_type == 'application/msword') {
            $file = base_url('backend/images/wordicon.png');
            $file_src = base_url() . $result->dir_path . $result->img_name;
        } elseif ($result->file_type == 'application/vnd.ms-excel') {
            $file = base_url('backend/images/excelicon.png');
            $file_src = base_url() . $result->dir_path . $result->img_name;
        } else {
            $file = base_url('backend/images/docicon.png');
            $file_src = base_url() . $result->dir_path . $result->img_name;
        }
        //==============
        $output = '';
        $output .= "<div class='col-sm-3 col-md-2 col-xs-6 img_div_modal image_div div_record_" . $result->id . "'>";
        $output .= "<div class='fadeoverlay'>";
        $output .= "<div class='fadeheight'>";
        $output .= "<img class='' data-fid='" . $result->id . "' data-content_type='" . $result->file_type . "' data-content_name='" . $result->img_name . "' data-is_image='" . $is_image . "' data-vid_url='" . $result->vid_url . "' data-img='" . base_url() . $result->dir_path . $result->img_name . "' src='" . $file . "'>";
        $output .= "</div>";
        if ($is_video == 1) {
            $output .= "<i class='fa fa-youtube-play videoicon'></i>";
        }
        if ($is_image == 1) {
            $output .= "<i class='fa fa-picture-o videoicon'></i>";
        }
        if (!$is_gallery) {
            $output .= "<div class='overlay3'>";
            $output .= "<a href='#' class='uploadcheckbtn' data-record_id='" . $result->id . "' data-toggle='modal' data-target='#detail' data-image='" . $file . "' data-source='" . $file_src . "' data-media_name='" . $result->img_name . "' data-media_size='" . $result->file_size . "' data-media_type='" . $result->file_type . "'><i class='fa fa-navicon'></i></a>";
            $output .= "<a href='#' class='uploadclosebtn' data-record_id='" . $result->id . "' data-toggle='modal' data-target='#confirm-delete'><i class=' fa fa-trash-o'></i></a>";
            $output .= "<p class='processing'>Processing...</p>";
            $output .= "</div>";
        }
        if ($is_video == 1) {
            $output .= "<p class=''>" . $result->vid_title . "</p>";
        } else {
            $output .= "<p class=''>" . $result->img_name . "</p>";
        }
        $output .= "</div>";
        $output .= "</div>";
        return $output;
        //================
    }


    public function deleteItem()
    {
        $record_id = $this->input->post('record_id', true);

        $record = get_row('front_cms_media_gallery', array('id' => $record_id));

        $this->cms_media_model->_table_name = "front_cms_media_gallery"; //table name
        $this->cms_media_model->_primary_key = "id";

        if (!empty($record)) {
            remove_files($record->img_name, $record->dir_path);

            $this->cms_media_model->delete($record_id);
            // messages for user
            $type = "success";
            $message = lang('delete') . ' ' . lang('media');
        } else {
            $type = "error";
            $message = lang('no_record_found');
        }
        $data['status'] = $type;
        $data['msg'] = $message;
        echo json_encode($data);
        exit();
    }

}

