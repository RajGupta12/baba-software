<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $config = array(
            'field' => 'slug',
            'title' => 'title',
            'table' => 'front_cms_pages',
            'id' => 'pages_id',
        );
        if (empty(super_admin())) {
            redirect('404');
        }
        $this->load->library('slug', $config);
        $this->load->config('ci-blog');
        $this->load->library('imageResize');
        $this->load->model('cms_page_model');
    }

    function index($pages_id = null)
    {
        $data['title'] = lang('page');
        $data['category'] = config_item('pageCategory');
        if (!empty($pages_id)) {
            $data['active'] = 2;
            $data['page_info'] = get_row('front_cms_pages', array('pages_id' => $pages_id));
        } else {
            $data['active'] = 1;
        }
        $data['all_page'] = get_result('front_cms_pages');

        $data['subview'] = $this->load->view('admin/front/pages/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    /*Show page list*/
    public function pageList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'front_cms_pages';
            $this->datatables->join_table = array('front_cms_page_contents');
            $this->datatables->join_where = array('front_cms_page_contents.page_id=front_cms_pages.pages_id');
            $this->datatables->column_search = array('title', 'front_cms_page_contents.content_type');
            $this->datatables->order = array('front_cms_pages.pages_id' => 'asc');
            $fetch_data = make_datatables();

            $data = array();
            $edited = can_action('4', 'edited');
            $deleted = can_action('4', 'deleted');

            foreach ($fetch_data as $_key => $pages) {
                $action = null;
                $sub_array = array();

                $sub_array[] = $pages->title;
                $sub_array[] = '<a target="_blank" href="' . base_url() . $pages->url . '">' . base_url() . $pages->url . '<a>';

                if ($pages->content_type == "gallery") {
                    $sub_array[] = '<span class="label label-green">' . $pages->content_type . '</span>';
                } elseif ($pages->content_type == "events") {
                    $sub_array[] = '<span class="label label-info">' . $pages->content_type . '</span>';
                } elseif ($pages->content_type == "notice") {
                    $sub_array[] = '<span class="label label-warning">' . $pages->content_type . '</span>';
                } else {
                    $sub_array[] = '<span class="label label-default">' . lang("standard") . '</span>';
                }

                if (!empty($edited)) {
                    $action .= btn_edit('admin/front/page/index/' . $pages->pages_id) . ' ';
                }
                if (!empty($deleted) && $pages->page_type != "default") {
                    $action .= ajax_anchor(base_url("admin/front/page/delete_page/$pages->pages_id"), "<i class='btn btn-xs btn-danger fa fa-trash'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key));
                }

                $sub_array[] = $action;
                $data[] = $sub_array;
            }
            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    /*Create page*/
    public function create($id = null)
    {
        $this->cms_page_model->_table_name = 'front_cms_pages';
        $this->cms_page_model->_primary_key = 'pages_id';
        $data = $this->cms_page_model->array_from_post(array('title'));
        $data['description'] = $this->input->post('description');
        $data['type'] = 'page';

        $data['slug'] = $this->slug->create_uri($data, $id);
        $data['url'] = config_item('ci_front_page_url') . $data['slug'];

        $edited = can_action('152', 'edited');
        if (!empty($id)) {
            $action = 'activity_update_pages';
            $msg = lang('update') . ' ' . lang('page');
        } else {
            $action = 'activity_save_page';
            $msg = lang('save') . ' ' . lang('page');
        }
        if (!empty($edited)) {
            $id = $this->cms_page_model->save($data, $id);
            $category = $this->input->post('content_category');

            if (!empty($category)) {
                if ($category != "standard") {

                    $data = array();
                    $data["page_id"] = $id;
                    $data["content_type"] = $category;
                    $this->cms_page_model->_table_name = 'front_cms_page_contents';
                    $this->cms_page_model->_primary_key = 'id';
                    $page_type = get_row('front_cms_page_contents', array('page_id' => $id));
                    $page_contents_id=null;
                    if(!empty($page_type)){
                        $page_contents_id=$page_type->id;
                    }
                    $this->cms_page_model->save($data,$page_contents_id);
                }
            }

            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'pages',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => (!empty($data['title']) ? $data['title'] : ''),
            );
            $this->cms_page_model->_table_name = 'tbl_activities';
            $this->cms_page_model->_primary_key = 'activities_id';
            $this->cms_page_model->save($activity);
            // messages for user
            $type = "success";
            $message = $msg;
            set_message($type, $message);
        } else {
            set_message('error', lang('there_in_no_value'));
        }
        redirect('admin/front/page');
    }

    /*Delete Page*/
    public function delete_page($id = null)
    {
        if ($id) {
            $page_info = $this->cms_page_model->check_by(array('pages_id' => $id), 'front_cms_pages');
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'cms_page_model',
                'module_field_id' => $this->session->userdata('user_id'),
                'activity' => ('activity_deleted_page'),
                'icon' => 'fa-user',
                'value1' => $page_info->title
            );

            $this->cms_page_model->_table_name = 'tbl_activities';
            $this->cms_page_model->_primary_key = "activities_id";
            $this->cms_page_model->save($activities);

            // deletre into tbl_account details by user id
            $this->cms_page_model->_table_name = 'front_cms_pages';
            $this->cms_page_model->_primary_key = 'pages_id';
            $this->cms_page_model->delete($id);

            $this->cms_page_model->_table_name = 'front_cms_page_contents';
            $this->cms_page_model->_primary_key = 'page_id';
            $this->cms_page_model->delete($id);

            // messages for user
            $type = "success";
            $message = lang('delete') . " " . lang('page');
        } else {
            $type = "error";
            $message = lang('no_permission');
        }
        $type = "success";
        echo json_encode(array("status" => $type, "message" => $message));
        exit();

    }
    // menu model form
    public function add_image()
    {
        $data['title'] = lang('add') . ' ' . lang('menu'); //Page title

        $data['subview'] = $this->load->view('admin/front/pages/add_image', $data, FALSE);
        $this->load->view('admin/_layout_modal_large', $data); //page load
    }
}
