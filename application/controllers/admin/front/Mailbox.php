<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mailbox extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('cms_mailbox_model');
        if (empty(super_admin())) {
            redirect('404');
        }
    }

    function index()
    {
        $data['title'] = lang('mailbox');

        $data['subview'] = $this->load->view('admin/front/mailbox/index', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    // Show page list
    public function email_list()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_front_contact_us';
            $this->datatables->column_search = array('name', 'email', 'phone');
            $this->datatables->order = array('id' => 'desc');
            $fetch_data = make_datatables();

            $data = array();
            $deleted = can_action('155', 'deleted');

            foreach ($fetch_data as $_key => $info) {
                $action = null;
                $sub_array = array();


                $sub_array[] = '<a title="View Email" style="' . ($info->view_status == 1 ? 'color: #656565;' : ' ') . '"  data-target="#myModal_lg" data-toggle="modal" href="' . base_url('admin/front/mailbox/view_email/') . $info->id . '">' . $info->name . '</a>';
                $sub_array[] = '<a href="mailto:' . $info->email . '">' . $info->email . '</a>';
                $sub_array[] = $info->phone;


                $action .= btn_view_modal('admin/front/mailbox/view_email/' . $info->id) . ' ';
                if (!empty($deleted)) {
                    $action .= ajax_anchor(base_url("admin/front/mailbox/delete_email/$info->id"), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key));
                }

                $sub_array[] = $action;
                $data[] = $sub_array;
            }
            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    // view email
    public function view_email($id = null, $inline = null)
    {
        $data['title'] = lang('mailbox');

        if (!empty($id)) {
            $data['email_info'] = $this->cms_mailbox_model->check_by(array('id' => $id), 'tbl_front_contact_us');

            if ($data['email_info']->view_status != 1) {
                $this->cms_mailbox_model->_table_name = 'tbl_front_contact_us';
                $this->cms_mailbox_model->_primary_key = 'id';

                $this->cms_mailbox_model->save(array('view_status' => 1), $id);
            }
            $view = 'modal_lg';
            $sub = false;
            if (!empty($inline)) {
                $sub = true;
                $view = 'main';
            }
            $data['subview'] = $this->load->view('admin/front/mailbox/preview', $data, $sub);
            $this->load->view('admin/_layout_' . $view, $data);
        } else {
            redirect('admin/front/mailbox');
        }
    }


    // delete email
    public function delete_email($id = null)
    {
        if ($id) {
            $email_info = $this->cms_mailbox_model->check_by(array('id' => $id), 'tbl_front_contact_us');
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'mailbox',
                'module_field_id' => $this->session->userdata('user_id'),
                'activity' => ('activity_deleted_email'),
                'icon' => 'fa-user',
                'value1' => $email_info->name
            );

            $this->cms_mailbox_model->_table_name = 'tbl_activities';
            $this->cms_mailbox_model->_primary_key = "activities_id";
            $this->cms_mailbox_model->save($activities);

            // deletre into tbl_front_contact_us details by id
            $this->cms_mailbox_model->_table_name = 'tbl_front_contact_us';
            $this->cms_mailbox_model->_primary_key = 'id';
            $this->cms_mailbox_model->delete($id);

            // messages for user
            $type = "success";
            $message = lang('delete') . " " . lang('email');
        } else {
            $type = "error";
            $message = lang('no_permission');
        }
        $type = "success";
        echo json_encode(array("status" => $type, 'message' => $message));
        exit();
    }


}
