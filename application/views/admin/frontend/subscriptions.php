<div class="panel panel-custom">
    <div class="panel-heading">
        <?= lang('subscriptions') . ' ' . lang('list') ?>
    </div>
    <!-- ************** general *************-->
    <div class="table-responsive">
        <table class="table table-striped da" id="DataTables">
            <thead>
            <tr>
                <th><?= lang('plan') ?></th>
                <th><?= lang('trial_period') ?></th>
                <th><?= lang('currency') ?></th>
                <th><?= lang('frequency') ?></th>
                <th><?= lang('status') ?></th>
                <th><?= lang('date') ?></th>
                <th><?= lang('action') ?></th>
            </tr>
            </thead>
            <tbody>
            <script>
                $(document).ready(function () {
                    list = base_url + "admin/frontend/subscriptionsList";
                });
            </script>
            </tbody>
        </table>

    </div>
</div>
