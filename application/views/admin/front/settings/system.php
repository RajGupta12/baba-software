<?php echo message_box('success') ?>
<div class="row" xmlns="http://www.w3.org/1999/html">
    <!-- Start Form -->
    <div class="col-lg-12">
        <form role="form" id="form" action="<?php echo base_url(); ?>admin/front/settings/save_system" method="post"
              class="form-horizontal  ">
            <section class="panel panel-custom">
                <header class="panel-heading  "><?= lang('system_settings') ?></header>
                <div class="panel-body">
                    <?php echo validation_errors(); ?>
                    <input type="hidden" name="settings" value="<?= $load_setting ?>">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('default_language') ?></label>
                        <div class="col-lg-4">
                            <select name="default_language" class="form-control select_box">
                                <?php
                                if (!empty($languages)) {
                                    foreach ($languages as $lang) :
                                        ?>
                                        <option lang="<?= $lang->code ?>"
                                                value="<?= $lang->name ?>"<?= (config_item('default_language') == $lang->name ? ' selected="selected"' : '') ?>><?= ucfirst($lang->name) ?></option>
                                    <?php
                                    endforeach;
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('timezone') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-5">
                            <select name="timezone" class="form-control select_box" required>
                                <?php foreach ($timezones as $timezone => $description) : ?>
                                    <option
                                            value="<?= $timezone ?>"<?= (config_item('timezone') == $timezone ? ' selected="selected"' : '') ?>><?= $description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('default_currency') ?></label>
                        <div class="col-lg-4">
                            <select name="default_currency" class="form-control select_box">
                                <?php $cur = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row(); ?>

                                <?php foreach ($currencies as $cur) : ?>
                                    <option
                                            value="<?= $cur->code ?>" <?= (config_item('default_currency') == $cur->code ? ' selected="selected"' : '') ?>><?= $cur->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <?php if ($this->session->userdata('user_type') == '1') { ?>
                            <div class="col-sm-1">
                                <span data-toggle="tooltip" data-placement="top" title="<?= lang('new_currency'); ?>"
                                </span>
                                <a data-toggle="modal" data-target="#myModal"
                                   href="<?= base_url() ?>admin/front/settings/new_currency" class="btn btn-sm btn-success">
                                    <i class="fa fa-plus text-white"></i></a>
                            </div>
                            <div class="col-sm-1">
                                <span data-toggle="tooltip" data-placement="top"
                                      title="<?= lang('view_all_currency'); ?>"
                                </span>
                                <a href="<?= base_url() ?>admin/front/settings/all_currency" class="btn btn-sm btn-primary">
                                    <i class="fa fa-list-alt text-white"></i></a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('enable_languages') ?></label>
                        <div class="col-lg-6">
                            <div class="checkbox c-checkbox">
                                <label class="needsclick">
                                    <input type="checkbox" <?php
                                    if (config_item('enable_languages') == 'on') {
                                        echo "checked=\"checked\"";
                                    }
                                    ?> name="enable_languages">
                                    <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('country') ?></label>
                        <div class="col-lg-7">
                            <select class="form-control select_box" style="width:100%" name="company_country">
                                <option
                                        value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>

                                <?php
                                $countries = get_result('tbl_countries');
                                foreach ($countries as $country): ?>
                                    <option <?= (config_item('company_country') == $country->value ? ' selected="selected"' : '') ?>
                                            value="<?= $country->value ?>"><?= $country->value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"></label>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-sm btn-primary"><?= lang('save_changes') ?></button>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <!-- End Form -->
</div>