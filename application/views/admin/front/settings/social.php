<?php echo message_box('success') ?>
<?php echo message_box('error') ?>

<div class="row">
    <!-- Start Form -->
    <div class="col-lg-12">
        <form role="form" id="form" action="<?php echo base_url(); ?>admin/front/settings/save_social" method="post" enctype="multipart/form-data" class="form-horizontal  ">
            <section class="panel panel-custom">
                <header class="panel-heading"><?= lang('social_link') ?></header>
                <div class="panel-body pb-sm">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Facebook </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_facebook_link') != '') { echo config_item('front_facebook_link'); }?>" name="front_facebook_link" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Twitter </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_twitter_link') != '') { echo config_item('front_twitter_link'); }?>" name="front_twitter_link" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Google+ </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_google_link') != '') { echo config_item('front_google_link'); }?>" name="front_google_link" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Instagram </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_instagram_link') != '') { echo config_item('front_instagram_link'); }?>" name="front_instagram_link" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Linkedin </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_linkedin_link') != '') { echo config_item('front_linkedin_link'); }?>" name="front_linkedin_link" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Pinterest </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_pinterest_link') != '') { echo config_item('front_pinterest_link'); }?>" name="front_pinterest_link" class="form-control">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
        <!-- End Form -->
    </div>
</div>
