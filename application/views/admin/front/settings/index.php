<div class="row">
    <div class="col-lg-12">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked navbar-custom-nav">
                <?php
                $can_do = super_admin();
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'general') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings">
                            <i class="fa fa-fw fa-info-circle"></i>
                            <?= lang('general_settings') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'system') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/system">
                            <i class="fa fa-fw fa-desktop"></i>
                            <?= lang('system_settings') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'email_settings') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/email">
                            <i class="fa fa-fw fa-envelope"></i>
                            <?php echo lang('email_settings') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'theme') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/theme">
                            <i class="fa fa-fw fa-code"></i>
                            <?= lang('theme_settings') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'payments') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/payments">
                            <i class="fa fa-fw fa-dollar"></i>
                            <?= lang('payment_settings') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'slider') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/slider">
                            <i class="fa fa-fw fa-sliders"></i>
                            <?= lang('slider') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'social') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/social">
                            <i class="fa fa-fw fa-link"></i>
                            <?= lang('social_link') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'contact') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/contact">
                            <i class="fa fa-phone"></i>
                            <?= lang('contact_info') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'pricing') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/pricing">
                            <i class="fa fa-money" aria-hidden="true"></i>
                            <?= lang('pricing') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'footer') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/footer">
                            <i class="fa fa-fw fa-cog"></i>
                            <?= lang('footer') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'translations') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/translations">
                            <i class="fa fa-fw fa-cog"></i>
                            <?= lang('translations') ?>
                        </a>
                    </li>
                <?php }
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($load_setting == 'system_update') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/front/settings/system_update">
                            <i class="fa fa-fw fa-pencil-square-o"></i>
                            <?php echo lang('system_update') ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <section class="col-sm-9">
            <section class="">
                <!-- Load the settings form in views -->
                <?php $this->load->view('admin/front/settings/' . $load_setting) ?>
                <!-- End of settings Form -->
            </section>
        </section>
    </div>
</div>
