<?php echo message_box('success') ?>
<?php echo message_box('error') ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>plugins/summernote/summernote.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>plugins/summernote/summernote.min.js">
<div class="row">
    <!-- Start Form -->
    <div class="col-lg-12">
        <form role="form" id="form" action="<?php echo base_url(); ?>admin/front/settings/save_footer" method="post" enctype="multipart/form-data" class="form-horizontal  ">
            <section class="panel panel-custom">
                <header class="panel-heading"><?= lang('footer') ?></header>
                <div class="panel-body pb0">
                    <h4 class="panel-title">Col 1</h4>
                    <hr style="margin-top: 10px;">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Title </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_footer_col_1_title') != '') { echo config_item('front_footer_col_1_title'); }?>" name="front_footer_col_1_title" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-12">Description </label>
                        <div class="col-lg-12">
                            <textarea name="front_footer_col_1_description" class="form-control summernote" rows="4"><?php if (config_item('front_footer_col_1_description') != '') { echo config_item('front_footer_col_1_description'); }?></textarea>
                        </div>
                    </div>

                    <h4 class="panel-title">Col 2</h4>
                    <hr style="margin-top: 10px;">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Title </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_footer_col_2_title') != '') { echo config_item('front_footer_col_2_title'); }?>" name="front_footer_col_2_title" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-12">Description </label>
                        <div class="col-lg-12">
                            <textarea style="" name="front_footer_col_2_description" class="form-control textarea_">
                               <?php if (config_item('front_footer_col_2_description') != '') { echo config_item('front_footer_col_2_description'); }?>
                            </textarea>
                        </div>
                    </div>

                    <h4 class="panel-title">Col 3</h4>
                    <hr style="margin-top: 10px;">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Title </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_footer_col_3_title') != '') { echo config_item('front_footer_col_3_title'); }?>" name="front_footer_col_3_title" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-12">Description </label>
                        <div class="col-lg-12">
                            <textarea style="" name="front_footer_col_3_description" class="form-control textarea_">
                               <?php if (config_item('front_footer_col_3_description') != '') { echo config_item('front_footer_col_3_description'); }?>
                            </textarea>
                        </div>
                    </div>


                    <h4 class="panel-title">Col 4</h4>
                    <hr style="margin-top: 10px;">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Title </label>
                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_footer_col_4_title') != '') { echo config_item('front_footer_col_4_title'); }?>" name="front_footer_col_4_title" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-12">Description </label>
                        <div class="col-lg-12">
                            <textarea style="" name="front_footer_col_4_description" class="form-control textarea_">
                               <?php if (config_item('front_footer_col_4_description') != '') { echo config_item('front_footer_col_4_description'); }?>
                            </textarea>
                        </div>
                    </div>



                    <div class="form-group" style="margin-top: 20px;">
                        <div class="col-lg-12 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
        <!-- End Form -->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 150,
            tabsize: 2
        });
    });
</script>
