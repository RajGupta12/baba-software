<?php echo message_box('success') ?>
<?php echo message_box('error') ?>
<style>
    .material-switch > input[type="checkbox"] {
        display: none;
    }

    .material-switch > label {
        cursor: pointer;
        height: 0px;
        position: relative;
        width: 40px;
    }

    .material-switch > label::before {
        background: rgb(0, 0, 0);
        box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
        border-radius: 8px;
        content: '';
        height: 16px;
        margin-top: -8px;
        position: absolute;
        opacity: 0.3;
        transition: all 0.4s ease-in-out;
        width: 40px;
    }

    .material-switch > label::after {
        background: rgb(255, 255, 255);
        border-radius: 16px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
        content: '';
        height: 24px;
        left: -4px;
        margin-top: -8px;
        position: absolute;
        top: -4px;
        transition: all 0.3s ease-in-out;
        width: 24px;
    }

    .material-switch > input[type="checkbox"]:checked + label::before {
        background: inherit;
        opacity: 0.5;
    }

    .material-switch > input[type="checkbox"]:checked + label::after {
        background: inherit;
        left: 20px;
    }
</style>

<div class="row">
    <!-- Start Form -->
    <div class="col-lg-12">
        <form role="form" id="form" action="<?php echo base_url(); ?>admin/front/settings/save_subscribe" method="post" enctype="multipart/form-data" class="form-horizontal  ">
            <section class="panel panel-custom">
                <header class="panel-heading"><?= lang('subscribe') ?></header>
                <div class="panel-body pb-sm">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?=lang('title')?> </label>

                        <div class="col-lg-6">
                            <input type="text" value="<?php if (config_item('front_subscribe_title') != '') { echo config_item('front_subscribe_title'); }?>" name="front_subscribe_title" class="form-control">
                        </div>
                    </div> 
                    
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?=lang('description')?> </label>

                        <div class="col-lg-6">
                            <textarea name="front_subscribe_description" class="form-control" rows="4">
                                <?php if (config_item('front_subscribe_description') != '') { echo config_item('front_subscribe_description'); }?>
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Subscribe </label>

                        <div class="col-lg-6">
                            <div class="material-switch" style="margin-top: 7px;">
                                <input name="front_subscribe" id="ext_url" type="checkbox"
                                       value="1" <?php
                                                if (config_item('front_subscribe') != '') {
                                                    echo "checked";
                                                }?> />
                                <label for="ext_url" class="label-success"></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
        <!-- End Form -->
    </div>
</div>
