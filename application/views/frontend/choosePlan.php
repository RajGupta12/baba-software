<!-- contect-page-section -->
<style>
    .primary-btn {
        border-color: #2D017C !important;
        background: #fff !important;
        border-radius: 24px !important;;
        color: initial !important;
        padding: 11px 20px !important;
    }

    .set_price.active:before {
        position: absolute;
        background-image: linear-gradient(to right, #3264f5 0%, #7202bb 51%, #3264f5 100%);
        content: "";
        height: 100%;
        width: 100%;
        border: 1px solid transparent !important;
        left: 0;
        top: 0;
        border-radius: 24px;
        z-index: -1;
        opacity: 1;
        transition: .3s;
    }

    .new_error {
        color: red;
    }
</style>
<script>
    $('html,body').animate({
//            scrollTop: 750
        },
        'slow');
</script>
<?php echo form_open(base_url('signed_up'), array('class' => '', 'id' => 'contact-form', 'enctype' => 'multipart/form-data', 'data-parsley-validate' => '', 'role' => 'form')); ?>
<div class="modal-body">
    <div class="row mb-3">
        <div class="col-sm-3">
            <div class="interval_type">
                <span class="set_price btn primary-btn pricing-btn secondary-btn <?= (!empty($interval_type) && $interval_type == 'monthly' || empty($interval_type) ? 'active' : '') ?> by_type"
                      result="by_type"
                      type="monthly"><?= lang('monthly') ?></span>
                <span
                        class="set_price btn primary-btn pricing-btn secondary-btn <?= (!empty($interval_type) && $interval_type == 'annually' ? 'active' : '') ?> by_type"
                        type="annually"
                        result="by_type"><?= lang('annually') ?></span>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="currency_type text-right">
                <?php
                $all_currency = $this->db->select('currency')->distinct()->get('tbl_currencywise_price')->result();
                if (!empty($all_currency)) {
                    foreach ($all_currency as $v_currency) {
                        $c_name = get_row('tbl_currencies', array('code' => $v_currency->currency));
                        ?>
                        <span currency="<?= $v_currency->currency ?>" result="by_currency"
                              class="set_price currencywise_price btn primary-btn pricing-btn secondary-btn <?= (!empty($currency_type) && $currency_type == $v_currency->currency || (empty($currency_type) && $v_currency->currency == config_item('default_currency')) ? 'custom-bg active' : 'btn-primary') ?>"><?= $c_name->name . '(' . $c_name->symbol . ')' ?></span>
                    <?php }
                }
                ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="column col-md-7 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-header bg-info">
                    <strong> <?= lang('sign_up') . ' ' . lang('for') . ' <span id="package_name">' . $plan_info->name ?></span></strong>
                </div>
                <div class="card-body">
                    <input id="new_interval_type" name="interval_type"
                           value="<?= (!empty($interval_type) ? $interval_type : 'monthly') ?>" type="hidden">
                    <input id="new_currency_type" name="currency_type"
                           value="<?= (!empty($currency_type) ? $currency_type : config_item('default_currency')) ?>"
                           type="hidden">

                    <?php $this->load->view('frontend/pricing_dropdown', array('c_pricing_info' => $c_pricing_info)) ?>


                    <div class="form-group">
                        <input name="name" required value="" class="form-control" placeholder="<?= lang('name') ?> *"
                               type="text">
                    </div>


                    <div class="form-group">
                        <input name="email" id="check_email" class="form-control" required
                               placeholder="<?= lang('email') ?> *"
                               type="email">
                        <small class="new_error" id="email_error"></small>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <input name="mobile" value="" class="form-control" required
                                       placeholder="<?= lang('mobile') ?> *"
                                       type="text">
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <select name="country" class="form-control select_box">
                                    <option
                                            value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                    <?php
                                    foreach ($countries as $country): ?>
                                        <option <?= (config_item('company_country') == $country->value ? ' selected="selected"' : '') ?>
                                                value="<?= $country->value ?>"><?= $country->value ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex row mb-0 mt-4" style="margin-bottom: 0!important;">
                        <div class="col-md-6 justify-content-start">
                            <button type="submit" id="new_company"
                                    class="btn primary-btn pricing-btn secondary-btn"><?= lang('sign_up') ?></button>
                        </div>
                        <div class="col-md-6 justify-content-end">
                            <p class="pt-lg text-right"><?= lang('already_have_an_account') ?>
                                <a href="<?= base_url() ?>login"
                                   class="btn primary-btn pricing-btn secondary-btn"><?= lang('sign_in') ?></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div id="package_details" class="col-md-5 col-sm-6 col-xs-12">
            <?php $this->load->view('frontend/package_details', array('plan_info' => $plan_info)) ?>
        </div>
    </div>
</div>


<!-- contect-page-section end -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".set_price").click(function () {
            var result = $(this).attr('result');
            if (result == 'by_currency') {
                $('.currencywise_price').removeClass('active');
            }
            if (result == 'by_type') {
                $('.by_type').removeClass('active');
            }
            $(this).addClass('active');

            var interval_type = $('.interval_type >  .active').attr('type');
            var currencywise_price = $('.currency_type >  .active').attr('currency');

            $('#new_interval_type').val(interval_type);
            $('#new_currency_type').val(currencywise_price);

            var formData = {
                'interval_type': interval_type,
                'currencywise_price': currencywise_price,
            };
            $.ajax({
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: '<?= base_url()?>frontend/get_currencywise_price/', // the url where we want to POST
                data: formData, // our data object
                dataType: 'json', // what type of data do we expect back from the server
                encode: true,
                success: function (res) {
                    if (res) {
                        $('select[name="pricing_id"]').find('option').remove();
                        $.ajax({
                            url: "<?= base_url()?>admin/global_controller/get_package_details/" + res[0].frontend_pricing_id,
                            type: "GET",
                            dataType: 'json',
                            success: function (result) {
                                document.getElementById('package_name').innerHTML = result.package_name;
                                document.getElementById('package_details').innerHTML = result.package_details;
                            }
                        });
                        $.each(res, function (index, item) {
                            if (interval_type == 'annually') {
                                var amount = item.yearly + ' /' + '<?= lang('yr')?>';
                            } else {
                                var amount = item.monthly + ' /' + '<?= lang('mo')?>';
                            }
                            $('select[name="pricing_id"]').append('<option value="' + item.frontend_pricing_id + '">' + item.name + ' ' + item.currency + amount + '</option>');
                        });
                    } else {
                        alert('There was a problem with AJAX');
                    }
                }
            })

        });
    });
    $(document).on("change", function () {
        var check_email = $('#check_email').val();
        var base_url = "<?= base_url()?>";
        var url = null;
        if (check_email) {
            id = 'email_error';
            btn = 'new_company';
            url = 'check_existing_subscription_email';
            value = check_email;
        }
        if (url) {
            $.ajax({
                url: base_url + "admin/global_controller/" + url,
                type: "POST",
                data: {
                    name: value,
                },
                dataType: 'json',
                success: function (res) {
                    if (res.error) {
                        $("#" + id).html(res.error);
                        $("#" + btn).attr("disabled", "disabled");
                        return;
                    } else {
                        $("#" + id).empty();
                        $("#" + btn).removeAttr("disabled");
                        return;
                    }
                }
            });
        }
    });
</script>