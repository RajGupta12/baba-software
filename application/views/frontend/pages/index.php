<?php
if ($active_menu == 'home' && config_item('front_slider') == 1) {
    $this->load->view('frontend/pages/components/slider');
} else {
    $this->load->view('frontend/pages/components/page_header');
}

if (!empty($page_info)) {
    if ($active_menu == 'contact-us') {
        //echo $page_info->description;
        $this->load->view('frontend/pages/contact');
    } elseif ($active_menu == 'home' && config_item('front_subscribe') == 1) {
        echo $page_info->description;
        //$this->load->view('frontend/pages/components/subscribe');
    } elseif ($active_menu == 'pricing') {
        //echo $page_info->description;
        $this->load->view('frontend/pages/pricing');
    } else {
        echo $page_info->description;
    }
}

?>
