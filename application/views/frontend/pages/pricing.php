<div class="cps-section" id="subscription-area" style="padding: 50px 0; background-color: #f0eded;">
    <div class="container">
        <div class="section-header text-center">
            <h2>
                <?php if (config_item('front_contact_title') != '') {
                    echo config_item('front_pricing_title');
                } ?>
            </h2>
            <p>
                <?php if (config_item('front_pricing_description') != '') {
                    echo config_item('front_pricing_description');
                } ?>
            </p>
        </div>
        <div class="error_login">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if (!empty($error)) {
                ?>
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <?php echo $error; ?>
                </div>
            <?php } ?>
            <?php if (!empty($success)) { ?>
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <?php echo $success; ?>
                </div>
            <?php } ?>
        </div>
        <div class="full-body">
            <script src="<?= base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
            <style type="text/css">
                #generic_price_table {
                    /*background-color: #f0eded;*/
                }

                .full-body {
                    overflow: hidden
                }

                /*PRICE COLOR CODE START*/
                #generic_price_table .generic_content {
                    background-color: #fff;
                }

                #generic_price_table .generic_content .generic_head_price {
                    background-color: #f6f6f6;
                }

                #generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg {
                    border-color: #e4e4e4 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #e4e4e4;
                }

                #generic_price_table .generic_content .generic_head_price .generic_head_content .head span {
                    color: #525252;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .sign {
                    color: #414141;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .currency {
                    color: #414141;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .cent {
                    color: #414141;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .month {
                    color: #414141;
                }

                #generic_price_table .generic_content .generic_feature_list ul li {
                    color: #a7a7a7;
                }

                #generic_price_table .generic_content .generic_feature_list ul li span {
                    color: #414141;
                }

                #generic_price_table .generic_content .generic_feature_list ul li:hover {
                    background-color: #E4E4E4;
                    border-left: 5px solid #2e39bf;
                }

                #generic_price_table .generic_content .generic_price_btn button {
                    border: 1px solid #2e39bf;
                    color: #2e39bf;
                    background: #ffffff;

                }

                #generic_price_table .generic_content.active .generic_head_price .generic_head_content .head_bg,
                #generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head_bg {
                    border-color: #2e39bf rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #2e39bf;
                    color: #fff;
                }

                #generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head span,
                #generic_price_table .generic_content.active .generic_head_price .generic_head_content .head span {
                    color: #fff;
                }

                #generic_price_table .generic_content:hover .generic_price_btn button,
                #generic_price_table .generic_content.active .generic_price_btn button {
                    background-color: #2e39bf;
                    color: #fff;
                }

                #generic_price_table {
                    /*margin: 50px 0 0 0;*/
                    font-family: 'Raleway', sans-serif;
                }

                .row .table {
                    padding: 28px 0;
                }

                /*PRICE BODY CODE START*/

                #generic_price_table .generic_content {
                    overflow: hidden;
                    position: relative;
                    text-align: center;
                }

                #generic_price_table .generic_content .generic_head_price {
                    margin: 0 0 20px 0;
                }

                #generic_price_table .generic_content .generic_head_price .generic_head_content {
                    margin: 0 0 50px 0;
                }

                #generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg {
                    border-style: solid;
                    border-width: 90px 1411px 23px 399px;
                    position: absolute;
                }

                #generic_price_table .generic_content .generic_head_price .generic_head_content .head {
                    padding-top: 40px;
                    position: relative;
                    z-index: 1;
                }

                #generic_price_table .generic_content .generic_head_price .generic_head_content .head span {
                    font-family: "Raleway", sans-serif;
                    font-size: 28px;
                    font-weight: 400;
                    letter-spacing: 2px;
                    margin: 0;
                    padding: 0;
                    text-transform: uppercase;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag {
                    padding: 0 0 20px;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .price {
                    display: block;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .sign {
                    display: inline-block;
                    font-family: "Lato", sans-serif;
                    font-size: 28px;
                    font-weight: 400;
                    vertical-align: middle;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .currency {
                    font-family: "Lato", sans-serif;
                    font-size: 60px;
                    font-weight: 300;
                    letter-spacing: -2px;
                    line-height: 60px;
                    padding: 0;
                    vertical-align: middle;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .cent {
                    display: inline-block;
                    font-family: "Lato", sans-serif;
                    font-size: 24px;
                    font-weight: 400;
                    vertical-align: bottom;
                }

                #generic_price_table .generic_content .generic_head_price .generic_price_tag .month {
                    font-family: "Lato", sans-serif;
                    font-size: 18px;
                    font-weight: 400;
                    letter-spacing: 3px;
                    vertical-align: bottom;
                }

                #generic_price_table .generic_content .generic_feature_list ul {
                    list-style: none;
                    padding: 0;
                    margin: 0;
                }

                #generic_price_table .generic_content .generic_feature_list ul li {
                    font-family: "Lato", sans-serif;
                    font-size: 18px;
                    padding: 5px 0;
                    transition: all 0.3s ease-in-out 0s;
                    /*float: left;*/
                }

                #generic_price_table .generic_content .generic_feature_list ul li:hover {
                    transition: all 0.3s ease-in-out 0s;
                    -moz-transition: all 0.3s ease-in-out 0s;
                    -ms-transition: all 0.3s ease-in-out 0s;
                    -o-transition: all 0.3s ease-in-out 0s;
                    -webkit-transition: all 0.3s ease-in-out 0s;

                }

                #generic_price_table .generic_content .generic_feature_list ul li .fa {
                    padding: 0 10px;
                }

                #generic_price_table .generic_content .generic_price_btn {
                    margin: 20px 0 32px;
                }

                #generic_price_table .generic_content .generic_price_btn button {
                    border-radius: 50px;
                    -moz-border-radius: 50px;
                    -ms-border-radius: 50px;
                    -o-border-radius: 50px;
                    -webkit-border-radius: 50px;
                    display: inline-block;
                    font-family: "Lato", sans-serif;
                    font-size: 18px;
                    outline: medium none;
                    padding: 12px 30px;
                    text-decoration: none;
                    text-transform: uppercase;
                }

                #generic_price_table .generic_content,
                #generic_price_table .generic_content:hover,
                #generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg,
                #generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head_bg,
                #generic_price_table .generic_content .generic_head_price .generic_head_content .head h2,
                #generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head h2,
                #generic_price_table .generic_content .price,
                #generic_price_table .generic_content:hover .price,
                #generic_price_table .generic_content .generic_price_btn button,
                #generic_price_table .generic_content:hover .generic_price_btn button {
                    transition: all 0.3s ease-in-out 0s;
                    -moz-transition: all 0.3s ease-in-out 0s;
                    -ms-transition: all 0.3s ease-in-out 0s;
                    -o-transition: all 0.3s ease-in-out 0s;
                    -webkit-transition: all 0.3s ease-in-out 0s;
                }

                @media (max-width: 320px) {
                }

                @media (max-width: 767px) {
                    #generic_price_table .generic_content {
                        margin-bottom: 75px;
                    }
                }

                @media (min-width: 768px) and (max-width: 991px) {
                    #generic_price_table .col-md-3 {
                        float: left;
                        width: 50%;
                    }

                    #generic_price_table .col-md-4 {
                        float: left;
                        width: 50%;
                    }

                    #generic_price_table .generic_content {
                        margin-bottom: 75px;
                    }
                }

                @media (min-width: 992px) and (max-width: 1199px) {
                }

                @media (min-width: 1200px) {
                }

                #generic_price_table_home {
                    font-family: 'Raleway', sans-serif;
                }

                .text-center h1,
                .text-center h1 a {
                    color: #7885CB;
                    font-size: 30px;
                    font-weight: 300;
                    text-decoration: none;
                }

                .demo-pic {
                    margin: 0 auto;
                }

                .demo-pic:hover {
                    opacity: 0.7;
                }

                #generic_price_table_home ul {
                    margin: 0 auto;
                    padding: 0;
                    list-style: none;
                    display: table;
                }

                #generic_price_table_home li {
                    float: left;
                }

                #generic_price_table_home li + li {
                    margin-left: 10px;
                    padding-bottom: 10px;
                }

                #generic_price_table_home li a {
                    display: block;
                    width: 50px;
                    height: 50px;
                    font-size: 0px;
                }

                #generic_price_table_home .blue {
                    background: #3498DB;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .emerald {
                    background: #2e39bf;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .grey {
                    background: #7F8C8D;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .midnight {
                    background: #34495E;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .orange {
                    background: #E67E22;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .purple {
                    background: #9B59B6;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .red {
                    background: #E74C3C;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .turquoise {
                    background: #1ABC9C;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .blue:hover,
                #generic_price_table_home .emerald:hover,
                #generic_price_table_home .grey:hover,
                #generic_price_table_home .midnight:hover,
                #generic_price_table_home .orange:hover,
                #generic_price_table_home .purple:hover,
                #generic_price_table_home .red:hover,
                #generic_price_table_home .turquoise:hover {
                    border-bottom-left-radius: 50px;
                    border-bottom-right-radius: 50px;
                    border-top-left-radius: 50px;
                    border-top-right-radius: 50px;
                    transition: all 0.3s ease-in-out 0s;
                }

                #generic_price_table_home .divider {
                    border-bottom: 1px solid #ddd;
                    margin-bottom: 20px;
                    padding: 20px;
                }

                #generic_price_table_home .divider span {
                    width: 100%;
                    display: table;
                    height: 2px;
                    background: #ddd;
                    margin: 50px auto;
                    line-height: 2px;
                }

                #generic_price_table_home .itemname {
                    text-align: center;
                    font-size: 50px;
                    padding: 50px 0 20px;
                    border-bottom: 1px solid #ddd;
                    margin-bottom: 40px;
                    text-decoration: none;
                    font-weight: 300;
                }

                #generic_price_table_home .itemnametext {
                    text-align: center;
                    font-size: 20px;
                    padding-top: 5px;
                    text-transform: uppercase;
                    display: inline-block;
                }

                #generic_price_table_home .footer {
                    padding: 40px 0;
                }

                .price-heading {
                    text-align: center;
                }

                .price-heading h1 {
                    color: #666;
                    margin: 0;
                    padding: 0 0 50px 0;
                }

                .demo-button {
                    background-color: #333333;
                    color: #ffffff;
                    display: table;
                    font-size: 20px;
                    margin-left: auto;
                    margin-right: auto;
                    margin-top: 20px;
                    margin-bottom: 50px;
                    outline-color: -moz-use-text-color;
                    outline-style: none;
                    outline-width: medium;
                    padding: 10px;
                    text-align: center;
                    text-transform: uppercase;
                }

                .bottom_btn {
                    background-color: #333333;
                    color: #ffffff;
                    display: table;
                    font-size: 28px;
                    margin: 60px auto 20px;
                    padding: 10px 25px;
                    text-align: center;
                    text-transform: uppercase;
                }

                .demo-button:hover {
                    background-color: #666;
                    color: #FFF;
                    text-decoration: none;

                }

                .bottom_btn:hover {
                    background-color: #666;
                    color: #FFF;
                    text-decoration: none;
                }


                .btn-primary {
                    border-color: #2D017C !important;
                    background: #fff !important;
                    border-radius: 0 !important;
                    color: initial !important;
                }

                .ribbon-wrapper-red {
                    width: 85px;
                    height: 88px;
                    overflow: hidden;
                    position: absolute;
                    top: 0px;
                    right: 15px;
                    z-index: 1;
                }

                .ribbon-red {
                    font: 700 15px Sans-Serif;
                    text-align: center;
                    text-shadow: hsla(0, 0%, 100%, .5) 0 1px 0;
                    -webkit-transform: rotate(45deg);
                    -moz-transform: rotate(45deg);
                    -ms-transform: rotate(45deg);
                    -o-transform: rotate(45deg);
                    position: relative;
                    padding: 7px 0;
                    left: -5px;
                    top: 15px;
                    width: 120px;
                    background-color: #ea181e;
                    background-image: -o-linear-gradient(top, #bfdc7a, #8ebf45);
                    color: #fff;
                    -webkit-box-shadow: 0 0 3px rgba(0, 0, 0, .3);
                    box-shadow: 0 0 3px rgba(0, 0, 0, .3);
                }

                .custom-bg {
                    background: #3378ff;
                    color: #fff;
                }

                .pricing-item {
                    /*width: 33%;*/
                    margin-bottom: 25px;
                }

            </style>

            <?= form_open(base_url('sign-up'), array('class' => 'form-horizontal', 'id' => 'contact-form', 'enctype' => 'multipart/form-data', 'data-parsley-validate' => '', 'role' => 'form')); ?>

            <input id="new_interval_type" name="interval_type" value="monthly" type="hidden">
            <input id="new_currency_type" name="currency_type" value="<?= config_item('default_currency') ?>"
                   type="hidden">

            <style>
                .hidden {
                    display: none;
                }

                .pricing-tabs span {
                    color: #000;
                    font-size: 18px;
                    padding: .5rem 1rem;
                    display: inline-block;
                    /*border-bottom: 2px solid transparent;*/
                    cursor: pointer;
                }

                .pricing-tabs-currency span {
                    font-size: 12px;
                }

                .pricing-tabs span.active {
                    border-bottom: 2px solid #2D017C;
                    /*background: #fff !important;*/
                }
            </style>
            <div class="row">
                <div class="col-xl-12">
                    <div class="nav pricing-tabs text-center d-block  mb-20 interval_type">
                        <span class="set_price btn-primary  active by_type" result="by_type"
                              type="monthly"><?= lang('monthly') ?></span>
                        <span class="set_price btn-primary  by_type" type="annually"
                              result="by_type"><?= lang('annually') ?></span>
                    </div>
                    <div class="nav pricing-tabs pricing-tabs-currency  mt-15 mb-15 currency_type d-flex align-items-start justify-content-lg-end justify-content-start">
                        <?php
                        $all_currency = $this->db->select('currency')->distinct()->get('tbl_currencywise_price')->result();
                        if (!empty($all_currency)) {
                            foreach ($all_currency as $v_currency) {
                                $c_name = get_row('tbl_currencies', array('code' => $v_currency->currency));
                                ?>
                                <span currency="<?= $v_currency->currency ?>" result="by_currency"
                                      class="set_price currencywise_price <?= (config_item('default_currency') == $v_currency->currency ? 'btn-primary active' : 'btn-primary') ?>"><?= $c_name->name . '(' . $c_name->symbol . ')' ?></span>
                            <?php }
                        } ?>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('.pricing-item').addClass('hidden');
                    var interval_type = $('.interval_type >  .active').attr('type');

                    var new_result = [];
                    <?php if (!empty($currency_wise_price)) {
                    foreach ($currency_wise_price as $v_c_price) {?>
                    var result = {
                        frontend_pricing_id: "<?= $v_c_price->frontend_pricing_id ?>",
                        currency: "<?= $v_c_price->currency?>",
                        yearly: "<?= $v_c_price->yearly?>",
                        monthly: "<?= $v_c_price->monthly ?>"
                    };
                    new_result.push(result)
                    <?php }
                    }?>

                    $.each(new_result, function (index, item) {
                        $('.pricing-currency').html(item.currency);
                        if (interval_type == 'annually') {
                            $('.table_list_' + item.frontend_pricing_id).removeClass('hidden');
                            $('.type_amount_' + item.frontend_pricing_id).html(item.yearly);
                            $('.pricing-period').html('/<?= lang('yr')?>');
                        } else {
                            $('.table_list_' + item.frontend_pricing_id).removeClass('hidden');
                            $('.type_amount_' + item.frontend_pricing_id).html(item.monthly);
                            $('.pricing-period').html('/<?= lang('mo')?>');
                        }

                    });

                    $(".set_price").click(function () {

                        var result = $(this).attr('result');

                        if (result == 'by_currency') {
                            $('.currencywise_price').removeClass('active');
                        }
                        if (result == 'by_type') {
                            $('.by_type').removeClass('active');
                        }
                        $(this).addClass('active');

                        var interval_type = $('.interval_type >  .active').attr('type');
                        var currencywise_price = $('.currency_type >  .active').attr('currency');

                        $('#new_interval_type').val(interval_type);
                        $('#new_currency_type').val(currencywise_price);

                        var formData = {
                            'interval_type': interval_type,
                            'currencywise_price': currencywise_price,
                        };
                        console.log(formData);
                        $.ajax({
                            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                            url: '<?= base_url()?>frontend/get_currencywise_price/', // the url where we want to POST
                            data: formData, // our data object
                            dataType: 'json', // what type of data do we expect back from the server
                            encode: true,
                            success: function (res) {
                                if (res) {
                                    $('.pricing-item').addClass('hidden');
                                    $.each(res, function (index, item) {
                                        $('.pricing-currency').html(item.currency);
                                        if (interval_type == 'annually') {
                                            $('.table_list_' + item.frontend_pricing_id).removeClass('hidden');
                                            $('.type_amount_' + item.frontend_pricing_id).html(item.yearly);
                                            $('.pricing-period').html('/<?= lang('yr')?>');
                                        } else {
                                            $('.table_list_' + item.frontend_pricing_id).removeClass('hidden');
                                            $('.type_amount_' + item.frontend_pricing_id).html(item.monthly);
                                            $('.pricing-period').html('/<?= lang('mo')?>');
                                        }

                                    });
                                } else {
                                    alert('There was a problem with AJAX');
                                }
                            }
                        })

                    });

                });

                function choosePlan(p_id) {

                    formData = {
                        'pricing_id': p_id,
                        'interval_type': $('#new_interval_type').val(),
                        'currency_type': $('#new_currency_type').val()
                    };

                    $.ajax({
                        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url: '<?= base_url()?>frontend/choosePlan_/', // the url where we want to POST
                        data: formData, // our data object
                        success: function (res) {
                            $('#modal-xl-con').html(res);
                            $('#modal-xl').modal();

                        }
                    })
                }

            </script>


            <div id="generic_price_table" class="col-md-12">
                <div class="row">
                    <?php
                    $all_pricing = get_order_by('tbl_frontend_pricing', array('status' => 1), 'sort', true);
                    $currency = $this->invoice_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
                    if (!empty($all_pricing)) {
                        foreach ($all_pricing as $key => $pricing) {
                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-12 pricing-item table_list_<?= $pricing->id ?>">
                                <!--PRICE CONTENT START-->
                                <div class="price-item mb-30 <?= ($pricing->recommended == 'Yes' ? 'active' : '') ?>">
                                    <h3 class="pricing-title text-center">
                                        <?= $pricing->name ?>
                                    </h3>

                                    <div class="pricing-values text-center">
                                        <div class="pricing__value">

                                            <?php

                                            $amount = explode(".", $pricing->amount);

                                            $amount1 = $amount[0];

                                            if (!empty($amount[1])) {
                                                $amount2 = $amount[1];
                                            } else {
                                                $amount2 = '00';
                                            }

                                            ?>

                                            <span class="pricing-currency">$</span>
                                            <span class="currency type_amount_<?= $pricing->id ?>"><?= $pricing->amount ?></span>
                                            <span class="pricing-period">/Month</span>
                                        </div>
                                    </div>

                                    <div class="price-text">
                                        <div class="price-header text-center">
                                            <?php if ($pricing->recommended == 'Yes') { ?>
                                                <div class="ribbon-wrapper-red">
                                                    <div class="ribbon-red"><?= lang('featured') ?></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="">
                                            <ul class="pricing-list mb-30">
                                                <li class='pricing-feature'><?= pricing_format($pricing->employee_no, lang('employee')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->trial_period, lang('days') . ' ' . lang('trail_period')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->supplier_no, lang('supplier')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->purchase_no, lang('purchase')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->client_no, lang('client')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->project_no, lang('project')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->invoice_no, lang('invoice')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->leads, lang('leads')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->accounting, lang('accounting')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->bank_account, lang('bank') . ' ' . lang('account')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format($pricing->tasks, lang('tasks')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->online_payment, lang('online_payment')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->mailbox, lang('mailbox')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->live_chat, lang('live_chat')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->tickets, lang('tickets')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->recruitment, lang('job_circular')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->attendance, lang('attendance')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->payroll, lang('payroll')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->leave_management, lang('leave_management')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->performance, lang('performance')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->training, lang('training')) ?></li>
                                                <li class='pricing-feature'><?= pricing_format_YN($pricing->reports, lang('report')) ?></li>
                                            </ul>
                                        </div>
                                        <div class="pricing-btn text-center">
                                            <button type="button" value="<?= $pricing->id ?>" name="pricing_id"
                                                    class="btn primary-btn pricing-btn secondary-btn"
                                                    onclick="choosePlan(<?= $pricing->id ?>)">
                                                <?= lang('buy_now') ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!--//PRICE CONTENT END-->
                            </div>
                        <?php }
                    } ?>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>