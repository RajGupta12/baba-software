</main>
<!-- Footer-area start -->

<!-- sanjiv made changes in footer class -->
<footer class="footer-area bg-dark pt-50 pb-50" style="<?php if (config_item('front_footer_bg')) {
                                                            echo 'background-image: url("' . base_url() . config_item('front_footer_bg') . '")';
                                                        } elseif (config_item('front_footer_bg_color')) {
                                                            echo 'background-color:' . config_item('front_footer_bg_color');
                                                        } else {
                                                            echo 'background-color: "#f8f9fa"';
                                                        } ?> ">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4">
                <div class="footer-widget mb-30">
                    <!-- <h4 class="widget-title">
                        <?php
                        if (config_item('front_footer_col_1_title') != '') {
                            echo config_item('front_footer_col_1_title');
                        } ?>
                    </h4> -->
                    <a href="http://localhost/baba-soft/home">
                        <img src="http://localhost/baba-soft/uploads/baba-logo.png" alt="SaaSera Logo">
                    </a>
                    <ul class="footer-link" style="margin-left:30px;">
                        <!-- <li><a href="">PAYROLL BABA</a></li>
                        <li><a href="">CRM BABA</a></li>
                        <li><a href="">ATS BABA</a></li>
                        <li><a href="">LMS BABA</a></li>
                        <li><a href="">AR BABA</a></li>
                        <li><a href="">SALES BABA</a></li>
                        <li><a href="">HR BABA</a></li> -->
                        <li><a href="">
                                <?php
                                if (config_item('front_footer_col_1_description') != '') {
                                    echo config_item('front_footer_col_1_description');
                                } ?>
                            </a></li>
                    </ul>
                    <!-- <p style="color:white;margin-top:25px;margin-left:10px;">
                        <?php
                        if (config_item('front_footer_col_1_description') != '') {
                            echo config_item('front_footer_col_1_description');
                        } ?>
                    </p> -->
                    <!-- sanjiv -->
                    <!-- <p style="color:white;">
                        <?php
                        if (config_item('front_footer_col_1_description') != '') {
                            echo config_item('front_footer_col_1_description');
                        } ?>
                    </p> -->
                    <!-- <?php
                            if (config_item('front_footer_col_1_description') != '') {
                                echo config_item('front_footer_col_1_description');
                            } ?> -->

                    <!-- <ul class="footer-icons">
                        <?php if (config_item('front_facebook_link') != '') { ?>
                            <li>
                                <a target="_blank" href="<?php echo config_item('front_facebook_link'); ?>">
                                    <i class="fab fa-facebook-f"></i></a>
                            </li>
                        <?php }
                        if (config_item('front_twitter_link') != '') { ?>
                            <li>
                                <a target="_blank" href="<?php echo config_item('front_twitter_link'); ?>">
                                    <i class="fab fa-twitter"></i></a>
                            </li>
                        <?php }
                        if (config_item('front_google_link') != '') { ?>
                            <li>
                                <a target="_blank" href="<?php echo config_item('front_google_link'); ?>">
                                    <i class="fab fa-google-plus-g"></i></a>
                            </li>
                        <?php }
                        if (config_item('front_instagram_link') != '') { ?>
                            <li>
                                <a target="_blank" href="<?php echo config_item('front_instagram_link'); ?>">
                                    <i class="fab fa-instagram"></i></a>
                            </li>
                        <?php }
                        if (config_item('front_linkedin_link') != '') { ?>
                            <li>
                                <a target="_blank" href="<?php echo config_item('front_linkedin_link'); ?>">
                                    <i class="fab fa-linkedin-in"></i></a>
                            </li>
                        <?php }
                        if (config_item('front_pinterest_link') != '') { ?>
                            <li>
                                <a target="_blank" href="<?php echo config_item('front_pinterest_link'); ?>">
                                    <i class="fab fa-pinterest-p"></i></a>
                            </li>
                        <?php } ?>
                    </ul> -->
                </div>
            </div>


            <div class="col-xl-2 col-lg-2 col-md-2">

                <!-- sanjiv -->
                <div class="footer-widget mb-30" style="border-left:1px solid white;padding-left:20px;">
                    <h4 class="widget-title">
                        <?php
                        if (config_item('front_footer_col_2_title') != '') {
                            echo config_item('front_footer_col_2_title');
                        } ?>

                    </h4>
                    <!-- sanjiv -->
                    <ul class="footer-link">
                        <!-- <li><a href="">Products</a></li>
                        <li><a href="">Solution</a></li>
                        <li><a href="">Pricing</a></li>
                        <li><a href="">Support</a></li> -->
                        <li><a href=""><?php
                                        if (config_item('front_footer_col_2_description') != '') {
                                            echo config_item('front_footer_col_2_description');
                                        } ?> </a>
                        </li>

                    </ul>
                    <!-- <?php
                            if (config_item('front_footer_col_2_description') != '') {
                                echo config_item('front_footer_col_2_description');
                            } ?> -->

                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2">
                <div class="footer-widget mb-30">
                    <!-- sanjiv -->
                    <h5 class="widget-title">
                        Event
                        <!-- <?php
                                if (config_item('front_footer_col_3_title') != '') {
                                    echo config_item('front_footer_col_3_title');
                                } ?> -->
                    </h5>

                    <ul class="footer-link">
                        <li><a href="">Customer First Summit</a></li>

                    </ul>
                    <br>
                    <h5 class="widget-title">
                        Case Studies
                    </h5>
                    <ul class="footer-link">
                        <li><a href="">Customer First Summit</a></li>

                    </ul>

                    <!-- <?php
                            if (config_item('front_footer_col_3_description') != '') {
                                echo config_item('front_footer_col_3_description');
                            } ?> -->
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4">
                <!-- sanjiv -->
                <h5 class="widget-title">
                    Contact Us
                    <!-- <?php
                            if (config_item('front_footer_col_4_title') != '') {
                                echo config_item('front_footer_col_4_title');
                            } ?> -->
                </h5>
                <ul class="contact-list">
                    <!-- sanjiv   -->
                    <li>
                        
                            <?php
                            if (config_item('front_footer_col_4_description') != '') {
                                echo config_item('front_footer_col_4_description');
                            } ?> 
                         
                    </li>
                </ul>
                <!-- <?php
                        if (config_item('front_footer_col_4_description') != '') {
                            echo config_item('front_footer_col_4_description');
                        } ?> -->
            </div>
        </div>

        <!-- sanjiv -->

        <div class="row">

            <div class="col-md-12 col-lg-12 col-xs-12 ">

                <ul class="footer-icons text-center">
                    <?php if (config_item('front_facebook_link') != '') { ?>
                        <li>
                            <a class="facebook_bg" target="_blank" href="<?php echo config_item('front_facebook_link'); ?>">
                                <i class="fab fa-facebook-f "></i></a>
                        </li>
                    <?php }
                    if (config_item('front_twitter_link') != '') { ?>
                        <li>
                            <a class="twitter_bg" target="_blank" href="<?php echo config_item('front_twitter_link'); ?>">
                                <i class="fab fa-twitter"></i></a>
                        </li>
                    <?php }
                    if (config_item('front_google_link') != '') { ?>
                        <li>
                            <a class="googleplus_bg" target="_blank" href="<?php echo config_item('front_google_link'); ?>">
                                <i class="fab fa-google-plus-g"></i></a>
                        </li>
                    <?php }
                    if (config_item('front_instagram_link') != '') { ?>
                        <li>
                            <a class="instagram_bg" target="_blank" href="<?php echo config_item('front_instagram_link'); ?>">
                                <i class="fab fa-instagram"></i></a>
                        </li>
                    <?php }
                    if (config_item('front_linkedin_link') != '') { ?>
                        <li>
                            <a class="linkedin_bg" target="_blank" href="<?php echo config_item('front_linkedin_link'); ?>">
                                <i class="fab fa-linkedin-in"></i></a>
                        </li>
                    <?php }
                    if (config_item('front_pinterest_link') != '') { ?>
                        <li>
                            <a target="_blank" href="<?php echo config_item('front_pinterest_link'); ?>">
                                <i class="fab fa-pinterest-p"></i></a>
                        </li>
                    <?php } ?>
                </ul>


            </div>
        </div>



    </div>
</footer>
<!-- Footer-area end -->

<div class="modal fade bd-example-modal-lg" id="modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="modal-lg-con">
            ...
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-xl" id="modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" id="modal-xl-con">
            ...
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-sm" id="modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" id="modal-sm-con">
            ...
        </div>
    </div>
</div>


<!-- JS here -->
<script src="<?= base_url() ?>assets/front/js/vendor/modernizr-3.5.0.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/popper.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/one-page-nav-min.js"></script>
<script src="<?= base_url() ?>assets/front/js/slick.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/jquery.meanmenu.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/ajax-form.js"></script>
<script src="<?= base_url() ?>assets/front/js/wow.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/jquery.scrollUp.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/imagesloaded.pkgd.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/js_jquery.knob.js"></script>
<script src="<?= base_url() ?>assets/front/js/js_jquery.appear.js"></script>
<script src="<?= base_url() ?>assets/front/js/waypoints.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/jquery.counterup.min.js"></script>
<script src="<?= base_url() ?>assets/front/js/plugins.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvEEMx3XDpByNzYNn0n62Zsq_sVYPx1zY"></script>
<script src="<?= base_url() ?>assets/front/js/main.js"></script>
<script>
    // mainSlider
    function mainSlider(Speed) {
        var BasicSlider = $('.slider-active');
        BasicSlider.on('init', function(e, slick) {
            var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });
        BasicSlider.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });
        BasicSlider.slick({
            autoplay: 1,
            autoplaySpeed: Speed,
            dots: false,
            fade: true,
            arrows: false,
            responsive: [{
                breakpoint: 767,
                settings: {
                    dots: false,
                    arrows: false
                }
            }]
        });

        function doAnimations(elements) {
            var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            elements.each(function() {
                var $this = $(this);
                var $animationDelay = $this.data('delay');
                var $animationType = 'animated ' + $this.data('animation');
                $this.css({
                    'animation-delay': $animationDelay,
                    '-webkit-animation-delay': $animationDelay
                });
                $this.addClass($animationType).one(animationEndEvents, function() {
                    $this.removeClass($animationType);
                });
            });
        }
    }
    var slider_speed = "<?php echo (!empty(config_item('home_slider_speed')) ? config_item('home_slider_speed') . '000' : 5000); ?>";
    // alert(slider_speed);
    mainSlider(slider_speed);


    $(document).ready(function() {

        /*-----------------------------------
           Contact Form
           -----------------------------------*/

        // Function for email address validation
        function isValidEmail(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

            return pattern.test(emailAddress);

        }

        $("#contactForm").on('submit', function(e) {
            e.preventDefault();
            var data = {
                name: $("#name").val(),
                email: $("#email").val(),
                phone: $("#phone").val(),
                subject: $("#subject").val(),
                description: $("#description").val()
            };

            if (isValidEmail(data['email']) && (data['description'].length > 1) && (data['subject'].length > 1) && (data['name'].length > 1) &&
                (data['phone'].length > 1)) {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() ?>home/contact",
                    data: data,
                    success: function(response) {
                        //console.log(response);
                        $('#contactForm .input-success').delay(500).fadeIn(1000);
                        $('#contactForm .input-error').fadeOut(500);
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                    },
                });
            } else {
                $('#contactForm .input-error').delay(500).fadeIn(1000);
                $('#contactForm .input-success').fadeOut(500);
            }

            return false;
        });

        // subscription
        $("#subscription").on('submit', function(e) {
            e.preventDefault();

            var data = {
                email: $("#sub_email").val(),
            };

            if (isValidEmail(data['email'])) {

                $.ajax({
                    type: "POST",
                    url: "<?= base_url() ?>home/subscribe",
                    data: data,
                    success: function(res) {
                        //alert('success');
                        $('#message .input-success').delay(500).fadeIn(1000);
                        $('#message .input-error').fadeOut(500);
                    }
                });
            } else {
                $('#message .input-error').delay(500).fadeIn(1000);
                $('#message .input-success').fadeOut(500);
            }

        });
    })
</script>
</body>

</html>