<!--Newsletter-area-start -->
<section class="newsletter gray-bg pt-120 pb-100">
    <div class="container">
        <div class="section-header text-center mb-50">
            <h2><?php if (config_item('front_subscribe_title') != '') { echo config_item('front_subscribe_title'); } ?></h2>
            <p><?php if (config_item('front_subscribe_description') != '') { echo config_item('front_subscribe_description'); } ?></p>
        </div>
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="newsletter mb-30">
                    <form id="subscription" action="#" method="post">
                        <input id="sub_email" name="sub_email" placeholder="Enter your email address...." type="text">
                        <button type="submit"><i class="fas fa-envelope-open"></i>Subscribe</button>
                    </form>
                </div>

                <div id="message">
                    <style>
                        .hidden{display: none;}
                    </style>

                    <div class="alert alert-success input-success hidden" role="alert">
                        Thanks for Subscribe.
                    </div>

                    <div class="alert alert-danger input-error hidden" role="alert">
                        Sorry, something went wrong. try again later.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Newsletter-area-end -->