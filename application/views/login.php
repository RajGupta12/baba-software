<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php echo $title; ?></title>
    <?php if (config_item('favicon') != '') : ?>
        <link rel="icon" href="<?php echo base_url() . config_item('favicon'); ?>" type="image/png">
    <?php else : ?>
        <link rel="icon" href="<?php echo base_url('assets/img/favicon.ico'); ?>" type="image/png">
    <?php endif; ?>
    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome/css/font-awesome.min.css">
    <!-- Toastr-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/toastr.min.css">
    <!-- =============== BOOTSTRAP STYLES ===============-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" id="bscss">
    <!-- =============== APP STYLES ===============-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.min.css" id="maincss">

    <!-- JQUERY-->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/dist/jquery.min.js"></script>

    <?php if (config_item('recaptcha_secret_key') != '' && config_item('recaptcha_site_key') != '') { ?>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php } ?>
    <?php

    $this->load->helper('file');
    $lbg = config_item('login_background');
    if (!empty($lbg)) {
        $login_background = _mime_content_type($lbg);
        $login_background = explode('/', $login_background);
    }
    ?>
    <?php if (!empty($login_background[0]) && $login_background[0] == 'video') {  ?>
        <style type="text/css">
            body {
                padding: 0;
                margin: 0;
                background-color: #ffffff;
            }

            .vid-container {
                position: relative;
                height: 100vh;
                overflow: hidden;
            }

            .bgvid.back {
                position: fixed;
                right: 0;
                bottom: 0;
                min-width: 100%;
                min-height: 100%;
                width: auto;
                height: auto;
                z-index: -100;
            }

            .inner {
                position: absolute;
            }

            .inner-container {
                width: 400px;
                height: 400px;
                position: absolute;
                top: calc(50vh - 200px);
                left: calc(50vw - 200px);
                overflow: hidden;
            }

            .bgvid.inner {
                width: 100%;
                /*border: 10px solid red;*/
                min-height: 100%;
                height: auto;
            }

            .box {
                position: absolute;
                height: 100%;
                width: 100%;
                font-family: Helvetica;
                color: #fff;
                background: rgba(0, 0, 0, 0.13);
                padding: 30px 0px;
            }

            .box h1 {
                text-align: center;
                margin: 30px 0;
                font-size: 30px;
            }

            .panel {
                background: transparent;

            }

            input {
                background: rgba(0, 0, 0, 0.2);
                color: #fff;
                border: 0;
            }

            input:focus,
            input:active,
            button:focus,
            button:active {
                outline: none;
            }

            .box button:active {
                background: #27ae60;
            }

            .box p {
                font-size: 14px;
                text-align: center;
            }

            .box p span {
                cursor: pointer;
                color: #666;
            }
        </style>
    <?php } ?>
</head>
<?php
$login_position = config_item('login_position');
if (!empty($login_background[0]) && $login_background[0] == 'image') {
    $login_background = config_item('login_background');
    if (!empty($login_background)) {
        $back_img = base_url() . '/' . config_item('login_background');
    }
} ?>
<style>
    body {
        background-color: #ffffff;
        overflow: hidden;
    }

    .left-login {
        height: auto;
        min-height: 100%;
        background: #fff;
        -webkit-box-shadow: 2px 0px 7px 1px rgba(0, 0, 0, 0.08);
        -moz-box-shadow: 2px 0px 7px 1px rgba(0, 0, 0, 0.08);
        box-shadow: 2px 0px 7px 1px rgba(0, 0, 0, 0.08);
    }

    .left-login-panel {
        -webkit-box-shadow: 0px 0px 28px -9px rgba(0, 0, 0, 0.74);
        -moz-box-shadow: 0px 0px 28px -9px rgba(0, 0, 0, 0.74);
        box-shadow: 0px 0px 28px -9px rgba(0, 0, 0, 0.74);
    }

    .apply_jobs {
        position: absolute;
        z-index: 1;
        right: 0;
        top: 0
    }

    /* sanjiv */
    .login-center {
        /* background: #fff; */
        width: 400px;
        margin: 0 auto;
    }

    @media only screen and (max-width: 380px) {
        .login-center {
            width: 320px;
            /* padding: 10px; */
        }

        .wd-xl {
            width: 260px;
        }
    }
</style>

<!-- sanjiv -->

<body style="background: url('../baba-soft/uploads/cinematic-bg-3-loopable-background_nl6buhlfg__F0000-2.png') no-repeat center center fixed;
 -webkit-background-size: cover;
 -moz-background-size: cover;
 -o-background-size: cover;
 background-size: cover;min-height: 100%;width:100% overflow:hidden;" cz-shortcut-listen="true">

    <div class="login-center">
        <div class="wrapper" style="margin: 30% 0 0 auto;">
            <div class="block-center mt-xl wd-xl">

                <div class="error_login">
                </div>
                <!-- START panel-->
                <div class="panel panel-dark panel-flat left-login-panel">
                    <!-- sanjiv -->
                    <!-- <div>
                        <a href="#" style="color: #ffffff">
                            <span style="font-size: 15px;">BABA Software </span></a>
                    </div> -->
                    <div class="text-center" style="margin-bottom: 20px">
                        <img style="width: 100%;margin-top:35px;" src="http://baba.software/baba-payroll/uploads/admin-logo.png" class="m-r-sm">
                    </div>

                    <div class="panel-body">
                        <p class="text-center pv" style="color:rgb(75, 211, 75);font-weight:700;font-size:16px;letter-spacing:1px; ">SIGN IN TO CONTINUE</p>
                        <form data-parsley-validate="" novalidate="" action="<?php echo base_url() ?>/login" method="post">
                            <div class="form-group has-feedback">
                                <input type="text" id="email" name="user_name" required="true" class="form-control" placeholder="Username" style="border:none;outline:none;border-bottom:1px solid grey;margin-top:50px;" data-parsley-id="4">
                                <span class="fa fa-envelope form-control-feedback text-muted"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" id="password" name="password" required="true" class="form-control" placeholder="Password" style="border:none;outline:none;border-bottom:1px solid grey;margin-top:50px;" data-parsley-id="6">
                                <span class="fa fa-lock form-control-feedback text-muted"></span>
                            </div>
                            <div class="clearfix" style="margin-top:30px;">
                                <div class="checkbox c-checkbox pull-left mt0">
                                    <label style="font-weight:700;">
                                        <input type="checkbox" value="" name="remember" data-parsley-multiple="remember" data-parsley-id="9">
                                        <span class="fa fa-check"></span>Remember Me</label>
                                </div>
                                <div class="pull-right"><a href="http://baba.software/baba-payroll/login/forgot_password" class="text-muted">Forgot password?</a>
                                </div>
                            </div>

                            <div style="margin-top:20px;">
                                <!-- sanjiv -->
                                <button type="submit" style="background-color:#2196f3;color:White;border-radius:30px;width:120px;padding:10px;font-size:15px;" class="btn   btn-flat">Sign in <i class="fa fa-arrow-right"></i></button>
                                <button type="submit" style=" color:White;border-radius:30px;padding:10px;font-size:15px;" name="mark_attendance" value="mark_attendance" class="btn btn-purple btn-flat pull-right">
                                    <i class="fa fa-clock-o"></i> Mark Attendance </button>
                            </div>

                        </form>
                    </div>
                    <div class="p-lg text-center">
                        <span>©</span>
                        <span><a href="https://codecanyon.net/user/unique_coder"> Uniquecoder</a></span>
                        <br>
                        <span>2015-2020</span>
                        <span>-</span>
                        <span>Version 1.2.4</span>
                    </div>
                </div>
                <!-- END panel-->

            </div>
        </div>
    </div>



    <div class=" hidden-xs">
    </div>

    <!-- =============== VENDOR SCRIPTS ===============-->

    <!-- =============== Toastr ===============-->
    <script src="http://baba.software/baba-payroll/assets/js/toastr.min.js"></script>
    <!-- BOOTSTRAP-->
    <script src="http://baba.software/baba-payroll/assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- STORAGE API-->
    <script src="http://baba.software/baba-payroll/assets/plugins/jQuery-Storage-API/jquery.storageapi.min.js"></script>
    <script src="http://baba.software/baba-payroll/assets/plugins/parsleyjs/parsley.min.js"></script>




</body>


</html>