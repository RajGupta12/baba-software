<p class="text-center pv" style="color:rgb(75, 211, 75);font-weight:700;font-size:16px;letter-spacing:1px; "><?= lang('sing_in_to_continue') ?></p>
<form data-parsley-validate="" novalidate="" action="<?php echo base_url() ?>login" method="post">
    <div class="form-group has-feedback">
        <input type="text" id="email" name="user_name" required="true" class="form-control" placeholder="<?= lang('username') ?>" style="border:none;outline:none;border-bottom:1px solid grey;margin-top:50px;" />
        <span class="fa fa-envelope form-control-feedback text-muted"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" id="password" name="password" required="true" class="form-control" placeholder="<?= lang('password') ?>" style="border:none;outline:none;border-bottom:1px solid grey;margin-top:50px;" />
        <span class="fa fa-lock form-control-feedback text-muted"></span>
    </div>
    <div class="clearfix" style="margin-top:30px;">
        <div class="checkbox c-checkbox pull-left mt0">
            <label style="font-weight:700;">
                <input type="checkbox" value="" name="remember">
                <span class="fa fa-check"></span><?= lang('remember_me') ?></label>
        </div>
        <div class="pull-right"><a href="<?= base_url() ?>login/forgot_password" class="text-muted"><?= lang('forgot_password') ?></a>
        </div>
    </div>
    <?php if (config_item('recaptcha_secret_key') != '' && config_item('recaptcha_site_key') != '') { ?>
        <div class="g-recaptcha mb-lg mt-lg" data-sitekey="<?php echo config_item('recaptcha_site_key'); ?>"></div>
    <?php }
    $mark_attendance_from_login = config_item('mark_attendance_from_login');
    if (!empty($mark_attendance_from_login) && $mark_attendance_from_login == 'Yes') {
        $class = null;
    } else {
        $class = 'btn-block';
    } 
    ?>

    <div style="margin-top:20px;">
        <!-- sanjiv -->
        <button type="submit" style="background-color:#2196f3;color:White;border-radius:30px;width:120px;padding:10px;font-size:15px;" class="btn  <?= $class ?> btn-flat"><?= lang('sign_in') ?> <i class="fa fa-arrow-right"></i></button>
        <?php if (empty($class)) { ?>
            <button type="submit"  style=" color:White;border-radius:30px;padding:10px;font-size:15px;" name="mark_attendance" value="mark_attendance" class="btn btn-purple btn-flat pull-right">
                <i class="fa fa-clock-o"></i> <?= lang('mark_attendance') ?> </button>
        <?php } ?>
    </div>

</form>