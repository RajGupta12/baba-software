<section class="first-fold pattern-gradient-light">
    <div class="l-page container mt-md">
      <div class="row">
        <div class="col-md-6 align-center-in-mobile align-left">     
          <h1>Schedule a Baba Software demo</h1>
          <p class="sub-text">In this one-hour session, our product consultant will walk you through a workflow that will demonstrate how you can provide excellent customer service with Baba Software.</p>        
          <div class="hide-in-mobile-and-tablet">
            <div class="mt-md "><div class="hr"></div></div>      
            <h6 class="mt-md">You will learn how you can</h6>
            <ul class="list-fdesk">
          	 <li>Manage support queries from different channels in one place</li>
          	 <li>Increase your team's engagement and productivity while supporting customers</li>
          	 <li>Get metrics and insights on your team and your customers</li>
            </ul>
          </div>
        </div>
      <div class="col-md-6 mt-sm">
        <div class="form-wrapper contact-form-wrapper form-white-bg">
          <div class="form-heading-text"></div>

            <form autocomplete="off" class="form-field-container demo-request-form" data-redirect="">
              <input style="display:none" class="list-id" name="list-id" value="">
              <input class="fm-list-id hide" name="fm-list-id" value="1108">

              <fieldset>

                <div class="name-field-wrapper">
                  <div class="name-field">
                    <div class="form-field">
                      <i class="icon-user"></i>
                      <input type="text" name="first-name" class="first-name-form" data-first-name-required="First name is required">
                      <label class="form-placeholder">First Name</label>
                      <div class="error-wrapper"></div>
                    </div>
                  </div>
                  <div class="name-field">
                    <div class="form-field">
                      <input type="text" name="last-name" class="last-name-form" data-last-name-required="Last name is required">
                      <label class="form-placeholder">Last Name</label>
                      <div class="error-wrapper"></div>
                    </div>
                  </div>
                </div>
              
                <div class="form-field">
                  <i class="icon-email"></i>
                  <input type="email" name="email" class="email-form" data-email-required="Please Enter an Email" data-email-invalid="Please enter a valid email address">
                  <label class="form-placeholder">Business Email</label>
                  <div class="error-wrapper"></div>
                </div>
                    
                <div class="form-field">
                  <i class="icon-company"></i>
                  <input type="text" name="company" class="company-form" data-company-min-length="Company should be minimum of 3 characters" data-company-required="Tell us where you work">
                  <label class="form-placeholder">Company Name</label>
                  <div class="error-wrapper"></div>
                </div>
                    
                <div class="form-field">
                  <i class="icon-mobile"></i>
                  <input type="text" name="phone" class="phone-form" data-phone-invalid="Only numbers and hyphen allowed">
                  <label class="form-placeholder">Phone</label>
                  <div class="error-wrapper"></div>
                </div>

              </fieldset>

              <fieldset>
                <input type="submit" value="Request your demo" class="button button-submit button--solid button--block ">
                <div class="copy_write-text align-center">
                  <p>By clicking on "REQUEST YOUR DEMO" you acknowledge having read our <a href="" target="">Privacy notice</a></p>
                </div>
                <div class="eu-extra-info ">
                  <label class="checkbox-control cb col-sm-12 promotional-offers">
                    <input type="checkbox" class="cb-type" name="send_promotions" value="true">
                    <div class="checkbox-control-indicator"></div>
                    <span>I agree to receive electronic marketing communications from Freshdesk and understand I can unsubscribe by clicking the ‘unsubscribe’ link in any email or by contacting Freshdesk.</span>
                  </label>
                  <div class="signup-terms">By clicking on "REQUEST YOUR DEMO" you acknowledge having read our
                    <a href="" target="">Privacy notice</a>
                  </div>
                </div>
              </fieldset> 

            </form>
            
        </div> 
      </div>
    </div>
  </div>
</section>